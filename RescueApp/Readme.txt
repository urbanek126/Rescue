Dane o alarmach gromadzone sa lokalnie na urządzeniu mobilnym w bazie danych Couchbase Lite oraz na dysku. Dane nastepnie synchronizowane sa z centralnym repozytorium zgloszen. W kolejnej fazie projektu zostanie przygotowany webowy interfejs do tego repozytorium.

Ze względu na wiele problemów z ReactNative aplikacja ostatecznie została zrealizowana w calosci w jezyku Java.

Aplikacja została zrealizowana wykorzystując podejście Domain Driven Design. Komunikacja pomiędzy poszczególnymi komponentami aplikacji realizowana jest poprzez publikacje zdarzen przy uzyciu EventBus (https://github.com/greenrobot/EventBus)

Zarzadzanie zależnościami zostalo zrealizowany przy uzyciu Daggera http://square.github.io/dagger/.

W celu wykrycia upadku/wypadku zostały wykorzystane algorytmy uczenia maszynowego implementowane w bibliotece Weka (http://www.cs.waikato.ac.nz/ml/weka/).

Dokumenty przechowywane w bazie lokalnej przy odczycie i zapisie  mapowa sa  przy uzyciu couchbase-lite-orm (https://github.com/BraisGabin/couchbase-lite-orm).

Aplikacja został zintegrowana z urzadzenie HTC Fetch (http://www.htc.com/us/accessories/htc-fetch/). Komunikacja pomiędzy urzadzeniem a aplikacja realizowane jest przy użyciu protokołu BLE (Bluetooth low energy). W kolejnych wersja aplikacji planujemy dodać obslugę innych urzadzeń np pulsometrow, ktore w momencie przekroczenia określonej wartości beda mogly uruchomić równiez alarm.