package pl.urbansoft.rescue.domain.shared.event;

import pl.urbansoft.rescue.domain.shared.entity.Location;

/**
 * Created by urbanek126 on 1/1/2016.
 */
public class LocationUpdatedEvent {
    private Location location;

    public LocationUpdatedEvent(Location location){
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
