package pl.urbansoft.rescue.domain.model.alert.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToInActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToPreActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertActiveFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertContactFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertInactiveFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertPreActiveFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.IconBarFragment;
import pl.urbansoft.rescue.domain.service.AlertService;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.ui.BaseActivity;

public class AlertActivity extends BaseActivity {
    private static final String TAG = AlertActivity.class.getSimpleName();

    private static final String REGISTER_ACTIVITY = "register_activity";
    public static final String PRE_ALERT_RAISED = "AlertActivity.pre_alert_raised";
    public static final String ALERT_CONFIRMED = "AlertActivity.alert_confirmed";

    public static final int CAMERA_PERMISSION_REQUEST = 1;
    public static final int LOCATION_PERMISSION_REQUEST = 2;
    public static final int SEND_SMS_PERMISSION_REQUEST = 3;
    public static final int MICROPHONE_PERMISSION_REQUEST = 4;

    /*
         Monitor if activity is running
     */
    public static boolean isActive = false;

    @Inject
    EventBus eventBus;

    @Inject
    UserService userService;

    @Inject
    AlertService alertService;

    private FragmentManager fragmentManager;
    private SharedPreferences sp;
    private Toolbar myToolbar;
    private LayoutInflater li;
    private View mcedfView;
    private TextView cancelBtn;
    private TextView acceptBtn;
    private TextView agrementsDialgoTitle;
    private TextView agrementsDialgoContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert);
        ((RescueApp) getApplication()).getRescueComponent().inject(this);

        Log.i(TAG, "AlertActivity:oncreate()" + this);

        //Intent intent = getIntent();
        Intent intent = getIntent();
        boolean isAlertRaised = intent.getBooleanExtra(AlertActivity.PRE_ALERT_RAISED, false);
        boolean isAlertConfirmed = intent.getBooleanExtra(AlertActivity.ALERT_CONFIRMED, false);
        fragmentManager = getFragmentManager();
        sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        if (!isAlertConfirmed) {
            if (!isAlertRaised) {
                // first screen

                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.alert_layout, AlertInactiveFragment.newInstance())
                        .commit();

            } else {
                //count down screen
                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.alert_layout, AlertPreActiveFragment.newInstance())
                        .commit();

            }
            //common fragments for first and count down screens

            fragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.icon_bar, IconBarFragment.newInstance())
                    .commit();

            refreshAlertContactFragment();
        } else {
            //progress bar screen
            fragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .replace(R.id.alert_layout, AlertActiveFragment.newInstance())
                    .commit();
        }

        myToolbar = (Toolbar) findViewById(R.id.toolbar);

        checkAppAgrements();

        setSupportActionBar(myToolbar);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_alert, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "AlertActivity:OnStart()" + this);
        isActive = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "AlertActivity:onResume()" + this);
        //     checkRegisterActivityStatus();

        refreshAlertContactFragment();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "AlertActivity:Onstop()" + this);
        isActive = false;
        eventBus.unregister(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "AlertActivity:onDestroy()" + this);
    }

    public void onEvent(AlertChangedToActiveEvent e) {
        Log.i(TAG, "CHANGED TO ACTIVE EVENT");

        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.alert_layout, AlertActiveFragment.newInstance())
                .commit();

    }

    public void onEvent(AlertChangedToPreActiveEvent e) {
        Log.i(TAG, "CHANGED TO PRE EVENT");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                setToolbarMenuVisibility(false);

                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.alert_layout, AlertPreActiveFragment.newInstance())
                        .commit();

                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.bottom_contact, AlertContactFragment.newInstance())
                        .commit();

                fragmentManager.beginTransaction()
                        .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                        .replace(R.id.icon_bar, IconBarFragment.newInstance())
                        .commit();
            }
        });
    }

    public void onEvent(AlertChangedToInActiveEvent e) {

        setToolbarMenuVisibility(true);

        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.alert_layout, AlertInactiveFragment.newInstance())
                .commit();

        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.bottom_contact, AlertContactFragment.newInstance())
                .commit();

        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.icon_bar, IconBarFragment.newInstance())
                .commit();
    }

    @Override
    public void onEvent(UserExternalButtonUpdatedEvent event) {
        super.onEvent(event);

        if (event.isExteranlButtonUsed()) {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0, 1.0f);
            (findViewById(R.id.bleRelative)).setLayoutParams(param);
        } else {
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    0, 0f);
            (findViewById(R.id.bleRelative)).setLayoutParams(param);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            switch (requestCode) {
                case CAMERA_PERMISSION_REQUEST:
                    userService.getUser().setIsCamera(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.CAMERA));
                    break;
                case LOCATION_PERMISSION_REQUEST:
                    userService.getUser().setIsLocation(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.LOCATION));
                    break;
                case SEND_SMS_PERMISSION_REQUEST:
                    userService.getUser().setIsMessage(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.MESSAGE));
                    break;
                case MICROPHONE_PERMISSION_REQUEST:
                    userService.getUser().setIsMicrophone(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.MICROPHONE));
                    break;
            }
        }
    }

    private void checkAppAgrements(){
        float currentAppVersionAgrements =RescueApp.getAppVersion(getApplicationContext());

        if(!RescueApp.isAgreementAccepted(getApplicationContext())){
            createMessageContentEditorDialog(currentAppVersionAgrements);

        }
    }

    private void createMessageContentEditorDialog(final float currentAppVersionAgrements) {
        li = LayoutInflater.from(this);
        mcedfView = li.inflate(R.layout.dialog_app_agrement, null);
        cancelBtn = (TextView) mcedfView.findViewById(R.id.cancelBtn);
        acceptBtn = (TextView) mcedfView.findViewById(R.id.acceptBtn);
        agrementsDialgoTitle = (TextView) mcedfView.findViewById(R.id.agrementsDialgoTitle);
        agrementsDialgoContent = (TextView) mcedfView.findViewById(R.id.agrementsDialgoContent);
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(mcedfView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        agrementsDialgoContent.setMovementMethod(LinkMovementMethod.getInstance());

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
                System.exit(0);
            }
        });

        final Activity parentActivity = this;
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                SharedPreferences.Editor edit = sp.edit();
                edit.putFloat(RescueApp.APP_VERSION_AGREMENTS, currentAppVersionAgrements);
                edit.commit();

                //Ask for permissions
                //startActivity(new Intent(parentActivity, TutorialActivity.class));
                alertService.checkPermission(parentActivity);
            }
        });
        setDialogFonts();
        alertDialog.show();
    }


    private void  setToolbarMenuVisibility(boolean visible){
        Menu menu = myToolbar.getMenu();
        menu.setGroupVisible(R.id.menu_alert, visible);
    }

    private void setDialogFonts() {
        Typeface RobotoRegular = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface RobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        agrementsDialgoTitle.setTypeface(RobotoMedium);
        agrementsDialgoContent.setTypeface(RobotoRegular);
        cancelBtn.setTypeface(RobotoMedium);
        acceptBtn.setTypeface(RobotoMedium);
    }

    private void refreshAlertContactFragment(){
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(R.id.bottom_contact, AlertContactFragment.newInstance())
                .commit();
    }
}
