package pl.urbansoft.rescue.infrastructure.ble;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.domain.service.UserService;

/**
 * Created by andrzej on 11/27/15.
 */

@Singleton
@Module
public class BleModule {

    @Provides
    @Singleton
    public BleService provideBleService(UserService userService, EventBus eventBus) {
        return new BleService(eventBus,userService);
    }
}
