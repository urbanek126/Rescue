package pl.urbansoft.rescue.domain.model.history.ui;

import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.Menu;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.history.ui.fragments.EmptyHistoryFragment;
import pl.urbansoft.rescue.domain.model.history.ui.fragments.SummaryAlertFragment;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.AbstractDocument;
import pl.urbansoft.rescue.domain.shared.entity.Alert;
import pl.urbansoft.rescue.domain.shared.entity.enums.ContentType;
import pl.urbansoft.rescue.domain.shared.entity.enums.DocumentType;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.ui.BaseActivity;
import pl.urbansoft.rescue.infrastructure.repository.RescueRepository;

public class HistoryActivity extends BaseActivity {
    private static final String TAG = HistoryActivity.class.getSimpleName();
    private FragmentManager fragmentManager;

    @Inject
    UserService userService;

    @Inject
    RescueRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ((RescueApp) getApplication()).getRescueComponent().inject(this);

        fragmentManager = getFragmentManager();
        Log.i(TAG, "AlertActivity:oncreate()" + this);


        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        createAlertHistory();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "HistoryActivity:OnStart()" + this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "HistoryActivity:OnResume()" + this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "HistoryActivity:Onstop()" + this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "HistoryActivity:onDestroy()" + this);
    }

    @Override
    public void onEvent(UserExternalButtonUpdatedEvent event){
        super.onEvent(event);
    }

    private void createAlertHistory() {
        List<AbstractDocument> alerts = repository.getAllDocumentByType(DocumentType.ALERT);
        Iterator i = alerts.iterator();

        if(!i.hasNext()){
            addEmptyHistoryFragment();
        }

        while (i.hasNext()) {
            Alert alert = (Alert) i.next();
            SummaryAlertFragment summaryAlertFragment = SummaryAlertFragment.newInstance();
            String contact = alert.getContact().toString();
            int mp3Count = alert.getAttachmentsCountByType(ContentType.MP3);
            int jpegCount = alert.getAttachmentsCountByType(ContentType.JPEG);
            int RecordedDuration = alert.getRecordedDuration();
            int location = alert.getLocations().size();
            int messageCounter = alert.getMessageCount();
            long alertTimeStamp = alert.getTimeStamp();

            summaryAlertFragment.setArguments(createSummaryAlertFragmentBundle(mp3Count, jpegCount, RecordedDuration, contact, location, messageCounter, alertTimeStamp));
            fragmentManager.beginTransaction()
                    .add(R.id.history_layout, summaryAlertFragment)
                    .commit();
        }
    }

    private Bundle createSummaryAlertFragmentBundle(int mp3Count, int jpegCount, int RecordedDuration, String contact, int location, int messageCounter, long alertTimeStamp) {
        Bundle bundle = new Bundle();
        bundle.putString("microphone", makeMicroPhoneText(mp3Count, RecordedDuration));
        bundle.putString("camera", jpegCount + "");
        bundle.putString("contact", contact);
        bundle.putString("alertTitle", makeAlertTitle(alertTimeStamp));
        bundle.putString("location", location + "");
        bundle.putString("message", messageCounter + "");
        return bundle;
    }

    private String makeAlertTitle(long alertTimeStamp) {
        return getResources().getString(R.string.history_alarm) + " - " + new SimpleDateFormat("yyyy.MM.dd - HH:mm").format(alertTimeStamp);
    }

    private String makeMicroPhoneText(int mp3Count, int RecordedDuration) {
        return mp3Count + " (" + RecordedDuration / 1000 + " " + getResources().getString(R.string.sec) + ") ";
    }

    private void addEmptyHistoryFragment(){
        fragmentManager.beginTransaction()
                .add(R.id.history_layout, EmptyHistoryFragment.newInstance())
                .commit();
    }
}
