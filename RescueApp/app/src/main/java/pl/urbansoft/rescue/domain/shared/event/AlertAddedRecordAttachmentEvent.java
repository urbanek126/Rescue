package pl.urbansoft.rescue.domain.shared.event;

import java.io.InputStream;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public class AlertAddedRecordAttachmentEvent {
    private byte[] bData;
    private String name;
    private String contentType;
    private int recordedDuration;

    public AlertAddedRecordAttachmentEvent(byte[]  bData, String name, String contentType, int recordedDuration) {
        this.bData = bData;
        this.name = name;
        this.contentType = contentType;
        this.recordedDuration = recordedDuration;
    }

    public int getRecordedDuration() {
        return recordedDuration;
    }

    public String getName() {
        return name;
    }

    public byte[] getBData() {
        return bData;
    }

    public String getContentType() {
        return contentType;
    }
}
