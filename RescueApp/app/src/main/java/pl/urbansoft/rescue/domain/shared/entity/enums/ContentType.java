package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 12/29/2015.
 */
public enum ContentType {
    JPEG("image/jpeg","IMG_",".jpg"),MP3("audio/mpeg","MIC_",".mp3");

    private String type;
    private String name;
    private String extension;

    private ContentType(String type, String name,String extension) {
        this.type = type;
        this.name = name;
        this.extension = extension;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getExtension() {
        return extension;
    }
}
