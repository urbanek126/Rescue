package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by andrzej on 11/15/15.
 */
public class WekaClassifierNormal implements RescueClassifier {

    public  double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifierNormal.N68f64d931(i);
        return p;
    }

    static double N68f64d931(Object[] i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 769.805444) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 769.805444) {
            p = 1;
        }
        return p;
    }
}
