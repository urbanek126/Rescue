package pl.urbansoft.rescue.domain.service;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.io.InputStream;

import javax.inject.Inject;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.AlertContext;
import pl.urbansoft.rescue.domain.shared.entity.Alert;
import pl.urbansoft.rescue.infrastructure.repository.RescueRepository;

/**
 * Created by urbanek126 on 12/29/2015.
 */
public class AlertService {

    private static final String TAG = AlertService.class.getSimpleName();
    final private static int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Inject
    UserService userService;

    @Inject
    RescueRepository repository;


    private String alertFolderPath;

    public AlertService() {
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
    }

    public Alert createNewAlert() {

        Log.i(TAG, "Saving data to : " + alertFolderPath);
        return AlertContext.getCurrentAlert(userService.getUser(), true);
    }

    public void updateAlert() {
        synchronized (this){
            repository.saveOrUpdate(AlertContext.getCurrentAlert(userService.getUser(), false));
        }
    }

    public void addAttachment(byte[] bData, String name, String contentType) {
        repository.addAttachment(AlertContext.getCurrentAlert(userService.getUser(), false), name, contentType, bData);
    }

    public String getAlertFolderPath() {
        return alertFolderPath;
    }

    public boolean checkPermission(Activity activity){
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String[] permissions = new String[] {Manifest.permission.READ_CONTACTS, Manifest.permission.INTERNET, Manifest.permission.CALL_PHONE};

            for(String p:permissions){
                if(ContextCompat.checkSelfPermission(RescueApp.getContext(), p) != PackageManager.PERMISSION_GRANTED){
                    activity.requestPermissions(new String[]{p}, REQUEST_CODE_ASK_PERMISSIONS);
                }
            }
            boolean hasReadContactsPermission
                    = ContextCompat.checkSelfPermission(RescueApp.getContext(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;

            return hasReadContactsPermission;
        }

        return  true;
    }

}
