package pl.urbansoft.rescue.domain.shared.entity;

import com.braisgabin.couchbaseliteorm.Entity;
import com.braisgabin.couchbaseliteorm.Field;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.enums.CameraInterval;
import pl.urbansoft.rescue.domain.shared.entity.enums.LocationInterval;
import pl.urbansoft.rescue.domain.shared.entity.enums.MessageInterval;
import pl.urbansoft.rescue.domain.shared.entity.enums.MicrophoneDuration;
import pl.urbansoft.rescue.domain.shared.entity.enums.PhoneSensitivity;

/**
 * Created by urbanek126 on 12/20/2015.
 */
@Entity("user")
public class User extends AbstractDocument {
    private static final int CANCEl_LDURATION = 10;
    private static final int PHONE_SENSETIVY = 5;

    @Field("isVibration")
    Boolean isVibration;
    @Field("isSiren")
    Boolean isSiren;
    @Field("isMicrophone")
    Boolean isMicrophone;
    @Field("isCamera")
    Boolean isCamera;
    @Field("isMessage")
    Boolean isMessage;
    @Field("isLocation")
    Boolean isLocation;
    @Field("isPhoneCall")
    Boolean isPhoneCall;
    @Field("messageContent")
    String messageContent;
    @Field("cameraInterval")
    int cameraInterval;
    @Field("microphoneDuration")
    int microphoneDuration;
    @Field("messageInterval")
    int messageInterval;
    @Field("locationInterval")
    int locationInterval;
    @Field("phoneSensitivity")
    int phoneSensitivity;
    @Field("cancelDuration")
    int cancelDuration;
    @Field("contact")
    Contact contact;
    @Field("email")
    String email;
    @Field("password")
    String password;
    @Field("isExternalButtonAlert")
    Boolean isExternalButtonAlert;
    @Field("isPowerDeviceButtonAlert")
    Boolean isPowerDeviceButtonAlert;
    @Field("isHeadPhonePluginAlert")
    Boolean isHeadPhonePluginAlert;
    @Field("isAutoDetection")
    Boolean isAutoDetection;

    public User() {
        this.isVibration = true;
        this.isSiren = true;
        this.isMicrophone = false;
        this.isCamera = false;
        this.isMessage = false;
        this.isLocation = false;
        this.isPhoneCall = true;
        this.isAutoDetection = false;
        this.isExternalButtonAlert = false;
        this.isPowerDeviceButtonAlert = true;
        this.isHeadPhonePluginAlert = true;
        this.locationInterval = LocationInterval.VERY_FAST.getValue();
        this.cameraInterval = CameraInterval.VERY_FAST.getValue();
        this.microphoneDuration = MicrophoneDuration.LONG.getValue();
        this.messageInterval = MessageInterval.VERY_FAST.getValue();
        this.phoneSensitivity = PHONE_SENSETIVY;
        this.cancelDuration = CANCEl_LDURATION;
        this.messageContent = RescueApp.getContext().getString(R.string.default_message);
        this.contact = new Contact();
    }

    public String toString() {
        return  " isAutoDetection: " + isAutoDetection + " locationInterval: " + locationInterval + " isExternalButtonAlert: " + isExternalButtonAlert + " isPowerDeviceButtonAlert: " + isPowerDeviceButtonAlert + " isHeadPhonePluginAlert: " + isHeadPhonePluginAlert + " email: " + email + " isVibration: " + isVibration + " isSiren: " + isSiren + " isMicrophone: " + isMicrophone + " isCamera: " + isCamera + " isMessage: " + isMessage + " isPhoneCall: " + isPhoneCall + " messageContent: " + messageContent + " cameraInterval: " + cameraInterval + " microphoneDuration: " + microphoneDuration + " messageInterval " + messageInterval + " phoneSensitivity: " + phoneSensitivity + " cancelDuration: " + cancelDuration + " contact: " + contact;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public int getCancelDuration() {
        return cancelDuration;
    }

    public Boolean isAutoDetection() {
        return isAutoDetection;
    }

    public void setAutoDetection(boolean autoDetection) {
        this.isAutoDetection = autoDetection;
    }

    public void setCancelDuration(int cancelDuration) {
        this.cancelDuration = cancelDuration;
    }

    public int getPhoneSensitivity() {
        return phoneSensitivity;
    }

    public PhoneSensitivity getPhoneSensitivityScale() {
        return  PhoneSensitivity.getPhoneSensitivityName(phoneSensitivity);
    }

    public void setPhoneSensitivity(int phoneSensitivity) {
        this.phoneSensitivity = phoneSensitivity;
    }

    public int getMessageInterval() {
        return messageInterval;
    }

    public void setMessageInterval(int messageInterval) {
        this.messageInterval = messageInterval;
    }

    public int getMicrophoneDuration() {
        return microphoneDuration;
    }

    public void setMicrophoneDuration(int microphoneDuration) {
        this.microphoneDuration = microphoneDuration;
    }

    public int getCameraInterval() {
        return cameraInterval;
    }

    public void setCameraInterval(int cameraInterval) {
        this.cameraInterval = cameraInterval;
    }

    public int getLocationInterval() {
        return locationInterval;
    }

    public void setLocationInterval(int locationInterval) {
        this.locationInterval = locationInterval;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Boolean isPhoneCall() {
        return isPhoneCall;
    }

    public void setIsPhoneCall(Boolean isPhoneCall) {
        this.isPhoneCall = isPhoneCall;
    }

    public Boolean isMessage() {
        return isMessage;
    }

    public void setIsMessage(Boolean isMessage) {
        this.isMessage = isMessage;
    }

    public Boolean isCamera() {
        return isCamera;
    }

    public void setIsCamera(Boolean isCamera) {
        this.isCamera = isCamera;
    }

    public Boolean isLocation() {
        return isLocation;
    }

    public void setIsLocation(Boolean isLocation) {
        this.isLocation = isLocation;
    }

    public Boolean isMicrophone() {
        return isMicrophone;
    }

    public void setIsMicrophone(Boolean isMicrophone) {
        this.isMicrophone = isMicrophone;
    }

    public Boolean isSiren() {
        return isSiren;
    }

    public void setIsSiren(Boolean isSiren) {
        this.isSiren = isSiren;
    }

    public Boolean isVibration() {
        return isVibration;
    }

    public void setIsVibration(Boolean isVibration) {
        this.isVibration = isVibration;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isExternalButtonAlert() {
        return isExternalButtonAlert;
    }

    public void setExternalButtonAlert(Boolean externalButtonAlert) {
        this.isExternalButtonAlert = externalButtonAlert;
    }

    public Boolean isPowerDeviceButtonAlert() {
        return isPowerDeviceButtonAlert;
    }

    public void setPowerDeviceButtonAlert(Boolean powerDeviceButtonAlert) {
        this.isPowerDeviceButtonAlert = powerDeviceButtonAlert;
    }

    public Boolean isHeadPhonePluginAlert() {
        return isHeadPhonePluginAlert;
    }

    public void setHeadPhonePluginAlert(Boolean headPhonePluginAlert) {
        this.isHeadPhonePluginAlert = headPhonePluginAlert;
    }
}
