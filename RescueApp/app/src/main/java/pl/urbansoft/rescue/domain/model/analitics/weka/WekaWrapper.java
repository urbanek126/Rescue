package pl.urbansoft.rescue.domain.model.analitics.weka;

import java.util.HashMap;

import pl.urbansoft.rescue.domain.shared.entity.enums.PhoneSensitivity;
import weka.core.Capabilities;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.RevisionUtils;

/**
 * Created by andrzej on 11/15/15.
 */
public class WekaWrapper {

    private static final HashMap<PhoneSensitivity, RescueClassifier> classifierMap;

    static {
        classifierMap = new HashMap<>();
        classifierMap.put(PhoneSensitivity.HIGH, new WekaClassifierOutlierSensitive());
        classifierMap.put(PhoneSensitivity.MEDIUM, new WekaClassifierOutlier());
        classifierMap.put(PhoneSensitivity.SMALL, new WekaClassifierOutlierExtreme());
    }
    /**
     * Returns only the toString() method.
     *
     * @return a string describing the classifier
     */
    public String globalInfo() {
        return toString();
    }

    /**
     * Returns the capabilities of this classifier.
     *
     * @return the capabilities
     */
    public Capabilities getCapabilities() {
        return null;
    }

    /**
     * only checks the data against its capabilities.
     *
     * @param i the training data
     */
    public void buildClassifier(Instances i) throws Exception {
        // can classifier handle the data?
        getCapabilities().testWithFail(i);
    }

    /**
     * Classifies the given instance.
     *
     * @param i the instance to classify
     * @return the classification result
     */
    public double classifyInstance(Instance i, PhoneSensitivity sensitivity) throws Exception {
        Object[] s = new Object[i.numAttributes()];

        for (int j = 0; j < s.length; j++) {
            if (!i.isMissing(j)) {
                if (i.attribute(j).isNominal())
                    s[j] = new String(i.stringValue(j));
                else if (i.attribute(j).isNumeric())
                    s[j] = new Double(i.value(j));
            }
        }

        // set class value to missing
        s[i.classIndex()] = null;

        return classifierMap.get(sensitivity).classify(s);
    }

    public double[] distributionForInstance(Instance instance) throws Exception {
        return new double[0];
    }

    /**
     * Returns the revision string.
     *
     * @return        the revision
     */
    public String getRevision() {
        return RevisionUtils.extract("1.0");
    }

    /**
     * Returns only the classnames and what classifier it is based on.
     *
     * @return a short description
     */
    public String toString() {
        return "Auto-generated classifier wrapper, based on weka.classifiers.trees.J48 (generated with Weka 3.6.13).\n" + this.getClass().getName() + "/WekaClassifierSensitive";
    }

}
