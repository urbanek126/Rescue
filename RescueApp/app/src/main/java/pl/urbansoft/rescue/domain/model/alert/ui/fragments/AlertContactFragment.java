package pl.urbansoft.rescue.domain.model.alert.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rey.material.widget.ImageButton;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.service.AlertService;
import pl.urbansoft.rescue.domain.service.LocationService;
import pl.urbansoft.rescue.domain.service.PhoneService;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.Contact;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.entity.enums.EmergencyNumbers;
import pl.urbansoft.rescue.domain.shared.event.CountryEvent;
import pl.urbansoft.rescue.domain.shared.event.LocationUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AlertContactFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AlertContactFragment extends Fragment {

    @Inject
    EventBus eventBus;
    @Inject
    UserService userService;
    @Inject
    PhoneService phoneService;
    @Inject
    LocationService locationService;
    @Inject
    AlertService alertService;

    Typeface RobotoRegular;
    Typeface RobotoBold;
    Typeface RobotoMedium;

    private static final String TAG = AlertContactFragment.class.getSimpleName();
    private static final String EMERGENCY_NUMBER = "emergency_number";
    private static final String EMERGENCY_DEFAULT_NUMBER = "112";
    private String emergencyNumber;
    private SharedPreferences sharedPreferences;

    ImageButton chooseContactButton;
    LinearLayout callForHelpClickableLayout;

    TextView chooseContactText;
    TextView callForHelpNumberTextView;

    public final static int PICK_CONTACT = 2022;

    public AlertContactFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AlertContactFragment.
     */
    public static AlertContactFragment newInstance() {
        AlertContactFragment fragment = new AlertContactFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);

        View v = inflater.inflate(R.layout.fragment_alert_contact, container, false);


        chooseContactButton = (ImageButton) v.findViewById(R.id.chooseContactButton);
        callForHelpClickableLayout = (LinearLayout) v.findViewById(R.id.callForHelpClickableLayout);

        chooseContactText = (TextView) v.findViewById(R.id.chooseContactText);
        callForHelpNumberTextView = (TextView) v.findViewById(R.id.callForHelpNumberTextView);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext());
        emergencyNumber = getLastOrDefaultNumber();
        initializeContactButton();

        callForHelpClickableLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPhone(emergencyNumber);
            }
        });

        RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");
        RobotoMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");

        setFonts(v);
        setCallForHelpNumber();
        return v;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
        setCallForHelpNumber();
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    private void initializeContactButton() {
        final User user = userService.getUser();
        if (user.getContact() != null && user.getContact().getNumber() != null && !"".equals(user.getContact().getNumber().trim())) {


            if (user.getContact().getName() != null) {
                chooseContactText.setText(user.getContact().getName().toUpperCase());
            }

            if(alertService.checkPermission(getActivity())) {
                chooseContactButton.setImageBitmap(
                        retrieveContactPhoto(getActivity(), user.getContact().getNumber()));

                chooseContactButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callPhone(userService.getUser().getContact().getNumber());
                    }
                });
            }

        } else {
            //no contact defined
            chooseContactButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                    i.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);

                    startActivityForResult(i, PICK_CONTACT);
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
            Uri contactUri = data.getData();
            Cursor cursor = getActivity().getContentResolver().query(contactUri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int number = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                int name = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
                String phoneName = cursor.getString(name);
                String phoneNumber = cursor.getString(number);

                cursor.close();
                if(alertService.checkPermission(getActivity())) {
                    chooseContactButton.setImageBitmap(retrieveContactPhoto(getActivity(), phoneNumber));

                    chooseContactButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callPhone(userService.getUser().getContact().getNumber());
                        }
                    });

                    if (phoneName != null) {
                        chooseContactText.setText(phoneName.toUpperCase());
                    }

                    userService.getUser().setContact(new Contact(phoneName, phoneNumber));
                    eventBus.post(new UserUpdatedEvent());
                }
            } else {
                Log.d(TAG, "no contact with given number found");
            }
        }

    }

    private void setCallForHelpNumber() {
        if (phoneService.isWifiConnected()) {
            locationService.startLocationUpdate(60);
        } else {
            callForHelpNumberTextView.setText(getLastOrDefaultNumber());
        }
    }

    private String getLastOrDefaultNumber() {
        return sharedPreferences.getString(EMERGENCY_NUMBER, EMERGENCY_DEFAULT_NUMBER);
    }

    public void onEvent(CountryEvent event) {
        locationService.stopLocationUpdate();
        emergencyNumber = EmergencyNumbers.getEmgergnecyNumberByCode(event.getCountry());
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(EMERGENCY_NUMBER, emergencyNumber);
        edit.commit();
        callForHelpNumberTextView.setText(emergencyNumber);
    }

    private void callPhone(String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        if (checkCallPermission()) {
            getActivity().startActivity(intent);
        } else {
            Log.d(TAG, "no permission to call");
        }
    }

    private boolean checkCallPermission() {

        String permission = "android.permission.CALL_PHONE";
        int res = getActivity().checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }


    private Bitmap retrieveContactPhoto(Context context, String number) {

        ContentResolver contentResolver = context.getContentResolver();
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor =
                contentResolver.query(
                        uri,
                        projection,
                        null,
                        null,
                        null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_person_enabled_op54);

        if (contactId != null) {
            try {
                InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(),
                        ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(contactId)));

                if (inputStream != null) {
                    photo = BitmapFactory.decodeStream(inputStream);
                    inputStream.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return photo;
    }

    private void setFonts(View v) {
        TextView chooseContactText = (TextView) v.findViewById(R.id.chooseContactText);
        TextView rescueServices = (TextView) v.findViewById(R.id.rescueServices);

        chooseContactText.setTypeface(RobotoMedium);
        rescueServices.setTypeface(RobotoMedium);
    }
}
