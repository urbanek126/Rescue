package pl.urbansoft.rescue.domain.shared.event;

import java.io.InputStream;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public class AlertAddedPhotoAttachmentEvent {
    private byte[] bData;
    private String name;
    private String contentType;

    public AlertAddedPhotoAttachmentEvent(byte[] bData, String name, String contentType) {
        this.bData = bData;
        this.name = name;
        this.contentType = contentType;
    }


    public String getName() {
        return name;
    }

    public byte[] getBData() {
        return bData;
    }

    public String getContentType() {
        return contentType;
    }
}
