package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public enum LocationInterval {
    VERY_FAST(60, 0),
    FAST(300, 1),
    SLOWLY(600, 2);

    private int value;
    private int position;

    private LocationInterval(int value, int position) {
        this.value = value;
        this.position = position;
    }

    public int getValue() {
        return value;
    }

    public int getPosition() {
        return position;
    }

    public static int getLocationIntervalPosition(int value) {

        if (value == LocationInterval.SLOWLY.getValue()) {
            return LocationInterval.SLOWLY.getPosition();
        } else if (value == LocationInterval.FAST.getValue()) {
            return LocationInterval.FAST.getPosition();
        } else {
            return LocationInterval.VERY_FAST.getPosition();
        }
    }
}
