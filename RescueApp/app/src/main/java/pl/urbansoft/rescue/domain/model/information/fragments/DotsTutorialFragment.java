package pl.urbansoft.rescue.domain.model.information.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import pl.urbansoft.rescue.R;

public class DotsTutorialFragment extends Fragment {
    private ImageView firstTutorialDot;
    private ImageView secondTutorialDot;
    private ImageView ThirdTutorialDot;
    private ImageView FourthTutorialDot;
    private ImageView fifthTutorialDot;
    private ImageView sixthTutorialDot;
    private ImageView seventhTutorialDot;
    private ImageView eighthTutorialDot;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tutorial_dots, container, false);

        setImageViewDots(v);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void setDot(int position) {
        switch (position) {
            case 0:
                setDots(true, false, false, false, false, false, false, false);
                break;
            case 1:
                setDots(false, true, false, false, false, false, false, false);
                break;
            case 2:
                setDots(false, false, true, false, false, false, false, false);
                break;
            case 3:
                setDots(false, false, false, true, false, false, false, false);
                break;
            case 4:
                setDots(false, false, false, false, true, false, false, false);
                break;
            case 5:
                setDots(false, false, false, false, false, true, false, false);
                break;
            case 6:
                setDots(false, false, false, false, false, false, true, false);
                break;
            case 7:
                setDots(false, false, false, false, false, false, false, true);
                break;
            default:
                break;
        }
    }

    private void setImageViewDots(View v) {
        firstTutorialDot = (ImageView) v.findViewById(R.id.first_tutorial_dot);
        secondTutorialDot = (ImageView) v.findViewById(R.id.second_tutorial_dot);
        ThirdTutorialDot = (ImageView) v.findViewById(R.id.third_tutorial_dot);
        FourthTutorialDot = (ImageView) v.findViewById(R.id.fourth_tutorial_dot);
        fifthTutorialDot = (ImageView) v.findViewById(R.id.fifth_tutorial_dot);
        sixthTutorialDot = (ImageView) v.findViewById(R.id.sixth_tutorial_dot);
        seventhTutorialDot = (ImageView) v.findViewById(R.id.seventh_tutorial_dot);
        eighthTutorialDot = (ImageView) v.findViewById(R.id.eighth_tutorial_dot);

        setDots(true, false, false, false, false, false, false, false);
    }

    private void setDots(boolean isFirstDot, boolean isSecondDot, boolean isThirdDot, boolean isFourthDot,
                         boolean isFifthDot, boolean isSixthDot, boolean isSeventhDot, boolean isEighthDot) {

        firstTutorialDot.setSelected(isFirstDot);
        secondTutorialDot.setSelected(isSecondDot);
        ThirdTutorialDot.setSelected(isThirdDot);
        FourthTutorialDot.setSelected(isFourthDot);
        fifthTutorialDot.setSelected(isFifthDot);
        sixthTutorialDot.setSelected(isSixthDot);
        seventhTutorialDot.setSelected(isSeventhDot);
        eighthTutorialDot.setSelected(isEighthDot);
    }

}
