package pl.urbansoft.rescue.domain.shared.event;

import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;

/**
 * Created by urbanek126 on 1/3/2016.
 */
public class AlertRespondedEvent {

    private int responseCode;
    private String responseMessage;
    private AlertInfoType alertParameter;

    public AlertRespondedEvent(int responseCode, String responseMessage, AlertInfoType alertParameter) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.alertParameter = alertParameter;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public AlertInfoType getAlertParameter() {
        return alertParameter;
    }
}
