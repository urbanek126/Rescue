package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public enum MessageInterval {
    VERY_FAST(60, 0),
    FAST(300, 1),
    SLOWLY(600, 2);

    private int value;
    private int position;

    private MessageInterval(int value, int position) {
        this.value = value;
        this.position = position;
    }

    public int getValue() {
        return value;
    }


    public int getPosition() {
        return position;
    }

    public static int getMessageIntervalPosition(int value) {

        if (value == MessageInterval.SLOWLY.getValue()) {
            return MessageInterval.SLOWLY.getPosition();
        } else if (value == MessageInterval.FAST.getValue()) {
            return MessageInterval.FAST.getPosition();
        } else {
            return MessageInterval.VERY_FAST.getPosition();
        }
    }
}


