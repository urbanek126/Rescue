package pl.urbansoft.rescue.domain.model.alert.ui.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.rey.material.widget.ImageButton;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToPreActiveEvent;
import pl.urbansoft.rescue.domain.service.UserService;



public class AlertInactiveFragment extends Fragment  {

    @Inject EventBus eventBus;
    @Inject UserService userService;

    Typeface RobotoRegular;
    Typeface RobotoBold;
    Typeface RobotoMedium;

    private static final String TAG = AlertInactiveFragment.class.getSimpleName();


    public static AlertInactiveFragment newInstance() {
        return new AlertInactiveFragment();
    }

    public AlertInactiveFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ((RescueApp)getActivity().getApplication()).getRescueComponent().inject(this);
        View v =  inflater.inflate(R.layout.fragment_inactive_alert, container, false);

        final ImageButton alertRescueBtn = (ImageButton) v.findViewById(R.id.alertRescueBtn);
        alertRescueBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertRescueBtn.setAlpha(0.7f);
                eventBus.post(new AlertChangedToPreActiveEvent(AlertChangedToPreActiveEvent.AlertType.BUTTON));
            }
        });

        RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");
        RobotoMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");

        setFonts(v);


        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void setFonts(View v) {
        TextView titleTextView = (TextView) v.findViewById(R.id.title);
        TextView alarmTextView = (TextView) v.findViewById(R.id.alarm);

        titleTextView.setTypeface(RobotoRegular);
        alarmTextView.setTypeface(RobotoRegular);
    }

}


