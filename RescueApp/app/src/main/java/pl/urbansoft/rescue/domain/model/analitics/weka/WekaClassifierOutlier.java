package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by ewa on 14/02/16.
 */
class WekaClassifierOutlier implements RescueClassifier {

    public double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifierOutlier.N3df093454(i);
        return p;
    }
    static double N3df093454(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 34.523403) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() > 34.523403) {
            p = WekaClassifierOutlier.Nbc50b1e5(i);
        }
        return p;
    }
    static double Nbc50b1e5(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 42.323151) {
            p = WekaClassifierOutlier.N7603a77c6(i);
        } else if (((Double) i[64]).doubleValue() > 42.323151) {
            p = 1;
        }
        return p;
    }
    static double N7603a77c6(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() <= 404.461102) {
            p = 1;
        } else if (((Double) i[0]).doubleValue() > 404.461102) {
            p = 0;
        }
        return p;
    }
}


