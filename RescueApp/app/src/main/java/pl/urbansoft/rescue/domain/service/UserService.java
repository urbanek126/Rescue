package pl.urbansoft.rescue.domain.service;

import android.util.Log;


import javax.inject.Inject;

import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.enums.DocumentType;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.infrastructure.repository.RescueRepository;


/**
 * Created by urbanek126 on 12/24/2015.
 */
public class UserService {
    @Inject RescueRepository repository;

    private static final String TAG = UserService.class.getSimpleName();
    private User user = null;

    public UserService(){
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
    }

    public User getUser(){
        if(user==null){
            initializeUser();
        }
        return user;
    }

    public void updateUser(){
        Log.i(TAG, "UserHandler:updateUser:User" + user);
        repository.saveOrUpdate(user);
    }

    private void initializeUser( ){
        user = (User) repository.getFirstDocumentByType(DocumentType.USER);
        if(user == null){
            user = new User();
            repository.saveOrUpdate(user);
        }
        Log.i(TAG, "UserHandler:UserInitializeEvent:user:" +user);
    }
}
