package pl.urbansoft.rescue.domain.model.alert.ui.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToInActiveEvent;
import pl.urbansoft.rescue.domain.service.UserService;

/**
 * Created by urbanek126 on 12/3/2015.
 */
public class AlertPreActiveFragment extends Fragment {

    private static final String TAG = AlertPreActiveFragment.class.getSimpleName();

    @Inject
    EventBus eventBus;
    @Inject
    UserService userService;

    Typeface RobotoRegular;
    Typeface RobotoBold;
    Typeface RobotoMedium;

    com.rey.material.widget.ImageButton alertButton;
    TextView counterView;
    CountDownTimer timer;

    public static AlertPreActiveFragment newInstance() {
        return new AlertPreActiveFragment();
    }

    public AlertPreActiveFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);

        View v = inflater.inflate(R.layout.fragment_preactive_alert, container, false);
        alertButton = (com.rey.material.widget.ImageButton) v.findViewById(R.id.alertCancelBtn);
        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertButton.setAlpha(0.7f);

                eventBus.post(new AlertChangedToInActiveEvent());
            }
        });
        counterView = (TextView) v.findViewById(R.id.counterView);

        int cancelDuration = userService.getUser().getCancelDuration(); //in seconds
        counterView.setText(""+cancelDuration+" s");

        timer = new CountDownTimer(cancelDuration*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                counterView.setText(  millisUntilFinished / 1000+ " s");
            }

            public void onFinish() {
                eventBus.post(new AlertChangedToActiveEvent());
            }
        }.start();

        RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");
        RobotoMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");

        setFonts(v);
        setAlertPreActiveAnim(v);

        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    public void onEvent(AlertChangedToInActiveEvent e) {
        if (timer != null){
            setAlertInActiveAnim();
            timer.cancel();
        }
    }

    private void setAlertInActiveAnim(){
        Animation zoomOut = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.zoom_out);
        alertButton.startAnimation(zoomOut);
    }

    private void setAlertPreActiveAnim(View v){
        View lineDividerView = v.findViewById(R.id.lineDividerView);
        Animation moveFromRightAnim = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.move_from_right);
        lineDividerView.startAnimation(moveFromRightAnim);

        Animation zoomIn = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.zoom_in);
        alertButton.startAnimation(zoomIn);
    }

    private void setFonts(View v) {
        TextView titleTextView = (TextView) v.findViewById(R.id.title);
        TextView counterViewTextView = (TextView) v.findViewById(R.id.counterView);
        TextView cancelViewTextView = (TextView) v.findViewById(R.id.cancelView);


        titleTextView.setTypeface(RobotoRegular);
        counterViewTextView.setTypeface(RobotoRegular);
        cancelViewTextView.setTypeface(RobotoRegular);
    }
}
