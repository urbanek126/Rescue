package pl.urbansoft.rescue.interfaces.broadcast;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToInActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToPreActiveEvent;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.infrastructure.bus.BusModule;

/**
 * Created by andrzej on 12/27/15.
 */
public class HeadPhoneReaciver extends BroadcastReceiver {

    private static final String TAG = HeadPhoneReaciver.class.getSimpleName();

    private  static final int HEADPHONES_DISCONNECTED = 0;
    private Timer scheduleAlarm = null;
    private boolean isActivated=false;

    @Inject
    UserService userService;

    public HeadPhoneReaciver() {
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int state = intent.getIntExtra("state", -1);

        Log.i(TAG, "Broadcast recived with action" + intent.getAction() + " and state: " + state);

        User user = userService.getUser();
        if(user.isHeadPhonePluginAlert()) {
            if (isActivated) {
                if (state == HEADPHONES_DISCONNECTED) {

                    KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);

                    if (myKM.inKeyguardRestrictedInputMode()) {
                        //screen is locked
                        scheduleAlarm = new Timer();

                        scheduleAlarm.schedule(new TimerTask() {
                            @Override
                            public void run() {
                                Log.i(TAG, "Alarm brodcasted - headphone");

                                BusModule.getEventBusInstance().post((new AlertChangedToPreActiveEvent(AlertChangedToPreActiveEvent.AlertType.HEAD_PHONE)));
                            }
                        }, 10000);

                    }
                } else {
                    //cancel alarm
                    if (scheduleAlarm != null) {
                        scheduleAlarm.cancel();
                        BusModule.getEventBusInstance().post((new AlertChangedToInActiveEvent()));
                    }
                }
            } else {
                if (state != HEADPHONES_DISCONNECTED) {
                    isActivated = true;
                }
            }
        }
        else{
            Log.i(TAG, "HeadPhone alert is not enabled");
        }
    }
}
