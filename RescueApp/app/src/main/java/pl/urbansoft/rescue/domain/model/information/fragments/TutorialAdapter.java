package pl.urbansoft.rescue.domain.model.information.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by urbanek126 on 5/1/2016.
 */
public class TutorialAdapter extends FragmentPagerAdapter {
    private static final int NUMBER_OF_PAGES = 8;

    public TutorialAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return NUMBER_OF_PAGES;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new FirstTutorialFragment();
            case 1:
                return new SecondTutorialFragment();
            case 2:
                return new ThirdTutorialFragment();
            case 3:
                return new FourthTutorialFragment();
            case 4:
                return new FifthTutorialFragment();
            case 5:
                return new SixthTutorialFragment();
            case 6:
                return new SeventhTutorialFragment();
            case 7:
                return new EigthTutorialFragment();
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }
}
