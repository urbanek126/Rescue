package pl.urbansoft.rescue.domain.service;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;
import pl.urbansoft.rescue.domain.shared.entity.enums.ContentType;
import pl.urbansoft.rescue.domain.shared.entity.enums.EventCode;
import pl.urbansoft.rescue.domain.shared.event.AlertAddedPhotoAttachmentEvent;
import pl.urbansoft.rescue.domain.shared.event.AlertRespondedEvent;


public class CameraServiceOld extends CameraService{
    private static final String TAG = CameraServiceOld.class.getSimpleName();

    AlertService alertService;
    EventBus eventbus;

    private static final int BACK_CAMERA = 0;
    private static final int FRONT_CAMERA = 1;
    private ScheduledExecutorService service;

    private static Map<Integer, Runnable> cameraRunnalbes = new HashMap<>();

    public CameraServiceOld(AlertService alertServic, EventBus eventbus){
        this.alertService = alertServic;
        this.eventbus = eventbus;
    }

    public static boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            // tutaj w ustawianich
            return true;
        } else {
            return false;
        }
    }

    public void startTakingPhotos(int cameraInterval) {
        if (service == null || service.isShutdown()) {
            service = Executors.newSingleThreadScheduledExecutor();
        }
        try {
            Runnable backCameraRunnable = getCameraRunnable(BACK_CAMERA, new CameraCallbak());
            service.scheduleAtFixedRate(backCameraRunnable, 0, cameraInterval, TimeUnit.SECONDS);

            if (Camera.getNumberOfCameras() > 1) {
                Runnable frontCameraRunnable = getCameraRunnable(FRONT_CAMERA, new CameraCallbak());
                service.scheduleAtFixedRate(frontCameraRunnable, cameraInterval / 2, cameraInterval, TimeUnit.SECONDS);
            }

        } catch (Exception e) {
            Log.e(TAG, "CameraServiceOld issue for camera");
            e.printStackTrace();
            eventbus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), e.getMessage(), AlertInfoType.CAMERA));
        }
    }

    public void stopTakingPhotos() {
        if(service!=null && !service.isShutdown())
            service.shutdown();
    }
    public class CameraCallbak implements Camera.PictureCallback {
        @Override
        public void onPictureTaken(byte[] bData, Camera camera) {
            camera.release();
            Log.i(TAG, "CameraServiceOld:initializeCameraCallbacks():onPictureTaken");
            eventbus.post(new AlertAddedPhotoAttachmentEvent(bData,createPhotoName(), ContentType.JPEG.getType()));
        }
    }
    public  Runnable getCameraRunnable(final int camerNumber, final CameraServiceOld.CameraCallbak cameraCallbak){

        Runnable cameraRunnable = new Runnable() {
            public void run() {
                Log.i(TAG, "startTakingPhotos");

                Camera camera = Camera.open(camerNumber);
                camera.startPreview();
                camera.takePicture(null, null, cameraCallbak);
           }

            public void finalize(){
                Log.i(TAG, "backCameraRunnable finalize");
            }
        };
        cameraRunnalbes.put(camerNumber, cameraRunnable);

        return cameraRunnable;
    }

}


