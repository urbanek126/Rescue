package pl.urbansoft.rescue.infrastructure.ble.event;

import pl.urbansoft.rescue.domain.shared.event.DomainEvent;

/**
 * Created by andrzej on 11/28/15.
 */
public abstract class BleAbstractEvent implements DomainEvent{

    private BleDevice device;

    public BleAbstractEvent( BleDevice device) {
        this.device = device;
    }

    public BleDevice getDevice(){
        return  device;
    }
}
