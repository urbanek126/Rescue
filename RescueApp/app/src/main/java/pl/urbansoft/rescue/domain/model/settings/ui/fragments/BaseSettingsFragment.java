package pl.urbansoft.rescue.domain.model.settings.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;
import com.rey.material.widget.Spinner;
import com.rey.material.widget.Switch;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.IconBarFragment;
import pl.urbansoft.rescue.domain.model.settings.ui.SettingsActivity;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.entity.enums.CameraInterval;
import pl.urbansoft.rescue.domain.shared.entity.enums.LocationInterval;
import pl.urbansoft.rescue.domain.shared.entity.enums.MessageInterval;
import pl.urbansoft.rescue.domain.shared.entity.enums.MicrophoneDuration;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.entity.Contact;
import pl.urbansoft.rescue.domain.service.UserService;


public class BaseSettingsFragment extends Fragment {
    @Inject
    EventBus eventBus;

    @Inject
    UserService userService;

    private static final String TAG = BaseSettingsFragment.class.getSimpleName();
    public final int PICK_CONTACT = 2015;
    private View v;

    EditText contactNameEditText;
    EditText contactNumberEditText;
    Switch microphoneSwitch;
    ImageView microphoneImage;
    TextView microphoneText;
    Spinner microphoneSpinner;
    Switch cameraSwitch;
    ImageView cameraImage;
    TextView cameraText;
    Spinner cameraSpinner;
    Switch locationSwitch;
    ImageView locationImage;
    TextView locationText;
    Spinner locationSpinner;
    Switch messageSwitch;
    ImageView messageImage;
    TextView messageText;
    Spinner messageSpinner;
    TextView messageContentTextView;
    Button changeMessageContentBtn;
    Button newContactBtn;
    LayoutInflater li;
    View mcedfView;
    EditText messageContenEditText;
    TextView cancelBtn;
    TextView acceptBtn;
    TextView messageContentDialgoTitle;
    Typeface RobotoRegular;
    Typeface RobotoBold;
    Typeface RobotoMedium;

    View mcedfPhoneView;
    TextView messageContenPhoneViewText;
    TextView messageContenNameViewText;
    TextView cancelPhoneBtn;
    TextView acceptPhoneBtn;
    TextView messageContentDialgPhoneTitle;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_settings_base, container, false);
        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);

        contactNameEditText = (EditText) v.findViewById(R.id.contactNameEditText);
        contactNumberEditText = (EditText) v.findViewById(R.id.contactNumberEditText);
        microphoneSwitch = (Switch) v.findViewById(R.id.microphoneSwitch);
        microphoneImage = (ImageView) v.findViewById(R.id.microphoneImage);
        microphoneText = (TextView) v.findViewById(R.id.microphoneText);
        microphoneSpinner = (Spinner) v.findViewById(R.id.microphoneSpinner);
        cameraSwitch = (Switch) v.findViewById(R.id.cameraSwitch);
        cameraImage = (ImageView) v.findViewById(R.id.cameraImage);
        cameraText = (TextView) v.findViewById(R.id.cameraText);
        cameraSpinner = (Spinner) v.findViewById(R.id.cameraSpinner);
        locationSwitch = (Switch) v.findViewById(R.id.locationSwitch);
        locationImage = (ImageView) v.findViewById(R.id.locationImage);
        locationText = (TextView) v.findViewById(R.id.locationText);
        locationSpinner = (Spinner) v.findViewById(R.id.locationSpinner);
        messageSwitch = (Switch) v.findViewById(R.id.messageSwitch);
        messageImage = (ImageView) v.findViewById(R.id.messageImage);
        messageText = (TextView) v.findViewById(R.id.messageText);
        messageSpinner = (Spinner) v.findViewById(R.id.messageSpinner);
        messageContentTextView = (TextView) v.findViewById(R.id.messageContenTextView);
        changeMessageContentBtn = (Button) v.findViewById(R.id.changeMessageContentBtn);
        newContactBtn = (Button) v.findViewById(R.id.newContactBtn);

        RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");
        RobotoMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");
        initializeComponents();
        setMicrophoneOptions();
        setCameraOptions();
        setMessageOptions();
        setAddContactOption();
        setLocationOptions();
        setMessageContentOption();
        setFonts();

        eventBus.register(this);

        return v;
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        eventBus.unregister(this);
    }

    @Override
    public void onResume(){
        super.onResume();

        final User user = userService.getUser();

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isCameraPermissionTakenBack(user)){
                    user.setIsCamera(false);
                    cameraSwitch.setChecked(false);
                }

                if (isLocationPermissionTakenBack(user)){
                    user.setIsLocation(false);
                    locationSwitch.setChecked(false);
                }

                if (isSendSMSPermissionTakenBack(user)) {
                    user.setIsMessage(false);
                    messageSwitch.setChecked(false);
                }

                if (isMicrophonePermissionTakenBack(user)) {
                    user.setIsMicrophone(false);
                    microphoneSwitch.setChecked(false);
                }

                setIconActive(RescueApp.CAMERA);
                setIconActive(RescueApp.LOCATION);
                setIconActive(RescueApp.MESSAGE);
                setIconActive(RescueApp.MICROPHONE);
            }
        }).start();
    }

    public void onEvent(UserExternalButtonUpdatedEvent event) {
        ((SettingsActivity) getActivity()).onEvent(event);
        LinearLayout BaseSettingsView = (LinearLayout) v.findViewById(R.id.BaseSettingsView);
        if (event.isExteranlButtonUsed()) {
            BaseSettingsView.setPadding(0, 0, 0, (int) getResources().getDimension(R.dimen.dimen_xxlarge));
        } else {
            BaseSettingsView.setPadding(0, 0, 0, 0);
        }
    }

    private void setMicrophoneOptions() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.microphone_duration_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        microphoneSpinner.setAdapter(adapter);
        microphoneSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                int value = MicrophoneDuration.values()[position].getValue();
                userService.getUser().setMicrophoneDuration(value);
                eventBus.post(new UserUpdatedEvent());
            }
        });

        microphoneSwitch.setOnTouchListener(getMicrophoneOnTouchListener());
    }

    private void setCameraOptions() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.camera_interval_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cameraSpinner.setAdapter(adapter);
        cameraSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                System.out.println(position);
                int value = CameraInterval.values()[position].getValue();
                userService.getUser().setCameraInterval(value);
                eventBus.post(new UserUpdatedEvent());
            }
        });

        cameraSwitch.setOnTouchListener(getCameraOnTouchListener());
    }

    private void setLocationOptions() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.location_interval_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locationSpinner.setAdapter(adapter);
        locationSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                int value = LocationInterval.values()[position].getValue();
                userService.getUser().setLocationInterval(value);
                eventBus.post(new UserUpdatedEvent());
            }
        });

        locationSwitch.setOnTouchListener(getLocationOnTouchListener());
    }

    private void setMessageOptions() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.message_interval_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        messageSpinner.setAdapter(adapter);
        messageSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner parent, View view, int position, long id) {
                int value = MessageInterval.values()[position].getValue();
                userService.getUser().setMessageInterval(value);
                eventBus.post(new UserUpdatedEvent());
            }
        });

        messageSwitch.setOnTouchListener(getMessageOnTouchListener());
    }

    private void setMessageContentOption() {
        changeMessageContentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createMessageContentEditorDialog();
            }
        });
    }

    private void createMessageContentEditorDialog() {
        li = LayoutInflater.from(getContext());
        mcedfView = li.inflate(R.layout.dialog_editor_content_message, null);
        messageContenEditText = (EditText) mcedfView.findViewById(R.id.messageContenEditText);
        cancelBtn = (TextView) mcedfView.findViewById(R.id.cancelBtn);
        acceptBtn = (TextView) mcedfView.findViewById(R.id.acceptBtn);
        messageContentDialgoTitle = (TextView) mcedfView.findViewById(R.id.messageContentDialgoTitle);
        messageContenEditText.setText(messageContentTextView.getText().toString());
        messageContenEditText.setSelection(messageContenEditText.getText().length());
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(mcedfView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newMessage = messageContenEditText.getText().toString();
                messageContentTextView.setText(newMessage);
                userService.getUser().setMessageContent(newMessage);
                eventBus.post(new UserUpdatedEvent());
                alertDialog.dismiss();
            }
        });
        setDialogFonts();
        alertDialog.show();
    }


    private void confirmPhoneDialog(final String phoneName, final String phoneNumber) {

        li = LayoutInflater.from(getContext());
        mcedfPhoneView = li.inflate(R.layout.dialog_confirm_phone, null);
        messageContenPhoneViewText = (TextView) mcedfPhoneView.findViewById(R.id.messageContenPhoneViewText);
        messageContenNameViewText = (TextView) mcedfPhoneView.findViewById(R.id.messageContenNameViewText);

        cancelPhoneBtn = (TextView) mcedfPhoneView.findViewById(R.id.cancelBtn);
        acceptPhoneBtn = (TextView) mcedfPhoneView.findViewById(R.id.acceptBtn);

        messageContentDialgPhoneTitle = (TextView) mcedfPhoneView.findViewById(R.id.messageContentPhoneDialgoTitle);

        messageContenPhoneViewText.setText(phoneNumber);
        messageContenNameViewText.setText(phoneName);

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setView(mcedfPhoneView);
        final AlertDialog alertDialog = alertDialogBuilder.create();

        cancelPhoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        acceptPhoneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactNameEditText.setText(phoneName);
                contactNumberEditText.setText(phoneNumber);
                userService.getUser().setContact(new Contact(phoneName, phoneNumber));
                eventBus.post(new UserUpdatedEvent());

                alertDialog.dismiss();
            }
        });
        messageContenPhoneViewText.setTypeface(RobotoMedium);
        messageContenNameViewText.setTypeface(RobotoMedium);

        messageContentDialgPhoneTitle.setTypeface(RobotoMedium);
        acceptPhoneBtn.setTypeface(RobotoMedium);
        cancelPhoneBtn.setTypeface(RobotoMedium);

        alertDialog.show();
    }

    private void setAddContactOption() {
        contactNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userService.getUser().setContact(new Contact(contactNameEditText.getText().toString(), userService.getUser().getContact().getNumber()));
                eventBus.post(new UserUpdatedEvent());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        contactNumberEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                userService.getUser().setContact(new Contact(userService.getUser().getContact().getName(), contactNumberEditText.getText().toString()));
                eventBus.post(new UserUpdatedEvent());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        newContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
                startActivityForResult(i, PICK_CONTACT);
            }
        });
    }

    private void initializeComponents() {
        User user = userService.getUser();

        microphoneSwitch.setChecked(user.isMicrophone());
        cameraSwitch.setChecked(user.isCamera());
        locationSwitch.setChecked(user.isLocation());
        messageSwitch.setChecked(user.isMessage());

        setIconActive(RescueApp.MICROPHONE);
        setIconActive(RescueApp.CAMERA);
        setIconActive(RescueApp.LOCATION);
        setIconActive(RescueApp.MESSAGE);

        contactNameEditText.setText(user.getContact().getName());
        contactNumberEditText.setText(user.getContact().getNumber());

        cameraSpinner.setSelection(CameraInterval.getCameraIntervalPosition(user.getCameraInterval()));
        messageSpinner.setSelection(MessageInterval.getMessageIntervalPosition(user.getMessageInterval()));
        microphoneSpinner.setSelection(MicrophoneDuration.getMicrophoneDurationPosition(user.getMicrophoneDuration()));
        locationSpinner.setSelection(LocationInterval.getLocationIntervalPosition(user.getLocationInterval()));
        messageContentTextView.setText(user.getMessageContent());
    }

    private void setFonts() {
        TextView alarmParametresSectionTextView = (TextView) v.findViewById(R.id.alarmParametresSectionTextView);
        TextView contactSectionTextView = (TextView) v.findViewById(R.id.contactSectionTextView);
        TextView messageSectionTextView = (TextView) v.findViewById(R.id.messageSectionTextView);
        TextView messageContenTextView = (TextView) v.findViewById(R.id.messageContenTextView);

        alarmParametresSectionTextView.setTypeface(RobotoBold);
        contactSectionTextView.setTypeface(RobotoBold);
        messageSectionTextView.setTypeface(RobotoBold);
        microphoneText.setTypeface(RobotoRegular);
        locationText.setTypeface(RobotoRegular);
        cameraText.setTypeface(RobotoRegular);
        messageText.setTypeface(RobotoRegular);
        contactNameEditText.setTypeface(RobotoMedium);
        contactNumberEditText.setTypeface(RobotoMedium);
        messageContenTextView.setTypeface(RobotoMedium);
        newContactBtn.setTypeface(RobotoMedium);
        changeMessageContentBtn.setTypeface(RobotoMedium);

    }

    private void setDialogFonts() {
        messageContenEditText.setTypeface(RobotoMedium);
        messageContentDialgoTitle.setTypeface(RobotoMedium);
        cancelBtn.setTypeface(RobotoMedium);
        acceptBtn.setTypeface(RobotoMedium);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_CONTACT && resultCode == Activity.RESULT_OK) {
            Uri contactUri = data.getData();
            Cursor cursor = getActivity().getContentResolver().query(contactUri,
                    null,
                    null,
                    null,
                    null);

            cursor.moveToLast();

            int number = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int name = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            String phoneName = cursor.getString(name);
            String phoneNumber = cursor.getString(number);

            confirmPhoneDialog(phoneName, phoneNumber);

            Log.i(TAG, "BaseSettingsFragment:onActivityResult():number name " + phoneName + phoneNumber);
        }
    }

    public void setIconActive(String name){
        if(isIconActive(name)){
            getIcon(name).setAlpha(1f);
            getTextView(name).setAlpha((float) 1);
            getSpinner(name).setVisibility(View.VISIBLE);
        }
        else {
            getIcon(name).setAlpha(0.5f);
            getTextView(name).setAlpha((float) 0.5);
            getSpinner(name).setVisibility(View.INVISIBLE);
        }
    }

    private Spinner getSpinner(String name){
        if(name.equals(RescueApp.CAMERA))
            return cameraSpinner;
        else if(name.equals(RescueApp.LOCATION))
            return locationSpinner;
        else if(name.equals(RescueApp.MESSAGE))
            return messageSpinner;
        else if(name.equals(RescueApp.MICROPHONE))
            return microphoneSpinner;
        else
            return null;
    }

    private TextView getTextView(String name){
        if(name.equals(RescueApp.CAMERA))
            return cameraText;
        else if(name.equals(RescueApp.LOCATION))
            return locationText;
        else if(name.equals(RescueApp.MESSAGE))
            return messageText;
        else if(name.equals(RescueApp.MICROPHONE))
            return microphoneText;
        else
            return null;
    }

    private ImageView getIcon(String name){
        if(name.equals(RescueApp.CAMERA))
            return cameraImage;
        else if(name.equals(RescueApp.LOCATION))
            return locationImage;
        else if(name.equals(RescueApp.MESSAGE))
            return messageImage;
        else if(name.equals(RescueApp.MICROPHONE))
            return microphoneImage;
        else
            return null;
    }

    private boolean isIconActive(String name){
        User user = userService.getUser();

        if(name.equals(RescueApp.CAMERA))
            return user.isCamera();
        else if(name.equals(RescueApp.LOCATION))
            return user.isLocation();
        else if(name.equals(RescueApp.MESSAGE))
            return user.isMessage();
        else if(name.equals(RescueApp.MICROPHONE))
            return user.isMicrophone();
        else
            return false;
    }

    private void requestPermission(String [] permission, int requestCode) {
        ActivityCompat.requestPermissions(getActivity(), permission, requestCode);
    }

    private Switch getSwitch(String name){
        if(name.equals(RescueApp.CAMERA))
            return cameraSwitch;
        else if(name.equals(RescueApp.LOCATION))
            return locationSwitch;
        else if(name.equals(RescueApp.MESSAGE))
            return messageSwitch;
        else if(name.equals(RescueApp.MICROPHONE))
            return microphoneSwitch;
        else
            return null;
    }

    public void onEvent(UserUpdatedEvent e) {
        if(!e.getName().isEmpty()) {
            getSwitch(e.getName()).setChecked(true);
            setIconActive(e.getName());
        }
    }

    private View.OnTouchListener getMicrophoneOnTouchListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    String permission = Manifest.permission.RECORD_AUDIO;

                    if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {
                        boolean isChecked = userService.getUser().isMicrophone();

                        userService.getUser().setIsMicrophone(!isChecked);
                        microphoneSwitch.setChecked(!isChecked);

                        setIconActive(RescueApp.MICROPHONE);
                        eventBus.post(new UserUpdatedEvent());
                    } else {
                        requestPermission(new String[]{permission}, AlertActivity.MICROPHONE_PERMISSION_REQUEST);
                        microphoneSwitch.setChecked(false);
                    }
                }

                return true;
            }
        };
    }

    private View.OnTouchListener getCameraOnTouchListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    String permission = Manifest.permission.CAMERA;

                    if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {
                        boolean isChecked = userService.getUser().isCamera();

                        userService.getUser().setIsCamera(!isChecked);
                        cameraSwitch.setChecked(!isChecked);

                        setIconActive(RescueApp.CAMERA);
                        eventBus.post(new UserUpdatedEvent());
                    } else {
                        requestPermission(new String[]{permission}, AlertActivity.CAMERA_PERMISSION_REQUEST);
                        cameraSwitch.setChecked(false);
                    }
                }

                return true;
            }
        };
    }

    private View.OnTouchListener getMessageOnTouchListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    String permission = Manifest.permission.SEND_SMS;

                    if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED) {
                        boolean isChecked = userService.getUser().isMessage();

                        userService.getUser().setIsMessage(!isChecked);
                        messageSwitch.setChecked(!isChecked);

                        setIconActive(RescueApp.MESSAGE);
                        eventBus.post(new UserUpdatedEvent());
                    } else {
                        requestPermission(new String[]{permission}, AlertActivity.SEND_SMS_PERMISSION_REQUEST);
                        messageSwitch.setChecked(false);
                    }
                }

                return true;
            }
        };
    }

    private View.OnTouchListener getLocationOnTouchListener() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    String permission = Manifest.permission.ACCESS_COARSE_LOCATION;
                    String permission2 = Manifest.permission.ACCESS_FINE_LOCATION;

                    if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), permission2) == PackageManager.PERMISSION_GRANTED){
                        boolean isChecked = userService.getUser().isLocation();

                        userService.getUser().setIsLocation(!isChecked);
                        locationSwitch.setChecked(!isChecked);

                        setIconActive(RescueApp.LOCATION);
                        eventBus.post(new UserUpdatedEvent());
                    } else {
                        requestPermission(new String[]{permission, permission2}, AlertActivity.LOCATION_PERMISSION_REQUEST);
                        locationSwitch.setChecked(false);
                    }
                }

                return true;
            }
        };
    }

    private boolean isMicrophonePermissionTakenBack(User user){
        return user.isMicrophone() && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean isSendSMSPermissionTakenBack(User user){
        return user.isMessage() && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean isCameraPermissionTakenBack(User user){
        return user.isCamera() && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean isLocationPermissionTakenBack(User user){
        return user.isLocation() && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }
}
