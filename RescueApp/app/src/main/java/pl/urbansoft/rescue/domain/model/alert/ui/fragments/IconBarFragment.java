package pl.urbansoft.rescue.domain.model.alert.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link IconBarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IconBarFragment extends Fragment {

    @Inject
    EventBus eventBus;
    @Inject
    UserService userService;

    ImageView microphoneImage;
    ImageView cameraImage;
    ImageView locationImage;
    ImageView emailImage;


    private static final String TAG = IconBarFragment.class.getSimpleName();

    public IconBarFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment IconBarFragment.
     */
    public static IconBarFragment newInstance() {
        IconBarFragment fragment = new IconBarFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);

        View v = inflater.inflate(R.layout.fragment_icon_bar, container, false);

        microphoneImage = (ImageView) v.findViewById(R.id.microphoneView);
        cameraImage = (ImageView) v.findViewById(R.id.photoView);
        locationImage = (ImageView) v.findViewById(R.id.pointView);
        emailImage = (ImageView) v.findViewById(R.id.mailView);

        initializeIcons();
        eventBus.register(this);

        return v;
    }

    @Override
    public void onResume(){
        super.onResume();

        final User user = userService.getUser();

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (isCameraPermissionTakenBack(user))
                    user.setIsCamera(false);

                if (isLocationPermissionTakenBack(user))
                    user.setIsLocation(false);

                if (isSendSMSPermissionTakenBack(user))
                    user.setIsMessage(false);

                if (isMicrophonePermissionTakenBack(user))
                    user.setIsMicrophone(false);

                setIconActive(RescueApp.CAMERA);
                setIconActive(RescueApp.LOCATION);
                setIconActive(RescueApp.MESSAGE);
                setIconActive(RescueApp.MICROPHONE);
            }
        }).start();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        eventBus.unregister(this);
    }

    public void onEvent(UserUpdatedEvent e) {
        if(!e.getName().isEmpty())
            setIconActive(e.getName());
    }

    public void setIconActive(String name){
        if(isIconActive(name))
            getIcon(name).setAlpha(1f);
        else
            getIcon(name).setAlpha(0.5f);
    }

    private ImageView getIcon(String name){
        if(name.equals(RescueApp.CAMERA))
            return cameraImage;
        else if(name.equals(RescueApp.LOCATION))
            return locationImage;
        else if(name.equals(RescueApp.MESSAGE))
            return emailImage;
        else if(name.equals(RescueApp.MICROPHONE))
            return microphoneImage;
        else
            return null;
    }

    private boolean isIconActive(String name){
        User user = userService.getUser();

        if(name.equals(RescueApp.CAMERA))
            return user.isCamera();
        else if(name.equals(RescueApp.LOCATION))
            return user.isLocation();
        else if(name.equals(RescueApp.MESSAGE))
            return user.isMessage();
        else if(name.equals(RescueApp.MICROPHONE))
            return user.isMicrophone();
        else
            return false;
    }

    private void initializeIcons() {

        cameraImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = userService.getUser().isCamera();

                String permission = Manifest.permission.CAMERA;

                if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED){
                    if (isChecked)
                        userService.getUser().setIsCamera(false);
                    else
                        userService.getUser().setIsCamera(true);

                    setIconActive(RescueApp.CAMERA);

                    eventBus.post(new UserUpdatedEvent());
                }
                else{
                    requestPermission(new String[]{permission}, AlertActivity.CAMERA_PERMISSION_REQUEST);
                }
            }
        });

        microphoneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = userService.getUser().isMicrophone();

                String permission = Manifest.permission.RECORD_AUDIO;

                if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED){
                    if (isChecked)
                        userService.getUser().setIsMicrophone(false);
                    else
                        userService.getUser().setIsMicrophone(true);

                    setIconActive(RescueApp.MICROPHONE);

                    eventBus.post(new UserUpdatedEvent());
                }
                else{
                    requestPermission(new String[]{permission}, AlertActivity.MICROPHONE_PERMISSION_REQUEST);
                }
            }
        });

        emailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = userService.getUser().isMessage();

                String permission = Manifest.permission.SEND_SMS;

                if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED){
                    if (isChecked)
                        userService.getUser().setIsMessage(false);
                    else
                        userService.getUser().setIsMessage(true);

                    setIconActive(RescueApp.MESSAGE);

                    eventBus.post(new UserUpdatedEvent());
                }
                else{
                    requestPermission(new String[]{permission}, AlertActivity.SEND_SMS_PERMISSION_REQUEST);
                }
            }
        });

        locationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = userService.getUser().isLocation();

                String permission = Manifest.permission.ACCESS_COARSE_LOCATION;
                String permission2 = Manifest.permission.ACCESS_FINE_LOCATION;

                if (ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(getActivity(), permission2) == PackageManager.PERMISSION_GRANTED){
                    if (isChecked)
                        userService.getUser().setIsLocation(false);
                    else
                        userService.getUser().setIsLocation(true);

                    setIconActive(RescueApp.LOCATION);

                    eventBus.post(new UserUpdatedEvent());
                }
                else{
                    requestPermission(new String[]{permission, permission2}, AlertActivity.LOCATION_PERMISSION_REQUEST);
                }
            }
        });
    }

    private void requestPermission(String [] permission, int requestCode) {
        ActivityCompat.requestPermissions(getActivity(), permission, requestCode);
    }

    private boolean isMicrophonePermissionTakenBack(User user){
        return user.isMicrophone() && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean isSendSMSPermissionTakenBack(User user){
        return user.isMessage() && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean isCameraPermissionTakenBack(User user){
        return user.isCamera() && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    private boolean isLocationPermissionTakenBack(User user){
        return user.isLocation() && (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED);
    }
}
