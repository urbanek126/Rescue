package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by andrzej on 11/15/15.
 */
public class WekaClassifierSensitive implements RescueClassifier {
    public double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifierSensitive.N4ebded0b2(i);
        return p;
    }
    static double N4ebded0b2(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 150.338013) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 150.338013) {
            p = 1;
        }
        return p;
    }
}

