package pl.urbansoft.rescue.domain.shared.ui.fragments;

/**
 * Created by urbanek126 on 12/28/2015.
 */

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.infrastructure.ble.HtcTagHandler;
import pl.urbansoft.rescue.infrastructure.ble.event.BleDevice;
import pl.urbansoft.rescue.infrastructure.ble.event.HtcTagConnectEvent;

/**
 * Created by urbanek126 on 12/21/2015.
 */
public class BleFragment extends Fragment {

    TextView bleConnectStatusTextView;

    public static BleFragment newInstance() {
        BleFragment fragment = new BleFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);
        View v = inflater.inflate(R.layout.fragment_ble, container, false);
        bleConnectStatusTextView = (TextView) v.findViewById(R.id.bleConnectStatusTextView);

        setFonts();
        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setFonts() {
        Typeface RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        bleConnectStatusTextView.setTypeface(RobotoRegular);
    }
}
