package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by ewa on 14/02/16.
 */
class WekaClassifierOutlierExtreme implements RescueClassifier {



        public double classify(Object[] i)
                throws Exception {

            double p = Double.NaN;
            p = WekaClassifierOutlierExtreme.N7b08a74a18(i);
            return p;
        }
        static double N7b08a74a18(Object []i) {
            double p = Double.NaN;
            if (i[64] == null) {
                p = 0;
            } else if (((Double) i[64]).doubleValue() <= 46.715377) {
                p = 0;
            } else if (((Double) i[64]).doubleValue() > 46.715377) {
                p = WekaClassifierOutlierExtreme.N7b1b3aab19(i);
            }
            return p;
        }
        static double N7b1b3aab19(Object []i) {
            double p = Double.NaN;
            if (i[64] == null) {
                p = 0;
            } else if (((Double) i[64]).doubleValue() <= 50.108751) {
                p = 0;
            } else if (((Double) i[64]).doubleValue() > 50.108751) {
                p = 1;
            }
            return p;
        }
    }

