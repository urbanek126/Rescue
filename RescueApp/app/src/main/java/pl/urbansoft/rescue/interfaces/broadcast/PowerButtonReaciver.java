package pl.urbansoft.rescue.interfaces.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.util.Log;

import javax.inject.Inject;

import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToPreActiveEvent;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.infrastructure.bus.BusModule;

/**
 * Created by andrzej on 12/26/15.
 */
public class PowerButtonReaciver extends BroadcastReceiver {

    private int count = 0;
    private long timeLastOccoured = 0;

    private static final String TAG = PowerButtonReaciver.class.getSimpleName();

    @Inject
    UserService userService;

    public PowerButtonReaciver() {
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Broadcast recived with action" + intent.getAction() + " and count:" + count);

        User user = userService.getUser();
        if(user.isPowerDeviceButtonAlert()) {
            long timeOccured = System.currentTimeMillis();

            if (timeLastOccoured == 0 || timeOccured - timeLastOccoured < 1000) {
                count = count + 1;
            } else {
                count = 0;
            }
            timeLastOccoured = timeOccured;

            if (count == 8) {
                Log.i(TAG, "Alarm brodcasted - power button");
                BusModule.getEventBusInstance().post( new AlertChangedToPreActiveEvent(AlertChangedToPreActiveEvent.AlertType.POWER_BUTTON));
            }
        }
        else{
            Log.i(TAG, "PowerButton alert is not enabled");
        }
    }
}
