package pl.urbansoft.rescue.infrastructure.repository;

import android.util.Log;

import com.braisgabin.couchbaseliteorm.CouchbaseLiteOrm;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.UnsavedRevision;
import com.couchbase.lite.View;
import com.couchbase.lite.auth.Authenticator;
import com.couchbase.lite.auth.BasicAuthenticator;
import com.couchbase.lite.replicator.Replication;
import com.couchbase.lite.support.CouchbaseLiteHttpClientFactory;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.AbstractDocument;
import pl.urbansoft.rescue.domain.shared.entity.enums.DocumentType;
import pl.urbansoft.rescue.infrastructure.security.SecurityProvider;
import pl.urbansoft.rescue.infrastructure.security.ServerSSLContext;

/**
 * Created by andrzej on 12/20/15.
 */
public class RescueRepository {

    public static final String TAG = RescueRepository.class.getSimpleName();

    private static final String VIEW_VERSIONS="31";
    private Database database;
    private CouchbaseLiteOrm orm;

    public RescueRepository(Database database) {

        this.database = database;
        orm = CouchbaseLiteOrm.newInstance();

        startSync();
        createUserView();
        createAlertView();

    }

    public void saveOrUpdate(AbstractDocument document) {
        Document couchDocument;
        try {
            couchDocument = document.getDocumentId() == null ?
                    database.createDocument() :
                    database.getDocument(document.getDocumentId());

            Map<String, Object> updatedProperties = new HashMap<>();

            if (document.getDocumentId() != null) {
                updatedProperties.putAll(couchDocument.getProperties());
            } else {
                document.setDocumentId(couchDocument.getId());
            }
            updatedProperties.putAll(orm.toProperties(document));
            couchDocument.putProperties(updatedProperties);

        } catch (CouchbaseLiteException e) {
            e.printStackTrace();
        }
    }

    public void addAttachment(AbstractDocument document,String name, String contentType,byte[] bData) {
        Document doc = database.getDocument(document.getDocumentId());
        try {
            if(doc==null){
                saveOrUpdate(document);
                doc = database.getDocument(document.getDocumentId());
            }

            UnsavedRevision newRev = doc.getCurrentRevision().createRevision();
            newRev.setAttachment(name, contentType, new ByteArrayInputStream(bData));
            newRev.save();
        }catch(CouchbaseLiteException e){
            Log.e(TAG,""+e.getMessage());
            e.printStackTrace();
        }
    }

    public AbstractDocument getDocumentById(String docId) {
        AbstractDocument document = orm.toObject(database.getDocument(docId));
        document.setDocumentId(docId);

        return document;
    }

    public AbstractDocument getFirstDocumentByType(final DocumentType type) {

        AbstractDocument result = null;

        View view = database.getExistingView(type.getType());
        Query query = view.createQuery();

        query.setLimit(1);

        try {
            QueryEnumerator enumerator = query.run();
            if(enumerator.hasNext()) {
                Document document = enumerator.next().getDocument();
                result = orm.toObject(document);
                result.setDocumentId(document.getId());
                result.setAttachmentNames(document.getCurrentRevision().getAttachmentNames());
            }
        } catch (Exception e) {
            Log.e(TAG,""+ e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    public List<AbstractDocument> getAllDocumentByType(DocumentType type) {

        List<AbstractDocument> result = new ArrayList<>();

        View view = database.getExistingView(type.getType());
        Query query = view.createQuery();
        query.setDescending(true);
        try {
            QueryEnumerator enumerator = query.run();
            while (enumerator.hasNext()){
                Document row = enumerator.next().getDocument();
                AbstractDocument doc = orm.toObject(row);
                doc.setAttachmentNames(row.getCurrentRevision().getAttachmentNames());
                doc.setDocumentId(row.getId());
                result.add(doc);
            }
        } catch (Exception e) {
            Log.e(TAG, "" + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    private void startSync() {
        try {
            CouchbaseLiteHttpClientFactory f =
                    new CouchbaseLiteHttpClientFactory(database.getPersistentCookieStore());
            f.setSSLSocketFactory(new org.apache.http.conn.ssl.SSLSocketFactory(ServerSSLContext.getKeyStore()));
            database.getManager().setDefaultHttpClientFactory(f);

            String dbUrl = RescueApp.getContext().getString(R.string.db_url);
            URL syncUrl = new URL(dbUrl);

            // client - server
            Replication pushReplication = database.createPushReplication(syncUrl);
            pushReplication.setContinuous(true);

            String keyPassword = RescueApp.getContext().getString(R.string.password);
            String keyUser = RescueApp.getContext().getString(R.string.username);

            String password = SecurityProvider.decrypt(keyPassword);
            String user = SecurityProvider.decrypt(keyUser);

            Authenticator auth = new BasicAuthenticator(user, password);
            pushReplication.setAuthenticator(auth);
            pushReplication.start();

        } catch (Exception e) {
            e.printStackTrace();

            throw new RuntimeException(e);
        }

    }

    private void createUserView() {
        View userView = database.getView(DocumentType.USER.getType());
        userView.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                if (DocumentType.USER.getType().equals(document.get("type"))) {
                    emitter.emit(DocumentType.USER.getType(), document);
                }
            }
        }, VIEW_VERSIONS);
    }

    private void createAlertView() {
        View alertView = database.getView(DocumentType.ALERT.getType());
        alertView.setMap(new Mapper() {
            @Override
            public void map(Map<String, Object> document, Emitter emitter) {
                if (DocumentType.ALERT.getType().equals(document.get("type"))) {
                    emitter.emit(new Object[]{document.get("timeStamp"), document.get("_id")}, null);
                }

            }
        }, VIEW_VERSIONS);
    }
}
