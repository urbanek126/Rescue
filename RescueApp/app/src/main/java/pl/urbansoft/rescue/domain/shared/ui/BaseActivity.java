package pl.urbansoft.rescue.domain.shared.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.model.history.ui.HistoryActivity;
import pl.urbansoft.rescue.domain.model.information.InformationActivity;
import pl.urbansoft.rescue.domain.model.settings.ui.SettingsActivity;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.ui.fragments.BleFragment;
import pl.urbansoft.rescue.infrastructure.ble.HtcTagHandler;
import pl.urbansoft.rescue.infrastructure.ble.event.BleDevice;
import pl.urbansoft.rescue.infrastructure.ble.event.HtcTagConnectEvent;

public class BaseActivity extends AppCompatActivity {

    private static final String TAG = AlertActivity.class.getSimpleName();
    private static final String BLE_FRAGEMNT_TAG = "ble";
    private FragmentManager fragmentManager;


    @Inject  EventBus eventBus;
    @Inject  UserService userService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((RescueApp) getApplication()).getRescueComponent().inject(this);

        fragmentManager = getFragmentManager();
        if (HtcTagHandler.isConnected()) {
            onEvent(new HtcTagConnectEvent(new BleDevice(BleDevice.Status.CONNECTED)));
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        ((RescueApp) getApplication()).getRescueComponent().inject(this);

        fragmentManager = getFragmentManager();
        if (HtcTagHandler.isConnected()) {
            onEvent(new HtcTagConnectEvent(new BleDevice(BleDevice.Status.CONNECTED)));
        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                break;
            case R.id.action_history:
                intent = new Intent(this, HistoryActivity.class);
                break;
            //case R.id.action_information:
            //    intent = new Intent(this, InformationActivity.class);
            //    break;
            default:
                intent = new Intent(this, AlertActivity.class);
                break;
        }
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    public void onEvent(final HtcTagConnectEvent e) {
        runOnUiThread(new Runnable() {
            BleDevice.Status status = e.getDevice().getStatus();

            @Override
            public void run() {
                if (status == BleDevice.Status.CONNECTED) {
                    Log.i(TAG, "addBleFragment");
                    //addBleFragment();
                    eventBus.post(new UserExternalButtonUpdatedEvent(true));
                } else {
                    Log.i(TAG, "removeBleFragment");
                    eventBus.post(new UserExternalButtonUpdatedEvent(false));
                }
            }

            ;
        });
    }

    public void onEvent(UserExternalButtonUpdatedEvent event){
        if(event.isExteranlButtonUsed()) {
            addBleFragment();
        }
        else{
            removeBleFragment();
        }
    }

    public void addBleFragment() {
        User user = userService.getUser();
        if (user.isExternalButtonAlert() && HtcTagHandler.isConnected()) {
            fragmentManager.beginTransaction()
                    .add(R.id.bleFragment, BleFragment.newInstance(), BLE_FRAGEMNT_TAG)
                    .commit();
        }

    }

    public void removeBleFragment() {
        Fragment fragemnt;
        if ((fragemnt = getFragmentManager().findFragmentByTag(BLE_FRAGEMNT_TAG)) != null) {
            fragmentManager.beginTransaction()
                    .remove(fragemnt)
                    .commit();
        }
    }
}




