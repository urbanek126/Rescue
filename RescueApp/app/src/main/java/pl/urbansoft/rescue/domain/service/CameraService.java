package pl.urbansoft.rescue.domain.service;

/**
 * Created by urbanek126 on 2/5/2016.
 */
public abstract class CameraService {
    public void stopTakingPhotos(){}
    public void startTakingPhotos(int cameraInterval){}

    public static  String createPhotoName() {
        return "IMG_" + System.currentTimeMillis();
    }
}
