package pl.urbansoft.rescue.domain.service;

import java.util.regex.Pattern;

/**
 * Created by urbanek126 on 12/28/2015.
 */
public class ValidationService {

    public boolean passwordValidation(String password) {
        return password.length() < 5 ? false : true;
    }

    public boolean numberValidation(String password) {
        return password.length() < 8 ? false : true;
    }

    public boolean emailValidation(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    private static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
}
