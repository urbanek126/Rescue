package pl.urbansoft.rescue.domain.service;

import android.os.Build;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * Created by urbanek126 on 12/21/2015.
 */
@Singleton
@Module
public class DomainModule {

    @Provides
    @Singleton
    public CameraService provideCameraService(AlertService alertService, EventBus eventbus, UserService userService) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return new CameraService2(alertService,eventbus,userService);
        } else {
            return new CameraServiceOld(alertService,eventbus);
        }
    }

    @Provides
    @Singleton
    public PhoneService providePhoneService() {
        return new PhoneService();
    }

    @Provides
    @Singleton
    public UserService provideUserService() {
        return new UserService();
    }

    @Provides
    @Singleton
    public MicroPhoneService provideMicroPhoneService() {
        return new MicroPhoneService();
    }

    @Provides
    @Singleton
    public PhoneReactionService providePhoneReactionService() {
        return new PhoneReactionService();
    }

    @Provides
    @Singleton
    public LocationService provideLocationService() {
        return new LocationService();
    }

    @Provides
    @Singleton
    public ValidationService provideValidationService() {
        return new ValidationService();
    }

    @Provides
    @Singleton
    public AlertService provideAlertService() {
        return new AlertService();
    }

}


