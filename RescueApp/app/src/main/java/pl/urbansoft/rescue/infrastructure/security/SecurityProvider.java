package pl.urbansoft.rescue.infrastructure.security;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;

/**
 * Created by andrzej on 2/3/16.
 */
public class SecurityProvider {


    public static String encrypt(String string) {
        try{
            String token = RescueApp.getContext().getString(R.string.token);

            Cipher cipher = Cipher.getInstance("DES"); // cipher is not thread safe
            DESKeySpec keySpec = new DESKeySpec(token.getBytes("UTF-8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKeySpec = keyFactory.generateSecret(keySpec);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);

            byte[] stringBytes =  string.getBytes("UTF-8");
            byte[] encryptedBytes = cipher.doFinal(stringBytes);
            return android.util.Base64.encodeToString(encryptedBytes, android.util.Base64.DEFAULT);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return null;

    }

    public static String decrypt(String string) {
        try{
            String token = RescueApp.getContext().getString(R.string.token);

            Cipher cipher = Cipher.getInstance("DES"); // cipher is not thread safe
            DESKeySpec keySpec = new DESKeySpec(token.getBytes("UTF-8"));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKeySpec = keyFactory.generateSecret(keySpec);

            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] stringBytes = android.util.Base64.decode(string.getBytes(), android.util.Base64.DEFAULT);
            byte[] decryptedBytes = cipher.doFinal(stringBytes);
            return new String(decryptedBytes,"UTF-8");
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }
}
