package pl.urbansoft.rescue.domain.service;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;


import java.util.ArrayList;


import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.Location;
import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;
import pl.urbansoft.rescue.domain.shared.entity.enums.EventCode;
import pl.urbansoft.rescue.domain.shared.event.AlertRespondedEvent;

/**
 * Created by urbanek126 on 12/21/2015.
 */
public class PhoneService {
    @Inject
    EventBus eventbus;

    private static final String TAG = PhoneService.class.getSimpleName();
    private static final String GOOGLE_MAPS_QUERY = "http://maps.google.com/?q=";
    private static final int ONE_SECOND_IN_MILLISECONDS = 1000;
    private static final int DELAY = 10000;
    private Context context;

    public PhoneService() {
        context = RescueApp.getContext();
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
    }

    public void sendSMS(String toNumber, String contentMessage, Location location, int interval) {
        String message = location == null ? contentMessage : makeContentMessageWithLocation(contentMessage, location);
        String footer = RescueApp.getContext().getResources().getString(R.string.sms_footer);

        startSendingSMS(toNumber, message + " " + footer, (interval * ONE_SECOND_IN_MILLISECONDS) - DELAY);
    }

    public void makeCall(String number) {
        try {
            String url = "tel:" + number;
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(url));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (SecurityException e) {
            Log.i(TAG, "PhoneService:makeCall()" + e.getMessage());
            e.printStackTrace();
        }
    }
    public boolean isWifiConnected() {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return wifiInfo.getState() == NetworkInfo.State.CONNECTED;
    }
    private void startSendingSMS(final String toNumber, final String message, int interval) {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        sendSMSviaPhoneSim(toNumber, message);
                    }
                }, interval
        );
    }

    private void sendSMSviaPhoneSim(String toNumber, String contentMessage) {
        try {
            SmsManager sms = SmsManager.getDefault();
            if (contentMessage.length() < 70 && toNumber != null && toNumber != "") {
                sms.sendTextMessage(toNumber, null, contentMessage, null, null);

            } else if (contentMessage.length() >= 70) {
                ArrayList<String> parts = sms.divideMessage(contentMessage);
                sms.sendMultipartTextMessage(toNumber, null, parts, null, null);
            }
            Log.i(TAG, "PhoneService:sendSMS()" + this);
            eventbus.post(new AlertRespondedEvent(EventCode.SUCCESS.getCode(), "", AlertInfoType.MESSAGE));
        }
        catch (Throwable e){
            eventbus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), "Cann't send message" , AlertInfoType.MESSAGE));
        }
    }


    private String setFromNumber() {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getLine1Number() == null ? context.getResources().getString(R.string.app_name) : telephonyManager.getLine1Number();
    }

    private String makeContentMessageWithLocation(String contentMessage, Location location) {
        return contentMessage +"  " + GOOGLE_MAPS_QUERY + location.getLatitude() + "," + location.getLongitude();
    }
}
