package pl.urbansoft.rescue.infrastructure.ble;

import java.util.UUID;

/**
 * Created by andrzej on 11/28/15.
 */
public class BleDefinedUUID {

    public static class DeviceName {
        public static final String HTC_FETCH="HTC Fetch";
    }

    public static class Service {
        final static public String HTC_NOTIFICAITON              		= "0daa5375-02d3-4b47-b6b7-53408ff159e5";
        final static public UUID IMMEDIATE_ALLERT               = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
    }

    public static class Characteristic {
        final public static UUID DOWNLOAD_INFORMATION_OR_COMMAND  = UUID.fromString("00008a81-0000-1000-8000-00805f9b34fb");
        final public static  UUID UPLOAD_INFORMATION_OR_EVENT  = UUID.fromString("00008a82-0000-1000-8000-00805f9b34fb");

        final static public String HTC_NOTIFICAITON  = 			"1daa5375-02d3-4b47-b6b7-53408ff159e5";
        final static public UUID PULSE_NOTIFICATION               = UUID.fromString("0000ff71-0000-1000-8000-00805f9b34fb");

        final static public UUID IMMEDIATE_ALLERT_LEVEL  = 	UUID.fromString("00002a06-0000-1000-8000-00805f9b34fb");

        final static public UUID HEART_RATE_MEASUREMENT   = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
        final static public UUID MANUFACTURER_STRING      = UUID.fromString("00002a29-0000-1000-8000-00805f9b34fb");
        final static public UUID MODEL_NUMBER_STRING      = UUID.fromString("00002a24-0000-1000-8000-00805f9b34fb");
        final static public UUID FIRMWARE_REVISION_STRING = UUID.fromString("00002a26-0000-1000-8000-00805f9b34fb");
        final static public UUID APPEARANCE               = UUID.fromString("00002a01-0000-1000-8000-00805f9b34fb");
        final static public UUID BODY_SENSOR_LOCATION     = UUID.fromString("00002a38-0000-1000-8000-00805f9b34fb");
        final static public UUID BATTERY_LEVEL            = UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb");
    }

    public static class Descriptor {
        final static public String HTC_NOTIFICATION       = "00002902-0000-1000-8000-00805f9b34fb";
    }

}
