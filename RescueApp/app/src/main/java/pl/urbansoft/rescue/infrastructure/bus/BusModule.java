package pl.urbansoft.rescue.infrastructure.bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import de.greenrobot.event.EventBus;

/**
 * Created by andrzej on 11/26/15.
 */

@Module
public class BusModule {

    private static EventBus eventBus;

    @Provides
    @Singleton
    public EventBus provideEventBus() {
        return getEventBusInstance();
    }

    public static final EventBus getEventBusInstance(){
        if(eventBus == null){
            eventBus = EventBus.builder().logNoSubscriberMessages(true).sendNoSubscriberEvent(false).build();
        }
        return eventBus;
    }
}
