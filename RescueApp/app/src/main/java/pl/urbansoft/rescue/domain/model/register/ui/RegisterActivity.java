package pl.urbansoft.rescue.domain.model.register.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.rey.material.widget.EditText;


import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.service.ValidationService;
import pl.urbansoft.rescue.domain.shared.entity.Contact;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    public final int PICK_CONTACT = 2015;

    @Inject ValidationService validationService;
    @Inject EventBus eventBus;
    @Inject UserService userService;

    EditText emailEditText;
    EditText passwordEditText;
    Button startApplicationBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ((RescueApp) getApplication()).getRescueComponent().inject(this);

        emailEditText = (EditText) findViewById(R.id.emailEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        startApplicationBtn = (Button) findViewById(R.id.startApplicationBtn);

        setRegisterOption();
        setStartApplicationOption();
        setFonts();
        isShowstartApplicationBtn();
    }


    private void setFonts() {
        Typeface RobotoRegular = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface RobotoMedium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");

        TextView registerSectionTextView = (TextView) findViewById(R.id.registerSectionTextView);

        registerSectionTextView.setTypeface(RobotoRegular);
        emailEditText.setTypeface(RobotoRegular);
        passwordEditText.setTypeface(RobotoRegular);
        startApplicationBtn.setTypeface(RobotoMedium);

    }
    private void setRegisterOption(){
        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (validationService.emailValidation(s.toString())) {
                    emailEditText.clearError();
                } else {
                    emailEditText.setError(getResources().getString(R.string.screen_register_register_section_email_error));
                }
                isShowstartApplicationBtn();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        passwordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (validationService.passwordValidation(s.toString())) {
                    passwordEditText.clearError();
                } else {
                    passwordEditText.setError(getResources().getString(R.string.screen_register_register_section_password_error));
                }
                isShowstartApplicationBtn();
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {}
        });
    }



    private void setStartApplicationOption() {
        startApplicationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userService.getUser().setEmail(emailEditText.getText().toString());
                userService.getUser().setPassword(passwordEditText.getText().toString());
                eventBus.post(new UserUpdatedEvent());
                Intent intent = new Intent(getBaseContext(), AlertActivity.class);
                startActivity(intent);
            }
        });
    }


    private void isShowstartApplicationBtn(){
        if(validationService.emailValidation(emailEditText.getText().toString()) && validationService.passwordValidation(passwordEditText.getText().toString())) {
            startApplicationBtn.setVisibility(View.VISIBLE);
        }else{
            startApplicationBtn.setVisibility(View.INVISIBLE);
        }
    }

}
