package pl.urbansoft.rescue.domain.shared.entity;

import com.braisgabin.couchbaseliteorm.Field;

import java.util.List;

/**
 * Created by andrzej on 12/20/15.
 */
public abstract class AbstractDocument {

    private String _id;

    private List<String> attachmentNames;

    public AbstractDocument(){}

    public AbstractDocument(String documentId){
        this._id =documentId;
    }

    public String getDocumentId() {
        return _id;
    }

    public void setDocumentId(String documentId){
        this._id=documentId;
    }

    public void setAttachmentNames(List<String> attachmentNames) {
        this.attachmentNames = attachmentNames;
    }

    public List<String> getAttachmentNames() {
        return attachmentNames;
    }
}
