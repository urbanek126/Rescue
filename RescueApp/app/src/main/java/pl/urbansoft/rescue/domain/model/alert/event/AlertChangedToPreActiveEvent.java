package pl.urbansoft.rescue.domain.model.alert.event;

import android.content.Context;

import pl.urbansoft.rescue.domain.shared.event.DomainEvent;

/**
 * Created by urbanek126 on 12/4/2015.
 */
public class AlertChangedToPreActiveEvent implements DomainEvent{

    private AlertType type;

    public AlertChangedToPreActiveEvent(AlertType type){
        this.type = type;
    }

    public enum AlertType{
        BLE,SENSOR,BUTTON,WIDGET_BUTTON,POWER_BUTTON, HEAD_PHONE
    }

    public AlertType getType(){
        return  type;
    }
}
