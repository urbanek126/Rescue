package pl.urbansoft.rescue.domain.model.information.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.information.TutorialActivity;

class ExpandableInformationAdapter extends BaseExpandableListAdapter {

    private final ArrayList<String> groupItem;
    private LayoutInflater inflater;
    private final boolean[] ifGroupExpandedArray;
    private final Context context;
    private final Dialog rulesPopupDialog;

    public ExpandableInformationAdapter(Context context, Dialog rulesPopupDialog) {
        String[] childItemsArray = context.getResources().getStringArray(R.array.expandableListPositionNames);
        groupItem = new ArrayList<>(Arrays.asList(childItemsArray));
        this.context = context;
        this.rulesPopupDialog = rulesPopupDialog;

        ifGroupExpandedArray = new boolean[groupItem.size()];
    }

    public void setInflater(LayoutInflater mInflater, Activity act) {
        this.inflater = mInflater;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        switch (groupPosition) {
            case 0:
                convertView = inflater.inflate(R.layout.first_expandable_child_item, null);
                TextView watchDemoLink = (TextView) convertView.findViewById(R.id.first_expandable_child_item_link);
                watchDemoLink.setOnClickListener(watchDemoLinkListener());
                break;
            case 1:
                convertView = new View(context);
                rulesPopupDialog.show();
                break;
            case 2:
                convertView = inflater.inflate(R.layout.third_expandable_child_item, null);
                TextView tv = (TextView) convertView.findViewById(R.id.info_mail_link);
                tv.setOnClickListener(getMailClickListener(tv));
                break;
            case 3:
                convertView = new View(context);
                break;
            case 4:
                convertView = inflater.inflate(R.layout.fifth_expandable_child_item, null);
                TextView mailLinkTextFifth = (TextView) convertView.findViewById(R.id.mail_link_text_fifth);
                mailLinkTextFifth.setOnClickListener(getMailClickListener(mailLinkTextFifth));
                break;
        }
        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.expandable_list_group_row, null);

        TextView groupViewTitle = (TextView) convertView.findViewById(R.id.group_text);
        groupViewTitle.setText(groupItem.get(groupPosition));

        View groupViewUnderline = convertView.findViewById(R.id.underline);
        ImageView expandIcon = (ImageView) convertView.findViewById(R.id.expand_icon);

        if (ifGroupExpandedArray[groupPosition]) {
            expandIcon.setEnabled(true);
            groupViewUnderline.setEnabled(true);
        } else {
            expandIcon.setEnabled(false);
            groupViewUnderline.setEnabled(false);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }


    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return groupItem.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);

        ifGroupExpandedArray[groupPosition] = false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);

        ifGroupExpandedArray[groupPosition] = true;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }


    private View.OnClickListener watchDemoLinkListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, TutorialActivity.class));
            }
        };
    }

    private View.OnClickListener getMailClickListener(final TextView textview) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SENDTO
                        , Uri.fromParts("mailto", textview.getText().toString(), null));

                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                context.startActivity(Intent.createChooser(intent, ""));

            }
        };
    }
}