package pl.urbansoft.rescue.domain;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.service.UserService;


public class WidgetProvider extends AppWidgetProvider {

    public static String WIDGET_BUTTON = "pl.urbansoft.rescue.WIDGET_BUTTON";

    @Inject
    EventBus eventBus;

    @Inject
    UserService userService;

    private static final String TAG = WidgetProvider.class.getSimpleName();


    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int N = appWidgetIds.length;


     //   ((RescueApp) context.getApplicationContext()).getRescueComponent().inject(this);

        // Perform this loop procedure for each App Widget that belongs to this provider
        for (int i=0; i<N; i++) {
            int appWidgetId = appWidgetIds[i];

              // Create an Intent to launch Activity

            Intent intent = new Intent(context, WidgetProvider.class);
            intent.setAction(WIDGET_BUTTON);

            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);


            // Get the layout for the App Widget and attach an on-click listener
            // to the button
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            views.setOnClickPendingIntent(R.id.widget_alert_btn, pendingIntent);

            // Tell the AppWidgetManager to perform an update on the current app widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    public void onDeleted(Context context , int[] appWidgetIds){
        // This is called every time an App Widget is deleted from the App Widget host.
    }

    public void onAppWidgetOptionsChanged(){
        // This is called when the widget is first placed and any time the widget is resized. You can use this callback to show or hide content based on the widget's size ranges
    }

    public void onEnabled(Context context ){

    }

    public void onDisabled(Context context ){

    }

    @Override
    public void onReceive(Context context, Intent intent){
        super.onReceive(context, intent);
        Log.d(TAG, "Widget activated");

        if (WIDGET_BUTTON.equals(intent.getAction())) {
            Log.d(TAG, "widget - button pressed");
     //       eventBus.post(new AlertChangedToPreActiveEvent());

            Intent activityIntent = new Intent(context, AlertActivity.class);
            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.putExtra(AlertActivity.PRE_ALERT_RAISED, RescueApp.isAgreementAccepted(context));
            activityIntent.putExtra(AlertActivity.ALERT_CONFIRMED, false);

            activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(activityIntent);


        }
    }
}