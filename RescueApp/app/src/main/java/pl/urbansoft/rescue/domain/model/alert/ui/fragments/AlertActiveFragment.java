package pl.urbansoft.rescue.domain.model.alert.ui.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToInActiveEvent;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;
import pl.urbansoft.rescue.domain.shared.entity.enums.EventCode;
import pl.urbansoft.rescue.domain.shared.event.AlertRespondedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;

/**
 * Created by urbanek126 on 12/21/2015.
 */
public class AlertActiveFragment extends Fragment {
    @Inject
    EventBus eventBus;

    @Inject
    UserService userService;

    User user;

    Button stopButton;
    ProgressView cameraProgress;
    ProgressView microphoneProgress;
    ProgressView locationProgress;
    ProgressView messageProgress;

    ImageView cameraImage;
    ImageView microphoneImage;
    ImageView locationImage;
    ImageView emailImage;

    int cameraInterval;
    int microphoneInterval;
    int locationInterval;
    int messageInterval;

    TextView titleTextView ;
    TextView sendPhotoTextView ;
    TextView sendLocationTextView ;
    TextView sendMessageTextView ;
    TextView sendVoiceTextView ;

    Typeface RobotoRegular;
    Typeface RobotoBold;
    Typeface RobotoMedium;

    private int REFRESH_INTERVAL = 100;

    Timer t;


    public static AlertActiveFragment newInstance() {
        AlertActiveFragment fragment = new AlertActiveFragment();
        return fragment;
    }

    public AlertActiveFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_active_alert, container, false);
        ((RescueApp)getActivity().getApplication()).getRescueComponent().inject(this);

        stopButton = (Button)v.findViewById(R.id.alertCancelBtn);

        cameraProgress = (ProgressView)v.findViewById(R.id.photoProgress);
        microphoneProgress = (ProgressView)v.findViewById(R.id.microphoneProgress);
        locationProgress = (ProgressView)v.findViewById(R.id.locationProgress);
        messageProgress = (ProgressView)v.findViewById(R.id.messageProgress);

        microphoneImage = (ImageView) v.findViewById(R.id.microphoneView);
        cameraImage = (ImageView) v.findViewById(R.id.photoView);
        locationImage = (ImageView) v.findViewById(R.id.locationView);
        emailImage = (ImageView) v.findViewById(R.id.messageView);

        sendPhotoTextView = (TextView) v.findViewById(R.id.sendPhoto);
        sendLocationTextView = (TextView) v.findViewById(R.id.sendLocation);
        sendMessageTextView = (TextView) v.findViewById(R.id.sendMessage);
        sendVoiceTextView = (TextView) v.findViewById(R.id.sendVoice);

        RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");
        RobotoMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Medium.ttf");

        //set icons and progress bar status
        initializeIconsAndProgressBars();

        t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        boolean isChecked = user.isMicrophone();
                        if (isChecked){
                            addProgressStep(microphoneProgress, microphoneInterval, REFRESH_INTERVAL);
                        }

                        isChecked = user.isCamera();
                        if (isChecked){
                            addProgressStep(cameraProgress, cameraInterval, REFRESH_INTERVAL);
                        }

                        isChecked = user.isLocation();
                        if (isChecked){
                            addProgressStep(locationProgress, locationInterval, REFRESH_INTERVAL);
                        }

                        isChecked = user.isMessage();
                        if (isChecked) {
                            addProgressStep(messageProgress, messageInterval, REFRESH_INTERVAL);
                        }
                    }
                });

            }
        }, 100, REFRESH_INTERVAL);



        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventBus.post(new AlertChangedToInActiveEvent());
            }
        });

        setFonts(v);


        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        t.cancel();
    }

    @Override
    public void onStart() {
        super.onStart();
        eventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        eventBus.unregister(this);
    }

    private void initializeIconsAndProgressBars(){
        user = userService.getUser();

        microphoneInterval = user.getMicrophoneDuration();
        cameraInterval = user.getCameraInterval();
        locationInterval = user.getLocationInterval();
        messageInterval = user.getMessageInterval();


       if (user.isMicrophone()) {
            enableProgressBar(microphoneProgress, microphoneImage, microphoneInterval,sendVoiceTextView);
        } else {
            disableProgressBar(microphoneProgress, microphoneImage, microphoneInterval,sendVoiceTextView);
        }

        microphoneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = user.isMicrophone();

                if (isChecked){
                    //uncheck
                    user.setIsMicrophone(false);
                    disableProgressBar(microphoneProgress, microphoneImage, microphoneInterval,sendVoiceTextView);
                } else {
                    user.setIsMicrophone(true);
                    enableProgressBar(microphoneProgress, microphoneImage, microphoneInterval,sendVoiceTextView);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });

        if (user.isCamera()) {
            enableProgressBar(cameraProgress, cameraImage, cameraInterval,sendPhotoTextView);
        } else {
            disableProgressBar(cameraProgress, cameraImage, cameraInterval,sendPhotoTextView);
        }
        cameraImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = user.isCamera();

                if (isChecked) {
                    //uncheck
                    disableProgressBar(cameraProgress, cameraImage, cameraInterval,sendPhotoTextView);
                    user.setIsCamera(false);
                } else {
                    enableProgressBar(cameraProgress, cameraImage, cameraInterval,sendPhotoTextView);
                    user.setIsCamera(true);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
        if (user.isMessage()) {
            enableProgressBar(messageProgress, emailImage, messageInterval,sendMessageTextView);
        } else {
            disableProgressBar(messageProgress, emailImage, messageInterval,sendMessageTextView);
        }
        emailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = user.isMessage();

                if (isChecked) {
                    //uncheck
                    disableProgressBar(messageProgress, emailImage, messageInterval,sendMessageTextView);
                    user.setIsMessage(false);

                } else {
                    enableProgressBar(messageProgress, emailImage, messageInterval,sendMessageTextView);
                    user.setIsMessage(true);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
        if (user.isLocation()) {
            enableProgressBar(locationProgress, locationImage, locationInterval,sendLocationTextView);
        } else {
            disableProgressBar(locationProgress, locationImage, locationInterval,sendLocationTextView);
        }
        locationImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = user.isLocation();

                if (isChecked) {
                    //uncheck
                    disableProgressBar(locationProgress, locationImage, locationInterval,sendLocationTextView);
                    user.setIsLocation(false);
                } else {
                    enableProgressBar(locationProgress, locationImage, locationInterval,sendLocationTextView);
                    user.setIsLocation(true);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });

    }

    private void enableProgressBar(ProgressView bar, ImageView icon, int duration,TextView textView){

        icon.setAlpha(0.54f);
        textView.setAlpha(0.89f);
        bar.start();
        bar.applyStyle(R.style.progressBarActive);

    }

    private void disableProgressBar(ProgressView bar, ImageView icon, int duration,TextView textView){

        icon.setAlpha(0.26f);
        textView.setAlpha(0.5f);
        bar.stop();
        bar.setProgress(0f);
        bar.applyStyle(R.style.progressBarInActive);

    }

    private void errorProgressBar(ProgressView bar, ImageView icon, int duration){

        icon.setAlpha(0.54f);
        bar.setProgress(1f);
        bar.applyStyle(R.style.progressBarError);
        bar.stop();


    }

    private void addProgressStep(ProgressView bar, int duration, int timerInterval){
        float percentageToBeAdded = (((float)timerInterval)/(duration*1000)); //both values in miliseconds
        float currentVal = bar.getProgress();
        float valToBeSet = 0f;
        if (currentVal+percentageToBeAdded <= 1)  {
            valToBeSet = currentVal+percentageToBeAdded;
        }

        bar.setProgress(valToBeSet);

    }


    private void setFonts(View v) {
        titleTextView = (TextView) v.findViewById(R.id.title);
        Button stop = (Button) v.findViewById(R.id.alertCancelBtn);

        titleTextView.setTypeface(RobotoRegular);
        sendPhotoTextView.setTypeface(RobotoRegular);
        sendLocationTextView.setTypeface(RobotoRegular);
        sendMessageTextView.setTypeface(RobotoRegular);
        sendVoiceTextView.setTypeface(RobotoRegular);

        stop.setTypeface(RobotoMedium);

    }

    public void onEvent(AlertRespondedEvent e) {
        if (EventCode.ERROR.getCode() == e.getResponseCode()){
            if (AlertInfoType.CAMERA.equals(e.getAlertParameter().name())){
                errorProgressBar(cameraProgress,cameraImage,cameraInterval);
                sendPhotoTextView.setText(R.string.camera_progress_error);
                sendPhotoTextView.setTextColor(getResources().getColor(R.color.ColorError));
            } else if (AlertInfoType.MICROPHONE.equals(e.getAlertParameter().name())){
                errorProgressBar(microphoneProgress, microphoneImage, microphoneInterval);
                sendVoiceTextView.setText(R.string.microphone_progress_error);
                sendVoiceTextView.setTextColor(getResources().getColor(R.color.ColorError));
            }else if (AlertInfoType.MESSAGE.equals(e.getAlertParameter().name())){
                errorProgressBar(messageProgress, emailImage, messageInterval);
                sendMessageTextView.setText(R.string.message_progress_error);
                sendMessageTextView.setTextColor(getResources().getColor(R.color.ColorError));
            }else if (AlertInfoType.LOCATION.equals(e.getAlertParameter().name())){
                errorProgressBar(locationProgress, locationImage, locationInterval);
                sendLocationTextView.setText(R.string.location_progress_error);
                sendLocationTextView.setTextColor(getResources().getColor(R.color.ColorError));
            }


        }
    }
}
