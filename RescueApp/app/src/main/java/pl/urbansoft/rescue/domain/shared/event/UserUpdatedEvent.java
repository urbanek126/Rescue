package pl.urbansoft.rescue.domain.shared.event;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public class UserUpdatedEvent implements DomainEvent{
    private String name;

    public UserUpdatedEvent(){
        this("");
    }

    public UserUpdatedEvent(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
