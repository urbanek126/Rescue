package pl.urbansoft.rescue.domain.service;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.Surface;
import android.view.WindowManager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;
import pl.urbansoft.rescue.domain.shared.entity.enums.ContentType;
import pl.urbansoft.rescue.domain.shared.entity.enums.EventCode;
import pl.urbansoft.rescue.domain.shared.event.AlertAddedPhotoAttachmentEvent;
import pl.urbansoft.rescue.domain.shared.event.AlertRespondedEvent;

/**
 * Created by andrzej on 1/7/16.
 */
public class CameraService2 extends CameraService {

    private final String TAG =  CameraService2.class.getSimpleName();
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private static final String BACK_CAMERA = "0";
    private static final String FRONT_CAMERA = "1";


    private HandlerThread mBackgroundThread;
    private android.os.Handler mBackgroundHandler;
    private ImageReader imageReader;

    private AlertService alertService;
    private UserService userService;
    private EventBus eventbus;
    private ScheduledExecutorService scheduler;

    CameraManager cameraManager = null;

    public CameraService2(AlertService alertServic, EventBus eventbus, UserService userService){
        this.alertService = alertServic;
        this.eventbus = eventbus;
        this.userService = userService;
        cameraManager = (CameraManager) RescueApp.getContext().getSystemService(Context.CAMERA_SERVICE);
    }

    private boolean isPhotoWithFlash(){
        return userService.getUser().isSiren();
    }
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            createCameraPreviewSession(cameraDevice);
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
        }

    };

    private final ImageReader.OnImageAvailableListener onImageAvailableListener = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
        mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), eventbus));
        }

    };

    public void startTakingPhotos(int cameraInterval) {

        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new android.os.Handler(mBackgroundThread.getLooper());

        if (scheduler == null || scheduler.isShutdown()) {
            scheduler = Executors.newSingleThreadScheduledExecutor();
        }
        try {
            Runnable backCameraRunnable = new Runnable() {
                @Override
                public void run() {
                    capturePhoto(BACK_CAMERA);
                }
            };
            Log.i(TAG, "Schedule picture capture for camera 1");
            scheduler.scheduleAtFixedRate(backCameraRunnable, 0, cameraInterval, TimeUnit.SECONDS);
            if(cameraManager.getCameraIdList().length > 1 ) {
                Runnable frontCameraRunnable = new Runnable() {
                    @Override
                    public void run() {
                        capturePhoto(FRONT_CAMERA);
                    }
                };
                Log.i(TAG,"Schedule picture capture for camera 2 ");
                scheduler.scheduleAtFixedRate(frontCameraRunnable, cameraInterval / 2, cameraInterval, TimeUnit.SECONDS);
            }

        } catch (Exception e) {
            Log.e(TAG, "CameraServiceOld issue for camera");
            e.printStackTrace();
            eventbus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), e.getMessage(), AlertInfoType.CAMERA));
        }

    }

    private void capturePhoto(String cameraId) {

        try {
            CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = characteristics.get(
                    CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            if (map != null) {
                // For still image captures, we use the largest available size.
                Size largest = Collections.max(
                        Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                        new Comparator<Size>() {
                            @Override
                            public int compare(Size lhs, Size rhs) {
                                // We cast here to ensure the multiplications won't overflow
                                return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                                        (long) rhs.getWidth() * rhs.getHeight());
                            }

                        });

                imageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 2);
                imageReader.setOnImageAvailableListener(onImageAvailableListener, mBackgroundHandler);

                if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw new RuntimeException("Time out waiting to lock camera opening.");
                }
                cameraManager.openCamera(cameraId, mStateCallback, mBackgroundHandler);

            }
            else{
                Log.i(TAG, "Skip camera " + cameraId);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
            eventbus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), e.getMessage(), AlertInfoType.CAMERA));
        } catch (InterruptedException e) {
            eventbus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), e.getMessage(), AlertInfoType.CAMERA));
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }

    }

    public void stopTakingPhotos() {

        if(scheduler !=null && !scheduler.isShutdown())
            scheduler.shutdown();

        if(mBackgroundThread!=null && mBackgroundThread.isAlive()) {
            mBackgroundThread.quit();
            mBackgroundThread =null;
        }

    }

    private void createCameraPreviewSession(final CameraDevice cameraDevice) {
        try {
            // Here, we create a CameraCaptureSession for camera preview.
            cameraDevice.createCaptureSession(Arrays.asList(imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == cameraDevice) {
                                return;
                            }
                            CaptureRequest.Builder builder = null;
                            try {
                                builder = setupCameraBuilder(cameraDevice);

                                builder.addTarget(imageReader.getSurface());

                                cameraCaptureSession.capture(builder.build(),
                                        new CameraCaptureSession.CaptureCallback(){
                                            public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                                                           @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                                                session.close();
                                                cameraDevice.close();
                                        }
                                }, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            Log.e(TAG, "Error with taking piictures");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            eventbus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), e.getMessage(), AlertInfoType.CAMERA));
            e.printStackTrace();
        }
    }


    private  CaptureRequest.Builder setupCameraBuilder(CameraDevice cameraDevice) throws  CameraAccessException{
        CaptureRequest.Builder builder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

        if(isPhotoWithFlash()) {
            builder.set(CaptureRequest.FLASH_MODE,CaptureRequest.FLASH_MODE_SINGLE);
        }
        else {
            builder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);
        }
        // Orientation
        WindowManager wm = (WindowManager) RescueApp.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int rotation = display.getRotation();
        builder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

        return builder;
    }

    private static boolean contains(int[] modes, int mode) {
        if (modes == null) {
            return false;
        }
        for (int i : modes) {
            if (i == mode) {
                return true;
            }
        }
        return false;
    }
    /**
     * Saves a JPEG {@link Image} into the specified {@link File}.
     */
    private static class ImageSaver implements Runnable {

        /**
         * The JPEG image
         */
        private final Image mImage;
        /**
         * The file we save the image into.
         */
        private final EventBus eventBus;

        public ImageSaver(Image image , EventBus eventBus) {
            this.mImage = image;
            this.eventBus = eventBus;
        }

        @Override
        public void run() {
            FileOutputStream output =null;
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);

            eventBus.post(new AlertAddedPhotoAttachmentEvent(bytes,createPhotoName(), ContentType.JPEG.getType()));

        }

    }
}
