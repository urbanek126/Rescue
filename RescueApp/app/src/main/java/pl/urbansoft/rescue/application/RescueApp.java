package pl.urbansoft.rescue.application;

import android.Manifest;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.shared.handler.AlertAlarmHandler;
import pl.urbansoft.rescue.domain.shared.handler.UserHandler;
import pl.urbansoft.rescue.domain.service.DomainModule;
import pl.urbansoft.rescue.infrastructure.ble.BleModule;
import pl.urbansoft.rescue.infrastructure.ble.BleService;
import pl.urbansoft.rescue.infrastructure.ble.HtcTagHandler;
import pl.urbansoft.rescue.infrastructure.bus.BusModule;
import pl.urbansoft.rescue.infrastructure.injection.AppModule;
import pl.urbansoft.rescue.infrastructure.injection.DaggerRescueComponent;
import pl.urbansoft.rescue.infrastructure.injection.RescueComponent;
import pl.urbansoft.rescue.infrastructure.repository.RepositoryModule;
import pl.urbansoft.rescue.infrastructure.sensors.Globals;
import pl.urbansoft.rescue.infrastructure.sensors.SensorsService;
import pl.urbansoft.rescue.interfaces.broadcast.HeadPhoneReaciver;
import pl.urbansoft.rescue.interfaces.broadcast.PowerButtonReaciver;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by andrzej on 11/27/15.
 */
public class RescueApp extends Application {

    private static final String TAG = RescueApp.class.getSimpleName();
    public static final String APP_VERSION_AGREMENTS = "app_version_agrements";
    private static SharedPreferences sp;

    public static final String CAMERA = "camera";
    public static final String LOCATION = "location";
    public static final String MESSAGE = "message";
    public static final String MICROPHONE = "microphone";

    private RescueComponent component;

    //keep list of handlers not garbage collect
    private List<Object> handlers = new ArrayList<>();

    private static Context context;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    public static final String  TRACKER_ID="UA-70809244-1";

    @Override
    public void onCreate() {
        super.onCreate();

        this.context = getApplicationContext();

        //Google analitics
        this.analytics = GoogleAnalytics.getInstance(this);
        this.analytics.setLocalDispatchPeriod(1800);
        this.tracker = analytics.newTracker(TRACKER_ID);
        this.tracker.enableExceptionReporting(true);
        this.tracker.enableAdvertisingIdCollection(true);
        this.tracker.enableAutoActivityTracking(true);

        //Fabric
        Fabric.with(this, new Crashlytics());

        DaggerRescueComponent.Builder builder = pl.urbansoft.rescue.infrastructure.injection.DaggerRescueComponent.builder()
            // list of modules that are part of this component need to be created here too
            .appModule(new AppModule(this))
            .busModule(new BusModule())
            .domainModule(new DomainModule())
            .repositoryModule(new RepositoryModule());

        if(BleService.isBleSupported()){
            builder = builder.bleModule(new BleModule());
        }

        this.component = builder.build();

        startServices();
        registerHandlers();
        registerBroadcasts();

        sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    }

    public RescueComponent getRescueComponent() {
        return component;
    }

    private void startServices(){
        //start alert detection service
        Intent intent = new Intent(this, SensorsService.class);
        Bundle extras = new Bundle();
        extras.putString(Globals.CLASS_LABEL_KEY, Globals.CLASS_LABEL_NORMAL);
        intent.putExtras(extras);

        startService(intent);
    }

    private void  registerBroadcasts(){
        //register receiver
        PowerButtonReaciver powerButtonBroadcast = new PowerButtonReaciver();
        context.registerReceiver(powerButtonBroadcast, new IntentFilter("android.intent.action.SCREEN_ON"));
        context.registerReceiver(powerButtonBroadcast, new IntentFilter("android.intent.action.SCREEN_OFF"));

        context.registerReceiver(new HeadPhoneReaciver(), new IntentFilter("android.intent.action.HEADSET_PLUG"));
    }

    private void registerHandlers(){
        //start handler
        if(BleService.isBleSupported()){
            handlers.add(new HtcTagHandler(component));
        }

        handlers.add(new AlertAlarmHandler(component));
        handlers.add(new UserHandler(component));
    }

    public static final Tracker getTracker(){
        return  analytics.newTracker(TRACKER_ID);
    }

    public static Context getContext(){
        return  context;
    }


    public static float getAppVersion(Context context){
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo("pl.urbansoft.rescue", 0);
        }catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "getAppVersion");
            return 0;
        }
        return Float.parseFloat(pInfo.versionName);
    }

    public static boolean isAgreementAccepted(Context context){
        float currentAppVersionAgrements = getAppVersion(context);
        float previouslyAppVersionAgrements = sp.getFloat(APP_VERSION_AGREMENTS, 0);

        Log.i(TAG, "currentversion:" + currentAppVersionAgrements +
                " previouslyAppVersionAgrements:"+previouslyAppVersionAgrements);
        return currentAppVersionAgrements <= previouslyAppVersionAgrements;

    }
}
