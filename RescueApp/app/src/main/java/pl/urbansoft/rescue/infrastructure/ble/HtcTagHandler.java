package pl.urbansoft.rescue.infrastructure.ble;

import android.bluetooth.BluetoothGattDescriptor;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;

import com.litesuits.bluetooth.LiteBluetooth;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;
import pl.urbansoft.rescue.infrastructure.ble.event.BleDevice;
import pl.urbansoft.rescue.infrastructure.ble.event.HtcTagConnectEvent;
import pl.urbansoft.rescue.infrastructure.injection.RescueComponent;

/**
 * Created by andrzej on 11/28/15.
 */
public class HtcTagHandler{

    private static final String TAG = HtcTagHandler.class.getSimpleName();
    private static boolean isConnected = false;

    private Thread monitoringThread = null;

    @Inject
    BleService bleService;

    @Inject
    EventBus eventBus;

    @Inject
    UserService userService;

    public HtcTagHandler(RescueComponent  component){
        component.inject(this);
        eventBus.register(this);

        onEvent(new HtcTagConnectEvent(new BleDevice(BleDevice.Status.INITIALIZED)));
    }

    private void startMonitoring(){
        if (BleService.isBleSupported()) {

            User user = userService.getUser();
            if (user != null && user.isExternalButtonAlert()) {
                if (monitoringThread == null || !monitoringThread.isAlive()) {
                    //ble monitoring
                    monitoringThread = new Thread(new Runnable() {
                        public void run() {
                            Looper.prepare();
                            while (true) {
                                int status = bleService.getStatus();

                                if (LiteBluetooth.STATE_DISCONNECTED == status) {
                                    SystemClock.sleep(1_000 * 60 * 60);//60 minut - try to connect
                                    eventBus.post(new HtcTagConnectEvent(new BleDevice()));
                                } else {
                                    SystemClock.sleep(1_000 * 60 * 60);//60 minut
                                }
                            }
                        }
                    });
                    monitoringThread.start();
                }
            }
        }
        else{
            Log.i(TAG, "BLE is not supported");
        }
    }


    public void onEvent(UserExternalButtonUpdatedEvent event){

        if(userService.getUser().isExternalButtonAlert()){
            startMonitoring();
        }
    }

    public void onEvent(HtcTagConnectEvent event) {

        Log.i(TAG, "HtcTagConnectEvent event with status:" + event.getDevice().getStatus());
        BleDevice device = event.getDevice();

        if (device.getStatus() == BleDevice.Status.INITIALIZED) {
            startMonitoring();
        } else if (device.getStatus() == BleDevice.Status.SEARCHING) {
            bleService.scanDevicesPeriod(new BleDevice());
        } else if (device.getStatus() == BleDevice.Status.DISCOVERED) {
            bleService.scanThenConnect(device);
        } else if (device.getStatus() == BleDevice.Status.CONNECTED) {
            new AsyncTask<Void, Void, Void>() {
                protected Void doInBackground(Void... unused) {
                    Looper.prepare();

                    Log.i(TAG, "Executing for CONNECTED in AsyncTask");
                    bleService.enableNotificationOfCharacteristic(
                            BleDefinedUUID.Service.HTC_NOTIFICAITON,
                            BleDefinedUUID.Characteristic.HTC_NOTIFICAITON
                    );

                    bleService.writeDataToDescriptor(
                            BleDefinedUUID.Service.HTC_NOTIFICAITON,
                            BleDefinedUUID.Characteristic.HTC_NOTIFICAITON,
                            BleDefinedUUID.Descriptor.HTC_NOTIFICATION,
                            BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    );
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }


        if (device.getStatus() == BleDevice.Status.CONNECTED){
            isConnected = true;
        }
        else{
            isConnected =false;
        }
    }

    public static boolean isConnected(){
        return  isConnected;
    }
}
