package pl.urbansoft.rescue.infrastructure.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.litesuits.bluetooth.LiteBleGattCallback;
import com.litesuits.bluetooth.LiteBluetooth;
import com.litesuits.bluetooth.conn.BleCharactCallback;
import com.litesuits.bluetooth.conn.BleDescriptorCallback;
import com.litesuits.bluetooth.conn.BleRssiCallback;
import com.litesuits.bluetooth.conn.LiteBleConnector;
import com.litesuits.bluetooth.exception.BleException;
import com.litesuits.bluetooth.exception.hanlder.DefaultBleExceptionHandler;
import com.litesuits.bluetooth.log.BleLog;
import com.litesuits.bluetooth.scan.PeriodMacScanCallback;
import com.litesuits.bluetooth.scan.PeriodScanCallback;
import com.litesuits.bluetooth.utils.BluetoothUtil;

import java.util.Arrays;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToPreActiveEvent;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.infrastructure.ble.event.BleDevice;
import pl.urbansoft.rescue.infrastructure.ble.event.HtcTagConnectEvent;
import pl.urbansoft.rescue.infrastructure.bus.BusModule;

/**
 * Created by andrzej on 11/28/15.
 */
public class BleService {

    private static final String TAG = BleService.class.getSimpleName();

    private static LiteBluetooth liteBluetooth;
    private DefaultBleExceptionHandler bleExceptionHandler;

    private EventBus eventBus;
    private UserService userService;

    private static int TIME_OUT_SCAN = 5000;

    public BleService(EventBus eventBus,UserService userService ) {
        if (liteBluetooth == null) {
            liteBluetooth = new LiteBluetooth(RescueApp.getContext());
        }

        bleExceptionHandler = new DefaultBleExceptionHandler(RescueApp.getContext());

        this.eventBus=eventBus;
        this.userService=userService;

    }

    public static boolean isBleSupported(){
        return  android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT &&
                         RescueApp.getContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public void disconnect(){
        if(liteBluetooth.isConnected()) {
            liteBluetooth.getBluetoothGatt().disconnect();
        }
    }

    public int getStatus(){
        return liteBluetooth.getConnectionState();
    }

    public void scanDevicesPeriod(final BleDevice bleDevice) {
        if(!liteBluetooth.isConnected()) {
            liteBluetooth.newBleConnector();
        }

        liteBluetooth.startLeScan(new PeriodScanCallback(TIME_OUT_SCAN) {
            private String mac = null;

            @Override
            public void onScanTimeout() {
                removeHandlerMsg();
                BleLog.i(TAG, TIME_OUT_SCAN + " Millis Scan Timeout! ");
            }

            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                BleLog.d(TAG, "device: " + device.getName() + "  mac: " + device.getAddress()
                        + "  rssi: " + rssi + "  scanRecord: " + Arrays.toString(scanRecord));

                if (bleDevice.getName().equals(device.getName()) && mac == null) {
                    liteBluetooth.stopScan(this);
                    mac = device.getAddress();

                    eventBus.post(new HtcTagConnectEvent(new BleDevice(device.getAddress(), BleDevice.Status.DISCOVERED)));
                }
            }
        });
    }


    public void scanSpecifiedDevicePeriod(String mac) {
        liteBluetooth.startLeScan(new PeriodMacScanCallback(mac, TIME_OUT_SCAN) {

            @Override
            public void onScanTimeout() {
                BleLog.i(TIME_OUT_SCAN + " Millis Scan Timeout!  Device Not Found! ");
            }

            @Override
            public void onDeviceFound(BluetoothDevice device, int rssi, byte[] scanRecord) {
                BleLog.i(" Device Found " + device.getName() + " MAC: " + device.getAddress()
                        + " \n RSSI: " + rssi + " records:" + Arrays.toString(scanRecord));
            }
        });
    }


    public void scanAndConnect(final BleDevice device) {

        final String mac = device.getMac();

        liteBluetooth.scanAndConnect(mac, false, new LiteBleGattCallback() {

            @Override
            public void onConnectSuccess(BluetoothGatt gatt, int status) {
                // discover services !
                gatt.discoverServices();
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                BluetoothUtil.printServices(gatt);
                BleLog.i(mac + " Services Discovered SUCCESS !");
            }

            @Override
            public void onConnectFailure(BleException exception) {
                bleExceptionHandler.handleException(exception);
                BleLog.i(mac + " Services Discovered FAILURE !");
            }
        });
    }

    public void scanThenConnect(final BleDevice bleDevice) {

        final String mac = bleDevice.getMac();

        liteBluetooth.startLeScan(new PeriodMacScanCallback(mac, TIME_OUT_SCAN) {

            @Override
            public void onScanTimeout() {

                BleLog.i(TIME_OUT_SCAN + " Timeout");

                removeHandlerMsg();
            }

            @Override
            public void onDeviceFound(final BluetoothDevice device, int rssi, byte[] scanRecord) {

                liteBluetooth.connect(device, false, new LiteBleGattCallback() {

                    private boolean eventPublished = false;

                    @Override
                    public void onConnectSuccess(BluetoothGatt gatt, int status) {
                        gatt.discoverServices();

                    }

                    @Override
                    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                        //BluetoothUtil.printServices(gatt);
                        if (!eventPublished) {
                            eventPublished = true;
                            eventBus.post(new HtcTagConnectEvent(new BleDevice(device.getAddress(), BleDevice.Status.CONNECTED)));
                        }
                    }

                    @Override
                    public void onConnectFailure(BleException exception) {
                        new AsyncTask<BleException, Void, Void>() {
                            protected void onPreExecute() {
                            }

                            protected Void doInBackground(final BleException... exception) {
                                bleExceptionHandler.handleException(exception[0]);
                                BleLog.i(device.getAddress() + " Error connected: ");
                                return null;
                            }

                            protected void onPostExecute() {
                            }
                        }.execute();

                    }

                    @Override
                    public void onConnectionStateChange(BluetoothGatt gatt, int status,
                                                        int newState) {

                        super.onConnectionStateChange(gatt, status, newState);

                        Log.i(TAG, "status:" + status + " newStatus: " + newState);

                        if (newState == 0) {
                            getLiteBluetooth().removeGattCallback(this);
                        }

                    }

                });
            }
        });
    }


    public void writeDataToCharacteristic(String UUID_SERVICE, String UUID_CHAR_WRITE) {
        LiteBleConnector connector = liteBluetooth.newBleConnector();
        connector.withUUIDString(UUID_SERVICE, UUID_CHAR_WRITE, null)
                .writeCharacteristic(new byte[]{1, 2, 3}, new BleCharactCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {
                        BleLog.i(TAG, "Write Success, DATA: " + Arrays.toString(characteristic.getValue()));
                    }

                    @Override
                    public void onFailure(BleException exception) {
                        BleLog.i(TAG, "Write failure: " + exception);
                        bleExceptionHandler.handleException(exception);
                    }
                });
    }

    public void writeDataToDescriptor(String UUID_SERVICE, String UUID_CHAR_WRITE,
                                      String UUID_DESCRIPTOR_WRITE, byte[] val ) {

        LiteBleConnector connector = liteBluetooth.newBleConnector();

        connector.withUUIDString(UUID_SERVICE, UUID_CHAR_WRITE, UUID_DESCRIPTOR_WRITE)
                .writeDescriptor(val, new BleDescriptorCallback() {
                    @Override
                    public void onSuccess(BluetoothGattDescriptor descriptor) {
                        BleLog.i(TAG, "Write Success, DATA: " + Arrays.toString(descriptor.getValue()));
                        liteBluetooth.removeGattCallback(this.getBluetoothGattCallback());
                    }

                    @Override
                    public void onFailure(BleException exception) {
                        BleLog.i(TAG, "Write failure: " + exception);
                        bleExceptionHandler.handleException(exception);
                    }
                });
    }


    public void readDataFromCharacteristic(String UUID_SERVICE, String UUID_CHAR_READ) {
        LiteBleConnector connector = liteBluetooth.newBleConnector();
        connector.withUUIDString(UUID_SERVICE, UUID_CHAR_READ, null)
                .readCharacteristic(new BleCharactCallback() {
                    @Override
                    public void onSuccess(BluetoothGattCharacteristic characteristic) {
                        BleLog.i(TAG, "Read Success, DATA: " + Arrays.toString(characteristic.getValue()));
                    }

                    @Override
                    public void onFailure(BleException exception) {
                        BleLog.i(TAG, "Read failure: " + exception);
                        bleExceptionHandler.handleException(exception);
                    }
                });
    }

    public void readDataFromDescriptor(String UUID_SERVICE, String UUID_CHAR_READ, String UUID_DESCRIPTOR_READ) {
        LiteBleConnector connector = liteBluetooth.newBleConnector();
        connector.withUUIDString(UUID_SERVICE, UUID_CHAR_READ, UUID_DESCRIPTOR_READ)
                .readDescriptor(new BleDescriptorCallback() {
                    @Override
                    public void onSuccess(BluetoothGattDescriptor descriptor) {
                        BleLog.i(TAG, "Read Success, DATA: " + Arrays.toString(descriptor.getValue()));

                        if (descriptor.getValue() != BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE) {
                            BleLog.i(TAG, "Notification is not enabled");
                            eventBus.post(new HtcTagConnectEvent(new BleDevice(null, BleDevice.Status.ENABLED_NOTIFICATION)));
                        }
                    }

                    @Override
                    public void onFailure(BleException exception) {
                        BleLog.i(TAG, "Read failure : " + exception);
                        bleExceptionHandler.handleException(exception);
                    }
                });
    }

    public void enableNotificationOfCharacteristic(String UUID_SERVICE, String UUID_CHAR_READ) {

        BleLog.i(TAG, "Enabling notifications: enableNotificationOfCharacteristic");

        LiteBleConnector connector = liteBluetooth.newBleConnector();

        connector.withUUIDString(UUID_SERVICE, UUID_CHAR_READ, null)
            .enableCharacteristicNotification(new BleCharactCallback() {
                @Override
                public void onSuccess(BluetoothGattCharacteristic characteristic) {
                    BleLog.i(TAG, "Notification characteristic Success, DATA: " + this.getBluetoothGattCallback().hashCode() + Arrays
                            .toString(characteristic.getValue()));
                    if (characteristic.getValue()[0] == 2) {
                            if(userService.getUser().isExternalButtonAlert()) {
                                //Alert only is externall button is activated
                                eventBus.post(new AlertChangedToPreActiveEvent(AlertChangedToPreActiveEvent.AlertType.BLE));
                            }
                    } else {
                        liteBluetooth.removeGattCallback(this.getBluetoothGattCallback());
                        Log.i(TAG, "Device disconnected");
                    }
                }

                @Override
                public void onFailure(BleException exception) {
                    BleLog.i(TAG, "Notification characteristic failure: " + exception);
                    liteBluetooth.removeGattCallback(this.getBluetoothGattCallback());

                    bleExceptionHandler.handleException(exception);
                }
            });

    }

    public void enableNotificationOfDescriptor(String UUID_SERVICE, String UUID_CHAR_READ, String UUID_DESCRIPTOR_READ) {

        LiteBleConnector connector = liteBluetooth.newBleConnector();
        connector.withUUIDString(UUID_SERVICE, UUID_CHAR_READ, UUID_DESCRIPTOR_READ)
                .enableDescriptorNotification(new BleDescriptorCallback() {
                    @Override
                    public void onSuccess(BluetoothGattDescriptor descriptor) {
                        BleLog.i(TAG,
                                "Notification descriptor Success, DATA: " + Arrays.toString(descriptor.getValue()));
                    }

                    @Override
                    public void onFailure(BleException exception) {
                        BleLog.i(TAG, "Notification descriptor failure : " + exception);
                        bleExceptionHandler.handleException(exception);
                    }
                });
    }


    public void readRssiOfDevice() {
        liteBluetooth.newBleConnector()
                .readRemoteRssi(new BleRssiCallback() {
                    @Override
                    public void onSuccess(int rssi) {
                        BleLog.i(TAG, "Read Success, rssi: " + rssi);
                    }

                    @Override
                    public void onFailure(BleException exception) {
                        BleLog.i(TAG, "Read failure : " + exception);
                        bleExceptionHandler.handleException(exception);
                    }
                });
    }

}
