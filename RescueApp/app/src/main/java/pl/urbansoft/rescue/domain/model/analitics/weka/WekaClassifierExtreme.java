package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by andrzej on 11/15/15.
 */
public class WekaClassifierExtreme implements RescueClassifier {
    public  double classify(Object[] i) throws Exception {

        double p = Double.NaN;
        p = WekaClassifierExtreme.N769dac960(i);
        return p;
    }
    static double N769dac960(Object []i) {
        double p = Double.NaN;
        if (i[0] == null) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() <= 1338.718708) {
            p = 0;
        } else if (((Double) i[0]).doubleValue() > 1338.718708) {
            p = 1;
        }
        return p;
    }
}
