package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 1/4/2016.
 */
public enum PhoneSensitivity {
        NONE(0), SMALL(4), MEDIUM(7), HIGH(10) ;

    private int value;

    private PhoneSensitivity(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static PhoneSensitivity getPhoneSensitivityName(int value) {

        if (value == PhoneSensitivity.NONE.getValue()) {
            return PhoneSensitivity.NONE;
        }
        if (value < PhoneSensitivity.SMALL.getValue()) {
            return PhoneSensitivity.SMALL;
        }
        if (value < PhoneSensitivity.MEDIUM.getValue()) {
            return PhoneSensitivity.MEDIUM;
        } else {
            return PhoneSensitivity.HIGH;
        }
    }
}
