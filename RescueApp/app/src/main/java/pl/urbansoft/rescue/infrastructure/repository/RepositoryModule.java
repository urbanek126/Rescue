package pl.urbansoft.rescue.infrastructure.repository;

import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.urbansoft.rescue.application.RescueApp;

/**
 * Created by andrzej on 12/19/15.
 */
@Module
public class RepositoryModule {
    public static final String TAG = RepositoryModule.class.getSimpleName();
    public static final String DB_NAME = "rescuedb";

    private static Manager manager = null;
    private static Database database = null;

    @Provides
    @Singleton
    public Database provideDatabaseInstance() {
        try{
            if (this.database == null) {
                if(this.manager == null){
                    manager = provideManagerInstance();
                }

                this.database = manager.getDatabase(DB_NAME);
            }
        }catch ( Exception e){
            Log.e(TAG,e.getMessage());
        }
        return database;
    }

    //@Provides
    //@Singleton
    public Manager provideManagerInstance() {
        try {
            if (manager == null) {
                manager = new Manager(new AndroidContext(RescueApp.getContext()), Manager.DEFAULT_OPTIONS);
            }
        } catch (Exception e){
            Log.e(TAG,e.getMessage());
        }

        return manager;
    }


    @Provides
    @Singleton
    public RescueRepository provideRescueRepository(Database database) {
        return  new RescueRepository(database);
    }
}
