package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by andrzej on 12/20/15.
 */
public enum DocumentType {
    USER("user"),ALERT("alert"),ALERT_AGG("alert_agg");

    private String type;

    private DocumentType(String type){
        this.type = type;
    }

    public String getType(){
        return  type;
    }
}
