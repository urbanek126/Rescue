package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 2/12/2016.
 */
public enum EmergencyNumbers {
    PL("112"),US("911"),IN("100"),CN("110"),BR("190"),RU("112"),ZA("10111"),AU("000"),CO("123");

    private String emgergnecyNumber;


    private EmergencyNumbers(String emgergnecyNumber) {
        this.emgergnecyNumber = emgergnecyNumber;

    }

    public String getEmgergnecyNumber() {
        return emgergnecyNumber;
    }

    public static String getEmgergnecyNumberByCode(String countryCode) {
        for (EmergencyNumbers en : EmergencyNumbers.values()) {
            if(en.name().equals(countryCode)){
                return en.getEmgergnecyNumber();
            }
        }
        return null;
    }
}


