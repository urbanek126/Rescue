package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 1/3/2016.
 */
public enum AlertInfoType {
    CAMERA, MICROPHONE, MESSAGE, LOCATION;
}
