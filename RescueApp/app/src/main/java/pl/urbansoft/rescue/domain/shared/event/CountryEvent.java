package pl.urbansoft.rescue.domain.shared.event;

import pl.urbansoft.rescue.domain.shared.entity.Location;

/**
 * Created by urbanek126 on 1/1/2016.
 */
public class CountryEvent {
    private String country;

    public CountryEvent( String country){
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
