package pl.urbansoft.rescue.domain.shared.entity;


import com.braisgabin.couchbaseliteorm.Entity;
import com.braisgabin.couchbaseliteorm.Field;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by urbanek126 on 12/29/2015.
 */
@Entity("location")
public class Location {

    @Field("latitude")
    double latitude;
    @Field("longitude")
    double longitude;
    @Field("timeStamp")
    long timeStamp;


    public String toString() {
        return " latitude: " + latitude + " longitude: " + longitude + " Timestamp: " + timeStamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }
}

