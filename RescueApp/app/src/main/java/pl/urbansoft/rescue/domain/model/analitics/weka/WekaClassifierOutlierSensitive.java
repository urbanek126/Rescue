package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by ewa on 14/02/16.
 */
class WekaClassifierOutlierSensitive implements RescueClassifier {

        public double classify(Object[] i)
                throws Exception {

            double p = Double.NaN;
            p = WekaClassifierOutlierSensitive.N11ebc4a613(i);
            return p;
        }
        static double N11ebc4a613(Object []i) {
            double p = Double.NaN;
            if (i[64] == null) {
                p = 0;
            } else if (((Double) i[64]).doubleValue() <= 42.323151) {
                p = WekaClassifierOutlierSensitive.N74033bdb14(i);
            } else if (((Double) i[64]).doubleValue() > 42.323151) {
                p = 1;
            }
            return p;
        }
        static double N74033bdb14(Object []i) {
            double p = Double.NaN;
            if (i[1] == null) {
                p = 0;
            } else if (((Double) i[1]).doubleValue() <= 149.288238) {
                p = 0;
            } else if (((Double) i[1]).doubleValue() > 149.288238) {
                p = WekaClassifierOutlierSensitive.N4122765d15(i);
            }
            return p;
        }
        static double N4122765d15(Object []i) {
            double p = Double.NaN;
            if (i[5] == null) {
                p = 0;
            } else if (((Double) i[5]).doubleValue() <= 73.236224) {
                p = WekaClassifierOutlierSensitive.N6e342f6a16(i);
            } else if (((Double) i[5]).doubleValue() > 73.236224) {
                p = WekaClassifierOutlierSensitive.N213518fe17(i);
            }
            return p;
        }
        static double N6e342f6a16(Object []i) {
            double p = Double.NaN;
            if (i[27] == null) {
                p = 1;
            } else if (((Double) i[27]).doubleValue() <= 2.799355) {
                p = 1;
            } else if (((Double) i[27]).doubleValue() > 2.799355) {
                p = 0;
            }
            return p;
        }
        static double N213518fe17(Object []i) {
            double p = Double.NaN;
            if (i[1] == null) {
                p = 0;
            } else if (((Double) i[1]).doubleValue() <= 182.733366) {
                p = 0;
            } else if (((Double) i[1]).doubleValue() > 182.733366) {
                p = 1;
            }
            return p;
        }
    }


