package pl.urbansoft.rescue.domain.service;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;


import com.google.common.primitives.Bytes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.enums.ContentType;
import pl.urbansoft.rescue.domain.shared.event.AlertAddedRecordAttachmentEvent;

/**
 * Created by urbanek126 on 12/26/2015.
 */
public class MicroPhoneService {
    @Inject
    AlertService alertService;
    @Inject
    EventBus eventbus;

    private static final String TAG = MicroPhoneService.class.getSimpleName();
    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final int BufferElements2Rec = 1024;
    private static final int BytesPerElement = 2;
    private static final int audioChunkSize = BufferElements2Rec*BytesPerElement*100;

    private AudioRecord audioRecorder = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;
    private long timeRecording;
    private byte[] bData;
    private int bDataSize;

    public MicroPhoneService() {
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
    }

    public void startRecording(int microphoneInterval) {
        audioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, BufferElements2Rec * BytesPerElement);

        audioRecorder.startRecording();
        isRecording = true;
        recordingThread = new Thread(new Runnable() {
            public void run() {
                captureAudioChunks();
            }
        }, "AudioRecorder Thread");
        recordingThread.start();

    }

    public void stopRecording() {
        if (audioRecorder != null) {
            isRecording = false;
            audioRecorder.stop();
            audioRecorder.release();
            audioRecorder = null;
            recordingThread = null;
            makeAudioChunkPost();
        }
    }

    private void makeAudioChunkPost() {
        eventbus.post(new AlertAddedRecordAttachmentEvent(bData, createMicName(), ContentType.MP3.getType(), (int) (System.currentTimeMillis() - timeRecording)));
    }

    private void captureAudioChunks() {
        short sRecordedPiece[] = new short[BufferElements2Rec];
        initilizeAudioChunk();

        while (isRecording) {
            if(bDataSize == audioChunkSize){
                makeAudioChunkPost();
                initilizeAudioChunk();
            }
            audioRecorder.read(sRecordedPiece, 0, BufferElements2Rec);
            byte [] bRecordedPiece = short2byte(sRecordedPiece);
            System.arraycopy(bRecordedPiece, 0, bData, bDataSize, bRecordedPiece.length);
            bDataSize +=  BufferElements2Rec*BytesPerElement;
        }
    }

    private void initilizeAudioChunk(){
        bData = new byte[audioChunkSize];
        bDataSize = 0;
        timeRecording = System.currentTimeMillis();
    }

    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }

    private String createMicName() {
        return "MIC_" + System.currentTimeMillis();
    }

}
