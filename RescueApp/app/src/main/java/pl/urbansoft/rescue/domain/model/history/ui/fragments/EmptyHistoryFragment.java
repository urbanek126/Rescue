package pl.urbansoft.rescue.domain.model.history.ui.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.urbansoft.rescue.R;

/**
 * Created by urbanek126 on 12/21/2015.
 */
public class EmptyHistoryFragment extends Fragment {

    TextView historyEmptyTextView;

    public EmptyHistoryFragment() {
    }

    public static EmptyHistoryFragment newInstance() {
        EmptyHistoryFragment fragment = new EmptyHistoryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_history_empty, container, false);

        historyEmptyTextView = (TextView) v.findViewById(R.id.historyEmptyTextView);

        setFonts();
        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setFonts() {
        Typeface RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");

        historyEmptyTextView.setTypeface(RobotoRegular);
    }
}
