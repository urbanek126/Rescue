package pl.urbansoft.rescue.domain.shared.handler;


import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.infrastructure.injection.RescueComponent;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public class UserHandler {
    private static final String TAG = UserHandler.class.getSimpleName();

    @Inject EventBus eventBus;
    @Inject UserService userService;

    public UserHandler(RescueComponent component){
        component.inject(this);
        eventBus.register(this);
    }

    public void onEvent(UserUpdatedEvent e) {
        userService.updateUser();
    }

}
