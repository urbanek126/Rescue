package pl.urbansoft.rescue.domain.model.analitics.weka;

/**
 * Created by andrzej on 12/27/15.
 */
public interface RescueClassifier {
    double classify(Object[] i) throws Exception;
}
