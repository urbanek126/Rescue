package pl.urbansoft.rescue.domain.model.information;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.information.fragments.DotsTutorialFragment;
import pl.urbansoft.rescue.domain.model.information.fragments.TutorialAdapter;
import pl.urbansoft.rescue.domain.shared.ui.BaseActivity;

/**
 * Created by urbanek126 on 4/30/2016.
 */
public class TutorialActivity extends BaseActivity {
    private static final String TAG = TutorialActivity.class.getSimpleName();
    private Fragment dotsIntroductionFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((RescueApp) getApplication()).getRescueComponent().inject(this);
        setContentView(R.layout.activity_tutorial);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setDotsIntroductionFragment();
        setIntroductionPager();

        Log.i(TAG, "oncreate()");
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_tutorial, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.skip:
                finish();
       //         startActivity(new Intent(this, ListPromotionsActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setIntroductionPager(){
        ViewPager pager = (ViewPager) findViewById(R.id.introduction_pager);
        FragmentPagerAdapter introductionAdapter = new TutorialAdapter(getSupportFragmentManager());
        pager.setAdapter(introductionAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                ((DotsTutorialFragment) dotsIntroductionFragment).setDot(position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setDotsIntroductionFragment(){
        dotsIntroductionFragment = new DotsTutorialFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.dots_frame, dotsIntroductionFragment)
                .commit();
    }

}
