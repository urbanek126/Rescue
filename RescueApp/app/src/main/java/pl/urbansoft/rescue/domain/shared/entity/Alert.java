package pl.urbansoft.rescue.domain.shared.entity;


import com.braisgabin.couchbaseliteorm.Entity;
import com.braisgabin.couchbaseliteorm.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import pl.urbansoft.rescue.domain.shared.entity.enums.ContentType;

/**
 * Created by urbanek126 on 12/20/2015.
 */
@Entity("alert")
public class Alert extends AbstractDocument {
    //ToDo - change to userId
    @Field("idUser")
    String idUser;
    @Field("locations")
    List<Location> locations;
    @Field("contact")
    Contact contact;
    @Field("recordedDuration")
    int recordedDuration;
    @Field("messageCount")
    int messageCount;
    @Field("timeStamp")
    long timeStamp;

    public Alert() {
        locations = new ArrayList<>();
        messageCount = 0;
        recordedDuration = 0;
    }

    public String toString() {
        return " idUser: " + idUser + " locations: " + locations + " contact: " + contact + " recordedDuration: " + recordedDuration + " messageCount: " + messageCount;
    }

    public void addLocation(Location location) {
        locations.add(location);
    }

    public int getRecordedDuration() {
        return recordedDuration;
    }

    public void addToRecordedDuration(int duration) {
        recordedDuration += duration ;
    }

    public void increaseMessageCounter() {
        messageCount++;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setRecordedDuration(int recordedDuration) {
        this.recordedDuration = recordedDuration;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public String getIdUser() {
        return idUser;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public int getAttachmentsCountByType(ContentType type) {

        int result = 0;
        for (String fileName : getAttachmentNames()) {
            if (fileName.startsWith(type.getName())) {
                result++;
            }
        }
        return result;
    }
}
