package pl.urbansoft.rescue.domain.model.information;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.information.fragments.InformationFragment;
import pl.urbansoft.rescue.domain.shared.ui.BaseActivity;

/**
 * Created by urbanek126 on 3/6/2016.
 */
public class InformationActivity extends BaseActivity {
    private static final String TAG = InformationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((RescueApp) getApplication()).getRescueComponent().inject(this);
        Log.i(TAG, "oncreate()");
        setContentView(R.layout.activity_information);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.information_frame, new InformationFragment())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_information, menu);
        return true;
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause()");
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
        super.onDestroy();
    }
}
