package pl.urbansoft.rescue.domain.model.alert;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.shared.entity.Alert;
import pl.urbansoft.rescue.domain.shared.entity.User;

/**
 * Created by andrzej on 1/4/16.
 */
public class AlertContext {

    private static final String TAG =AlertContext.class.getSimpleName();

    private static Alert alert;

    private static Boolean isActive = Boolean.FALSE;

    public static synchronized final Alert getCurrentAlert(User user, boolean forceCreate){
        if( forceCreate ){
            Log.i(TAG,"Creating new alarm");
            alert = new Alert();
            alert.setIdUser(user.getDocumentId());
            alert.setContact(user.getContact());
            alert.setTimeStamp(System.currentTimeMillis());
            isActive=Boolean.TRUE;
        }
        return alert;
    }

    public static void closeAlert(){
        Log.i(TAG,"Closing alarm");
        isActive = false;
    }

    public static boolean isAlertActive(){
        return isActive;
    }
}
