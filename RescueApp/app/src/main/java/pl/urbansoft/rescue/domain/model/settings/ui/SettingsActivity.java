package pl.urbansoft.rescue.domain.model.settings.ui;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.IconBarFragment;
import pl.urbansoft.rescue.domain.model.settings.ui.fragments.BaseSettingsFragment;
import pl.urbansoft.rescue.domain.model.settings.ui.fragments.ExtraSettingsFragment;
import pl.urbansoft.rescue.domain.model.settings.ui.fragments.SettingsPageAdapter;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.ui.BaseActivity;
import pl.urbansoft.rescue.domain.shared.ui.fragments.BleFragment;


public class SettingsActivity extends BaseActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();
    private FragmentManager fragmentManager;

    public static final int CAMERA_PERMISSION_REQUEST = 1;
    public static final int LOCATION_PERMISSION_REQUEST = 2;
    public static final int SEND_SMS_PERMISSION_REQUEST = 3;
    public static final int MICROPHONE_PERMISSION_REQUEST = 4;

    @Inject
    UserService userService;

    @Inject
    EventBus eventBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ((RescueApp) getApplication()).getRescueComponent().inject(this);

        fragmentManager = getFragmentManager();
        Log.i(TAG, "SettingsActivity:oncreate()" + this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        initViewPagerAndTabs();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "SettingsActivity:OnStart()" + this);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "SettingsActivity:onResume()" + this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "SettingsActivity:Onstop()" + this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "SettingsActivity:onDestroy()" + this);
    }

    @Override
    public void onEvent(UserExternalButtonUpdatedEvent event) {
        super.onEvent(event);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case CAMERA_PERMISSION_REQUEST:
                    userService.getUser().setIsCamera(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.CAMERA));
                    break;
                case LOCATION_PERMISSION_REQUEST:
                    userService.getUser().setIsLocation(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.LOCATION));
                    break;
                case SEND_SMS_PERMISSION_REQUEST:
                    userService.getUser().setIsMessage(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.MESSAGE));
                    break;
                case MICROPHONE_PERMISSION_REQUEST:
                    userService.getUser().setIsMicrophone(true);
                    eventBus.post(new UserUpdatedEvent(RescueApp.MICROPHONE));
                    break;
            }
        }
    }

    private void initViewPagerAndTabs() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.settings_viewpager);
        SettingsPageAdapter pagerAdapter = new SettingsPageAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(new BaseSettingsFragment(), getString(R.string.screen_settings_base));
        pagerAdapter.addFragment(new ExtraSettingsFragment(), getString(R.string.screen_settings_extra));
        viewPager.setAdapter(pagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.settigns_tabs_layout);
        tabLayout.setupWithViewPager(viewPager);
        setTabFonts(tabLayout);
    }

    private void setTabFonts(TabLayout tabLayout) {
        Typeface RobotoRegular = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = viewGroup.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup viewGroupTab = (ViewGroup) viewGroup.getChildAt(j);
            int tabChildsCount = viewGroupTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = viewGroupTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(RobotoRegular);
                }
            }
        }
    }
}
