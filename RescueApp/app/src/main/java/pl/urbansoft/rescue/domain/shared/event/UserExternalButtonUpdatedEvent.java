package pl.urbansoft.rescue.domain.shared.event;

/**
 * Created by andrzej on 12/29/15.
 */
public class UserExternalButtonUpdatedEvent extends UserUpdatedEvent {
    private boolean isExteranlButtonUsed = false;

    public UserExternalButtonUpdatedEvent(boolean isUsed){
        this.isExteranlButtonUsed = isUsed;
    }

    public boolean isExteranlButtonUsed(){
        return  isExteranlButtonUsed;
    }
}
