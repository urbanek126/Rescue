package pl.urbansoft.rescue.infrastructure.ble.event;

/**
 * Created by andrzej on 11/28/15.
 */
public class BleDevice {

    private String mac;
    private String name ="HTC Fetch";

    private Status status = null;

    public BleDevice(){
        status = Status.SEARCHING;
    }

    public BleDevice(Status status){
        this.status = status;
    }

    public BleDevice(String mac, Status status){
        this.mac = mac;
        this.status =status;
    }

    public Status getStatus(){
        return status;
    }

    public String getName(){
        return name;
    }
    public String getMac() {
        return mac;
    }

    public enum Status{
        INITIALIZED,SEARCHING, DISCOVERED, CONNECTED, ENABLED_NOTIFICATION;
    }
}
