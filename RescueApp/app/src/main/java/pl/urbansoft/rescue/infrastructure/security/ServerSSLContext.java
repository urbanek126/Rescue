package pl.urbansoft.rescue.infrastructure.security;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import pl.urbansoft.rescue.application.RescueApp;

/**
 * Created by urbanek126 on 1/1/2016.
 */
public class ServerSSLContext {
    private static final String TAG = ServerSSLContext.class.getSimpleName();
    private static final String CERTIFICATE_FILE = "server.cer";
    private static SSLContext context;
    private static KeyStore keyStore;

    public static SSLContext getSSLContext() {
        if (context == null) {
            context = createContext();
        }
        return context;
    }

    public static KeyStore getKeyStore() {
        if (keyStore == null) {
            createContext();
        }
        return keyStore;
    }
    
    private static SSLContext createContext() {
        SSLContext context = null;
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(RescueApp.getContext().getAssets().open(CERTIFICATE_FILE));
            Certificate ca = cf.generateCertificate(caInput);
            caInput.close();

            String keyStoreType = KeyStore.getDefaultType();
            keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

        } catch (Exception e) {
            Log.e(TAG, "SSLContextProvider:" + e.getMessage());
            e.printStackTrace();
        }
        return context;
    }
}
