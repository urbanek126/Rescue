/*
* Copyright (C) 2014 The Android Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package pl.urbansoft.rescue.domain.model.information.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import javax.inject.Inject;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;

public class InformationFragment extends Fragment {
    private static final String TAG = InformationFragment.class.getSimpleName();
    private static final String WEBSITE_ADDRESS = "http://www.high5.zone";

    private static final int DIVIDER_HEIGHT = 0;
    private static final int NUMBER_OF_POPUP_GROUP = 1;
    private static final int MEASURE_SPEC_ARGUMENT = 0;

    private static final int EXPANDABLE_LIST_MIN_HEIGHT = 0;
    private static final int EXPANDABLE_LIST_DEFAULT_HEIGHT = 0;

    private static final int SCROLLVIEW_MARGIN_LEFT = 0;
    private static final int SCROLLVIEW_MARGIN_TOP = 0;
    private static final int SCROLLVIEW_MARGIN_BOTTOM = 0;

    private static final int NUMBER_OF_JUSTIFIED_TEXTS = 5;
    private final WebView[] webViewTable = new WebView[NUMBER_OF_JUSTIFIED_TEXTS];
    private final String[] stringResourceTable = new String[NUMBER_OF_JUSTIFIED_TEXTS];

    private View v;
    private ArrayList<String> groupItem = new ArrayList<>();
    private ExpandableListView expandableList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);
        v = inflater.inflate(R.layout.fragment_information, container, false);

        setExpandableList();
        setWebLink(v);

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP)
            setInitialListViewHeight(expandableList);

        Log.i(TAG, "onCreateView()");

        return v;
    }


    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    private void setWebLink(View v) {
        TextView high5WebsiteButton = (TextView) v.findViewById(R.id.high5_website_button);

        high5WebsiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(WEBSITE_ADDRESS));
                startActivity(browserIntent);
            }
        });
    }


    private void setExpandableList() {
        expandableList = (ExpandableListView) v.findViewById(R.id.explist);
        expandableList.setDividerHeight(DIVIDER_HEIGHT);
        expandableList.setGroupIndicator(null);
        expandableList.setClickable(true);

        ExpandableInformationAdapter mNewAdapter = new ExpandableInformationAdapter(getActivity(), getPopupDialog());
        mNewAdapter.setInflater(
                (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE),
                getActivity());

        expandableList.setAdapter(mNewAdapter);

        expandableList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int position, long id) {
                setListViewHeight(parent, position);
                return false;
            }
        });
    }

    private Dialog getPopupDialog() {
        final Dialog rulesPopupDialog = new Dialog(getActivity(), R.style.FullHeightDialog);
        rulesPopupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        rulesPopupDialog.setContentView(R.layout.information_regules_popup);

        WindowManager.LayoutParams wmlp = rulesPopupDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL;

        ImageView informationRegulesPopupDeleteButton = (ImageView) rulesPopupDialog.findViewById(R.id.information_regules_popup_delete_button);
        informationRegulesPopupDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableList.collapseGroup(NUMBER_OF_POPUP_GROUP);
                rulesPopupDialog.dismiss();
            }
        });

        initJustifiedTexts(rulesPopupDialog);
        setJustifiedTexts();

        scrollbarShiftInPopupDialog(informationRegulesPopupDeleteButton, rulesPopupDialog);

        return rulesPopupDialog;
    }

    private void setJustifiedTexts(){
        for(int i=0; i<NUMBER_OF_JUSTIFIED_TEXTS; i++){
            webViewTable[i].loadData("<p style=\"text-align: justify\">"+ stringResourceTable[i] +"</p>", "text/html; charset=utf-8", "UTF-8");

            final WebSettings webSettings = webViewTable[i].getSettings();
            float fontSize = getResources().getDimension(R.dimen.dimen_big_text) / getResources().getDisplayMetrics().density;
            webSettings.setDefaultFontSize((int)fontSize);
        }
    }

    private void initJustifiedTexts(Dialog rulesPopupDialog){
        stringResourceTable[0] = getResources().getString(R.string.rules_first_text);
        stringResourceTable[1] = getResources().getString(R.string.rules_second_text);
        stringResourceTable[2] = getResources().getString(R.string.rules_third_text);
        stringResourceTable[3] = getResources().getString(R.string.rules_fourth_text);
        stringResourceTable[4] = getResources().getString(R.string.rules_fifth_text);

        webViewTable[0] = (WebView) rulesPopupDialog.findViewById(R.id.webView0);
        webViewTable[1] = (WebView) rulesPopupDialog.findViewById(R.id.webView1);
        webViewTable[2] = (WebView) rulesPopupDialog.findViewById(R.id.webView2);
        webViewTable[3] = (WebView) rulesPopupDialog.findViewById(R.id.webView3);
        webViewTable[4] = (WebView) rulesPopupDialog.findViewById(R.id.webView4);
    }

    private void scrollbarShiftInPopupDialog(ImageView informationRegulesPopupDeleteButton, Dialog rulesPopupDialog) {
        ScrollView scrollView = (ScrollView) rulesPopupDialog.findViewById(R.id.rules_scroll_view);

        informationRegulesPopupDeleteButton.measure(View.MeasureSpec.makeMeasureSpec(MEASURE_SPEC_ARGUMENT, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(MEASURE_SPEC_ARGUMENT, View.MeasureSpec.UNSPECIFIED));

        int shift = informationRegulesPopupDeleteButton.getMeasuredWidth() / 2;

        int scrollBarSize;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
            scrollBarSize = scrollView.getScrollBarSize() / 2;
        else
            scrollBarSize = 0;

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(SCROLLVIEW_MARGIN_LEFT, SCROLLVIEW_MARGIN_TOP, shift - scrollBarSize, SCROLLVIEW_MARGIN_BOTTOM);

        scrollView.setLayoutParams(layoutParams);
    }


    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();

        int totalHeight = countListViewHeight(listView, group);

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (isListHeightTooSmall(height))
            height = EXPANDABLE_LIST_DEFAULT_HEIGHT;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private boolean isListHeightTooSmall(int height){
        return height < EXPANDABLE_LIST_MIN_HEIGHT;
    }

    private int countListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);

        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null, listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            }
        }

        return totalHeight;
    }

    private void setInitialListViewHeight(ExpandableListView listView) {
        ListAdapter mAdapter = listView.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < mAdapter.getCount(); i++) {
            View mView = mAdapter.getView(i, null, listView);

            mView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            totalHeight += mView.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (mAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

}
