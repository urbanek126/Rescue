package pl.urbansoft.rescue.infrastructure.ble.event;

/**
 * Created by andrzej on 11/28/15.
 */
public class HtcTagConnectEvent  extends BleAbstractEvent {

    public HtcTagConnectEvent(BleDevice device) {
        super(device);
    }

    public HtcTagConnectEvent(){
        super(null);
    }
}
