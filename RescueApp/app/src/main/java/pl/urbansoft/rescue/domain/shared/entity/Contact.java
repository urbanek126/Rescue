package pl.urbansoft.rescue.domain.shared.entity;

import com.braisgabin.couchbaseliteorm.Entity;
import com.braisgabin.couchbaseliteorm.Field;

/**
 * Created by Marek on 2015-08-20.
 */

@Entity("contact")
public class Contact {
    @Field("name")
    String name;
    @Field("number")
    String number;

    public Contact() {
        this.number = "";
        this.name = "";
    }

    public Contact(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public String toString() {
        return name + " " + number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object object) {
        boolean isSame = false;
        if (object != null && object instanceof Contact) {
            isSame = (this.name.equals(((Contact) object).getName())) && (this.number == ((Contact) object).getNumber());
        }
        return isSame;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode() + number.hashCode();
    }
}
