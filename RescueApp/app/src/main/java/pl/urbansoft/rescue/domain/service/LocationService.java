package pl.urbansoft.rescue.domain.service;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.AlertContext;
import pl.urbansoft.rescue.domain.shared.entity.Location;
import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;
import pl.urbansoft.rescue.domain.shared.entity.enums.EventCode;
import pl.urbansoft.rescue.domain.shared.event.AlertRespondedEvent;
import pl.urbansoft.rescue.domain.shared.event.CountryEvent;
import pl.urbansoft.rescue.domain.shared.event.LocationUpdatedEvent;

/**
 * Created by urbanek126 on 12/27/2015.
 */
public class LocationService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    @Inject
    EventBus eventBus;

    private static final String TAG = LocationService.class.getSimpleName();
    private static final int ONE_SECOND_IN_MILLISECONDS = 1000;
    private static final String ERROR_MESSAGE = "OnConnectionFailed to GoogleApi";
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Context context;

    public LocationService() {
        ((RescueApp) RescueApp.getContext()).getRescueComponent().inject(this);
        context = RescueApp.getContext();
        initializeGoogleApiClient();
    }

    public void startLocationUpdate(int interval) {
        if (mLocationRequest == null) {
            createLocationRequest(interval);
        }

        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    public void stopLocationUpdate() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onLocationChanged(android.location.Location mlocation) {
        Location location = creatLocation(mlocation);
        if (AlertContext.isAlertActive() && location != null) {
            eventBus.post(new LocationUpdatedEvent(location));
            Log.i(TAG, "LocationUpdatedEvent" + location);
        } else {
            Log.i(TAG, "Alert is not active. Location is not updating" + mlocation);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Location location = creatLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
        if (location != null) {
            eventBus.post(new CountryEvent(getCountryCode(location)));
            eventBus.post(new LocationUpdatedEvent(location));
        }

        if(AlertContext.isAlertActive() && location != null) {
            eventBus.post(new LocationUpdatedEvent(location));
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed");
        eventBus.post(new AlertRespondedEvent(EventCode.ERROR.getCode(), ERROR_MESSAGE, AlertInfoType.LOCATION));
    }

    private void initializeGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(RescueApp.getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

    }

    private void createLocationRequest(int interval) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(interval * ONE_SECOND_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private String getCountryCode(Location location) {
        Geocoder gcd = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            Log.e(TAG, "getCountryCode");
        }
        if (addresses != null && addresses.size() != 0)
            return addresses.get(0).getCountryCode();
        return null;
    }

    private Location creatLocation(android.location.Location mlocation) {
        if (mlocation != null) {
            Location location = new Location();
            location.setLatitude(mlocation.getLatitude());
            location.setLongitude(mlocation.getLongitude());
            location.setTimeStamp(mlocation.getTime());
            return location;
        }
        return null;
    }


    public Location getLocation() {
        android.location.Location location = null;

        try {
            LocationManager mLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            // getting GPS status
            boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    Log.d("Network", "Network");
                    if (mLocationManager != null) {
                        location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
                //get the location by gps
                if (isGPSEnabled) {
                    if (location == null) {
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (mLocationManager != null) {
                            location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return creatLocation(location);
    }
}
