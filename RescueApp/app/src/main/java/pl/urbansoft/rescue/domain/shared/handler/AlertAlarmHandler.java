package pl.urbansoft.rescue.domain.shared.handler;


import android.content.Intent;
import android.util.Log;


import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.alert.AlertContext;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToInActiveEvent;


import pl.urbansoft.rescue.domain.service.AlertService;

import pl.urbansoft.rescue.domain.model.alert.event.AlertChangedToPreActiveEvent;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;
import pl.urbansoft.rescue.domain.service.CameraService;
import pl.urbansoft.rescue.domain.service.LocationService;
import pl.urbansoft.rescue.domain.service.MicroPhoneService;
import pl.urbansoft.rescue.domain.service.PhoneReactionService;
import pl.urbansoft.rescue.domain.service.PhoneService;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.entity.Alert;
import pl.urbansoft.rescue.domain.shared.entity.Location;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.entity.enums.AlertInfoType;
import pl.urbansoft.rescue.domain.shared.entity.enums.EventCode;
import pl.urbansoft.rescue.domain.shared.event.AlertAddedPhotoAttachmentEvent;
import pl.urbansoft.rescue.domain.shared.event.AlertAddedRecordAttachmentEvent;
import pl.urbansoft.rescue.domain.shared.event.AlertRespondedEvent;
import pl.urbansoft.rescue.domain.shared.event.LocationUpdatedEvent;
import pl.urbansoft.rescue.infrastructure.injection.RescueComponent;

/**
 * Created by urbanek126 on 12/18/2015.
 */
public class AlertAlarmHandler {
    private static final String TAG = AlertAlarmHandler.class.getSimpleName();
    private User user;
    private Location location;

    @Inject
    EventBus eventBus;
    @Inject
    CameraService cameraService;
    @Inject
    PhoneService phoneService;
    @Inject
    MicroPhoneService microPhoneService;
    @Inject
    PhoneReactionService phoneReactionService;
    @Inject
    LocationService locationService;
    @Inject
    UserService userService;
    @Inject
    AlertService alertService;

    public AlertAlarmHandler(RescueComponent component) {
        component.inject(this);
        eventBus.register(this, 1);
    }

    public void onEvent(AlertChangedToActiveEvent e) {
        if(!AlertContext.isAlertActive()) {

            user = userService.getUser();
            alertService.createNewAlert();

            if (user.isMessage() && !user.isLocation())
                phoneService.sendSMS(user.getContact().getNumber(), user.getMessageContent(), locationService.getLocation(), user.getMessageInterval());
            if (user.isLocation())
                locationService.startLocationUpdate(user.getMessageInterval());
            if (user.isSiren())
                phoneReactionService.startSiren();
            if (user.isVibration())
                phoneReactionService.startVibrate();
            if (user.isCamera())
                cameraService.startTakingPhotos(user.getCameraInterval());
            if (user.isMicrophone())
                microPhoneService.startRecording(user.getMicrophoneDuration());
        }
        else{
            Log.i(TAG, "There is already active alarm");
        }
    }


    public void onEvent(AlertChangedToInActiveEvent e) {
        AlertContext.closeAlert();

        User user = userService.getUser();
        if (user.isLocation())
            locationService.stopLocationUpdate();
        if (user.isSiren())
            phoneReactionService.stopSiren();
        if (user.isVibration())
            phoneReactionService.stopVibrate();
        if (user.isCamera())
            cameraService.stopTakingPhotos();
        if (user.isMicrophone())
            microPhoneService.stopRecording();

    }

    public void onEvent(LocationUpdatedEvent e) {
        location = e.getLocation();
        user = userService.getUser();
        Alert alert = AlertContext.getCurrentAlert(user,false);
        Log.d(TAG, "LocationUpdatedEvent:" + location);

        if (AlertContext.isAlertActive() && user.isLocation() && user.isMessage()) {
            alert.addLocation(location);
            phoneService.sendSMS(user.getContact().getNumber(), user.getMessageContent(), location, user.getMessageInterval());
            alertService.updateAlert();
        }
        else if(AlertContext.isAlertActive() && user.isLocation()){
            alert.addLocation(location);
            alertService.updateAlert();
        }
    }

    public void onEvent(AlertAddedPhotoAttachmentEvent e) {
        alertService.addAttachment(e.getBData(), e.getName(), e.getContentType());
    }

    public void onEvent(AlertAddedRecordAttachmentEvent e) {
        AlertContext.getCurrentAlert(user, false).addToRecordedDuration(e.getRecordedDuration());
        alertService.addAttachment(e.getBData(), e.getName(), e.getContentType());
        alertService.updateAlert();
    }

    public void onEvent(AlertRespondedEvent e ){
        System.out.println("SMS"+e);
        if((e.getAlertParameter() == AlertInfoType.MESSAGE) && e.getResponseCode() == EventCode.SUCCESS.getCode()){
            System.out.println("SMSTUTAJ"+e);
            AlertContext.getCurrentAlert(user,false).increaseMessageCounter();
            alertService.updateAlert();
        }

    }

    /*
        Start activity to handle alert if doesn't exist
     */
    public void onEvent(AlertChangedToPreActiveEvent e) {
        if (!AlertActivity.isActive) {
            Log.d(TAG, "Creating alert Activity from hanlder");
            Intent i = new Intent(RescueApp.getContext(), AlertActivity.class);
            i.putExtra(AlertActivity.PRE_ALERT_RAISED, true);
            i.putExtra(AlertActivity.ALERT_CONFIRMED, false);

            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            RescueApp.getContext().startActivity(i);
        }

        // send event
        Tracker t = RescueApp.getTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder()
                .setCategory("Alarm")
                .setAction("Tiggered")
                .setLabel(e.getType().name())
                .build());
    }
}
