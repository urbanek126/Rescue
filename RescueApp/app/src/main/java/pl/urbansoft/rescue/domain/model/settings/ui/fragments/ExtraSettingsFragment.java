package pl.urbansoft.rescue.domain.model.settings.ui.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rey.material.widget.Slider;
import com.rey.material.widget.Switch;

import javax.inject.Inject;

import de.greenrobot.event.EventBus;
import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;
import pl.urbansoft.rescue.domain.model.settings.ui.SettingsActivity;
import pl.urbansoft.rescue.domain.shared.entity.User;
import pl.urbansoft.rescue.domain.shared.entity.enums.PhoneSensitivity;
import pl.urbansoft.rescue.domain.shared.event.UserExternalButtonUpdatedEvent;
import pl.urbansoft.rescue.domain.shared.event.UserUpdatedEvent;
import pl.urbansoft.rescue.domain.service.UserService;

import static pl.urbansoft.rescue.domain.shared.entity.enums.PhoneSensitivity.*;

/**
 * Created by urbanek126 on 12/19/2015.
 */
public class ExtraSettingsFragment extends Fragment {

    @Inject
    UserService userService;
    @Inject
    EventBus eventBus;
    private View v;

    Switch headPhonePluginAlertSwitch;
    Switch powerDeviceButtonAlertSwitch;
    Switch autoDetectionAlertSwitch;
    Switch vibrationSwitch;
    Switch sirenSwitch;
    Switch externalButtonAlertSwitch;
    Slider cancelDurationSlider;
    Slider autoDetectionSensitivitySlider;
    TextView headPhonePluginAlertTextView;
    TextView autoDetectionAlertTextView;
    TextView powerDeviceButtonAlertTextView;
    TextView externalButtonAlertTextView;
    TextView vibrationTextView;
    TextView sirenTextView;
    TextView autoDetectionSensitivityThumBallText;
    LinearLayout autoDetectionSensitivityLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_settings_extra, container, false);
        ((RescueApp) getActivity().getApplication()).getRescueComponent().inject(this);

        sirenSwitch = (Switch) v.findViewById(R.id.sirenSwitch);
        vibrationSwitch = (Switch) v.findViewById(R.id.vibrationSwitch);
        headPhonePluginAlertSwitch = (Switch) v.findViewById(R.id.headPhonePluginAlertSwitch);
        powerDeviceButtonAlertSwitch = (Switch) v.findViewById(R.id.powerDeviceButtonAlertSwitch);
        externalButtonAlertSwitch = (Switch) v.findViewById(R.id.externalButtonAlertSwitch);
        autoDetectionAlertSwitch = (Switch) v.findViewById(R.id.autoDetectionAlertSwitch);
        cancelDurationSlider = (Slider) v.findViewById(R.id.cancelDurationSlider);
        autoDetectionSensitivitySlider = (Slider) v.findViewById(R.id.autoDetectionSensitivitySlider);
        headPhonePluginAlertTextView = (TextView) v.findViewById(R.id.headPhonePluginAlertTextView);
        powerDeviceButtonAlertTextView = (TextView) v.findViewById(R.id.powerDeviceButtonAlertTextView);
        externalButtonAlertTextView = (TextView) v.findViewById(R.id.externalButtonAlertTextView);
        vibrationTextView = (TextView) v.findViewById(R.id.vibrationTextView);
        sirenTextView = (TextView) v.findViewById(R.id.sirenTextView);
        autoDetectionSensitivityThumBallText = (TextView) v.findViewById(R.id.autoDetectionSensitivityThumBallText);
        autoDetectionAlertTextView = (TextView) v.findViewById(R.id.autoDetectionAlertTextView);
        autoDetectionSensitivityLayout = (LinearLayout) v.findViewById(R.id.autoDetectionSensitivityLayout);

        setFonts();
        setCancelTimeSlider();
        setAutoDetectionSensitivitySlider();
        setSirenSwitch();

        setheadPhonePluginAlertSwitch();
        setPowerDeviceButtonAlertSwitch();
        setExternalButtonAlertSwitch();
        setAutoDetectionAlertSwitch();

        setVibrationSwitch();
        initializeComponents();

        return v;
    }

    public void onEvent(UserExternalButtonUpdatedEvent event) {
        ((SettingsActivity) getActivity()).onEvent(event);
        LinearLayout ExtraSettingsView = (LinearLayout) v.findViewById(R.id.ExtraSettingsView);
        if (event.isExteranlButtonUsed()) {
            ExtraSettingsView.setPadding(0, 0, 0, (int) getResources().getDimension(R.dimen.dimen_xxlarge));
        } else {
            ExtraSettingsView.setPadding(0, 0, 0, 0);
        }
    }

    private void setheadPhonePluginAlertSwitch() {
        headPhonePluginAlertSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean isChecked) {
                if (isChecked) {
                    headPhonePluginAlertTextView.setAlpha(0.89f);
                    userService.getUser().setHeadPhonePluginAlert(true);
                } else {
                    userService.getUser().setHeadPhonePluginAlert(false);
                    headPhonePluginAlertTextView.setAlpha(0.5f);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
    }

    private void setPowerDeviceButtonAlertSwitch() {
        powerDeviceButtonAlertSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean isChecked) {
                if (isChecked) {
                    powerDeviceButtonAlertTextView.setAlpha(0.89f);
                    userService.getUser().setPowerDeviceButtonAlert(true);
                } else {
                    userService.getUser().setPowerDeviceButtonAlert(false);
                    powerDeviceButtonAlertTextView.setAlpha(0.5f);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
    }

    private void setExternalButtonAlertSwitch() {
        externalButtonAlertSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean isChecked) {
                if (isChecked) {
                    externalButtonAlertTextView.setAlpha(0.89f);
                    userService.getUser().setExternalButtonAlert(true);
                } else {
                    userService.getUser().setExternalButtonAlert(false);
                    externalButtonAlertTextView.setAlpha(0.5f);
                }
                eventBus.post(new UserExternalButtonUpdatedEvent(isChecked));

            }
        });
    }

    private void setSirenSwitch() {
        sirenSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean isChecked) {
                if (isChecked) {
                    sirenTextView.setAlpha(0.89f);
                    userService.getUser().setIsSiren(true);
                } else {
                    userService.getUser().setIsSiren(false);
                    sirenTextView.setAlpha(0.5f);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
    }

    private void setVibrationSwitch() {
        vibrationSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean isChecked) {
                if (isChecked) {
                    userService.getUser().setIsVibration(true);
                    vibrationTextView.setAlpha(0.89f);
                } else {
                    userService.getUser().setIsVibration(false);
                    vibrationTextView.setAlpha(0.5f);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
    }

    private void setAutoDetectionAlertSwitch() {
        autoDetectionAlertSwitch.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch view, boolean isChecked) {
                if (isChecked && autoDetectionSensitivitySlider.getValue() == 0)
                    autoDetectionSensitivitySlider.setValue(PhoneSensitivity.MEDIUM.getValue(), true);
                if (isChecked) {
                    autoDetectionSensitivitySlider.setEnabled(true);
                    autoDetectionSensitivityThumBallText.setAlpha(0.89f);
                    autoDetectionAlertTextView.setAlpha(0.89f);
                    userService.getUser().setAutoDetection(true);
                } else {
                    autoDetectionSensitivitySlider.setEnabled(false);
                    userService.getUser().setAutoDetection(false);
                    autoDetectionSensitivityThumBallText.setAlpha(0.5f);
                    autoDetectionAlertTextView.setAlpha(0.5f);
                }
                eventBus.post(new UserUpdatedEvent());
            }
        });
    }

    private void setCancelTimeSlider() {
        cancelDurationSlider.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                userService.getUser().setCancelDuration(newValue);
                eventBus.post(new UserUpdatedEvent());
            }
        });
    }

    private void setAutoDetectionSensitivitySlider() {
        autoDetectionSensitivitySlider.setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                if (isAutoDetectionSensitivity(newValue, oldValue)) {
                    autoDetectionSensitivitySlider.setValue(PhoneSensitivity.NONE.getValue(), true);
                    autoDetectionSensitivitySlider.setEnabled(false);
                    autoDetectionAlertSwitch.setChecked(false);
                }
                userService.getUser().setPhoneSensitivity(newValue);
                eventBus.post(new UserUpdatedEvent());
                setPhoneSensitivityThumBallText(newValue);
            }
        });
    }

    private boolean isAutoDetectionSensitivity(float newValue, int oldValue) {
        int sensitivityOff = PhoneSensitivity.NONE.getValue();
        return newValue == sensitivityOff && oldValue != sensitivityOff;
    }

    private void initializeComponents() {
        User user = userService.getUser();
        headPhonePluginAlertSwitch.setChecked(user.isHeadPhonePluginAlert());
        powerDeviceButtonAlertSwitch.setChecked(user.isPowerDeviceButtonAlert());
        externalButtonAlertSwitch.setChecked(user.isExternalButtonAlert());
        sirenSwitch.setChecked(user.isSiren());
        vibrationSwitch.setChecked(user.isVibration());
        cancelDurationSlider.setValue(user.getCancelDuration(), false);
        autoDetectionSensitivitySlider.setValue(user.getPhoneSensitivity(), false);
        setPhoneSensitivityThumBallText(user.getPhoneSensitivity());
        autoDetectionAlertSwitch.setChecked(user.isAutoDetection());
    }

    private void setPhoneSensitivityThumBallText(int value) {
        PhoneSensitivity phoneSensitivity = getPhoneSensitivityName(value);
        switch (phoneSensitivity) {
            case NONE:
                autoDetectionSensitivityThumBallText.setText(getResources().getString(R.string.screen_settings_extra_autoDetectionSensitivity_none));
                autoDetectionSensitivityThumBallText.setAlpha(0.5f);
                break;
            case HIGH:
                autoDetectionSensitivityThumBallText.setText(getResources().getString(R.string.screen_settings_extra_autoDetectionSensitivity_high));
                autoDetectionSensitivityThumBallText.setAlpha(0.89f);
                break;
            case MEDIUM:
                autoDetectionSensitivityThumBallText.setText(getResources().getString(R.string.screen_settings_extra_autoDetectionSensitivity_medium));
                autoDetectionSensitivityThumBallText.setAlpha(0.89f);
                break;
            default:
                autoDetectionSensitivityThumBallText.setText(getResources().getString(R.string.screen_settings_extra_autoDetectionSensitivity_small));
                autoDetectionSensitivityThumBallText.setAlpha(0.89f);
        }
    }

    private void setFonts() {
        Typeface RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");

        TextView cancelDurationText = (TextView) v.findViewById(R.id.cancelDurationText);
        TextView phoneReactionText = (TextView) v.findViewById(R.id.phoneReactionText);
        TextView alarmPosibiltiesTextView = (TextView) v.findViewById(R.id.alarmPosibiltiesTextView);

        alarmPosibiltiesTextView.setTypeface(RobotoBold);
        cancelDurationText.setTypeface(RobotoBold);
        phoneReactionText.setTypeface(RobotoBold);
        vibrationTextView.setTypeface(RobotoRegular);
        sirenTextView.setTypeface(RobotoRegular);
        headPhonePluginAlertTextView.setTypeface(RobotoRegular);
        powerDeviceButtonAlertTextView.setTypeface(RobotoRegular);
        externalButtonAlertTextView.setTypeface(RobotoRegular);
        autoDetectionSensitivityThumBallText.setTypeface(RobotoRegular);
        autoDetectionAlertTextView.setTypeface(RobotoRegular);
    }
}
