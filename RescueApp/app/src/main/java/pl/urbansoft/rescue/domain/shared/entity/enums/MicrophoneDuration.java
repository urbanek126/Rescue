package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public enum MicrophoneDuration {
    SHORT(60,0),
    LONG(300,1),
    VERY_LONG(600,2);

    private int value;
    private int position;

    private MicrophoneDuration(int value, int position) {
        this.value = value;
        this.position = position;
    }

    public int getValue() {
        return value;
    }


    public int getPosition() {
        return position;
    }


    public static int getMicrophoneDurationPosition(int value) {

        if (value == MicrophoneDuration.SHORT.getValue()) {
            return MicrophoneDuration.SHORT.getPosition();
        } else if (value == MicrophoneDuration.LONG.getValue()) {
            return MicrophoneDuration.LONG.getPosition();
        } else {
            return MicrophoneDuration.VERY_LONG.getPosition();
        }
    }

}


