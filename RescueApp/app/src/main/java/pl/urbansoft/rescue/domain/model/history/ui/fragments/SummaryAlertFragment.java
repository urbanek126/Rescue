package pl.urbansoft.rescue.domain.model.history.ui.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pl.urbansoft.rescue.R;

/**
 * Created by urbanek126 on 12/21/2015.
 */
public class SummaryAlertFragment extends Fragment {

    TextView alertDataText;
    TextView microphoneText;
    TextView microphoneValueText;
    TextView cameraText;
    TextView cameraValueText;
    TextView messageText;
    TextView locationText;
    TextView locationValueText;
    TextView messageValueText;
    TextView contactText;
    TextView contactValueText;

    public SummaryAlertFragment() {
    }

    public static SummaryAlertFragment newInstance() {
        SummaryAlertFragment fragment = new SummaryAlertFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_alert_summary, container, false);

        alertDataText = (TextView) v.findViewById(R.id.alertDataText);
        microphoneText = (TextView) v.findViewById(R.id.microphoneText);
        microphoneValueText = (TextView) v.findViewById(R.id.microphoneValueText);
        cameraText = (TextView) v.findViewById(R.id.cameraText);
        cameraValueText = (TextView) v.findViewById(R.id.cameraValueText);
        messageText = (TextView) v.findViewById(R.id.messageText);
        messageValueText = (TextView) v.findViewById(R.id.messageValueText);
        contactText = (TextView) v.findViewById(R.id.contactText);
        contactValueText = (TextView) v.findViewById(R.id.contactValueText);
        locationText = (TextView) v.findViewById(R.id.locationText);
        locationValueText = (TextView) v.findViewById(R.id.locationValueText);

        initialize(getArguments());

        setFonts();
        return v;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void initialize(Bundle bundle){
        microphoneValueText.setText(bundle.getString("microphone"));
        cameraValueText.setText(bundle.getString("camera"));
        contactValueText.setText(bundle.getString("contact"));
        locationValueText.setText(bundle.getString("location"));
        messageValueText.setText(bundle.getString("message"));
        alertDataText.setText(bundle.getString("alertTitle"));
    }

    private void setFonts() {
        Typeface RobotoRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Regular.ttf");
        Typeface RobotoBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Bold.ttf");

        alertDataText.setTypeface(RobotoBold);
        microphoneText.setTypeface(RobotoRegular);
        microphoneValueText.setTypeface(RobotoRegular);
        cameraText.setTypeface(RobotoRegular);
        cameraValueText.setTypeface(RobotoRegular);
        messageText.setTypeface(RobotoRegular);
        messageValueText.setTypeface(RobotoRegular);
        contactText.setTypeface(RobotoRegular);
        contactValueText.setTypeface(RobotoRegular);
        locationText.setTypeface(RobotoRegular);
        locationValueText.setTypeface(RobotoRegular);
    }
}
