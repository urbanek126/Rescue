package pl.urbansoft.rescue.domain.service;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;

import pl.urbansoft.rescue.R;
import pl.urbansoft.rescue.application.RescueApp;

/**
 * Created by urbanek126 on 12/26/2015.
 */
public class PhoneReactionService {
    private static final int VIBRATE_TIME = 6000000;
    private Context context;
    private Vibrator vibrator;
    private MediaPlayer mediaPlayer;

    public PhoneReactionService() {
        context = RescueApp.getContext();
        vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        setMaxVolume();
    }

    public void startVibrate() {
        vibrator.vibrate(VIBRATE_TIME);
    }

    public void stopVibrate() {
        vibrator.cancel();
    }

    public void startSiren() {
        mediaPlayer = MediaPlayer.create(context, R.raw.siren);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public void stopSiren() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
        }
    }

    private void setMaxVolume() {
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
    }
}
