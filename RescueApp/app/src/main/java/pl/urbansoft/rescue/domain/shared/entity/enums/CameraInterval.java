package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 12/23/2015.
 */
public enum CameraInterval {
    VERY_FAST(60, 0),
    FAST(300, 1),
    SLOWLY(600, 2);

    private int value;
    private int position;

    private CameraInterval(int value, int position) {
        this.value = value;
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public int getValue() {
        return value;
    }

    public static int getCameraIntervalPosition(int value) {

        if (value == CameraInterval.SLOWLY.getValue()) {
            return CameraInterval.SLOWLY.getPosition();
        } else if (value == CameraInterval.FAST.getValue()) {
            return CameraInterval.FAST.getPosition();
        } else {
            return CameraInterval.VERY_FAST.getPosition();
    }
    }
}
