package pl.urbansoft.rescue.infrastructure.injection;

/**
 * Created by andrzej on 11/27/15.
 */

import javax.inject.Singleton;

import dagger.Component;
import pl.urbansoft.rescue.domain.WidgetProvider;
import pl.urbansoft.rescue.domain.model.alert.AlertContext;
import pl.urbansoft.rescue.domain.model.alert.ui.AlertActivity;

import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertActiveFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertContactFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertInactiveFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.AlertPreActiveFragment;
import pl.urbansoft.rescue.domain.model.alert.ui.fragments.IconBarFragment;
import pl.urbansoft.rescue.domain.model.history.ui.HistoryActivity;
import pl.urbansoft.rescue.domain.model.information.InformationActivity;
import pl.urbansoft.rescue.domain.model.information.TutorialActivity;
import pl.urbansoft.rescue.domain.model.information.fragments.InformationFragment;
import pl.urbansoft.rescue.domain.model.register.ui.RegisterActivity;
import pl.urbansoft.rescue.domain.model.settings.ui.SettingsActivity;
import pl.urbansoft.rescue.domain.model.settings.ui.fragments.BaseSettingsFragment;
import pl.urbansoft.rescue.domain.model.settings.ui.fragments.ExtraSettingsFragment;
import pl.urbansoft.rescue.domain.service.AlertService;
import pl.urbansoft.rescue.domain.service.CameraService;
import pl.urbansoft.rescue.domain.service.LocationService;
import pl.urbansoft.rescue.domain.service.MicroPhoneService;
import pl.urbansoft.rescue.domain.service.PhoneService;
import pl.urbansoft.rescue.domain.shared.handler.AlertAlarmHandler;
import pl.urbansoft.rescue.domain.shared.handler.UserHandler;
import pl.urbansoft.rescue.domain.service.DomainModule;
import pl.urbansoft.rescue.domain.service.UserService;
import pl.urbansoft.rescue.domain.shared.ui.BaseActivity;
import pl.urbansoft.rescue.domain.shared.ui.fragments.BleFragment;
import pl.urbansoft.rescue.infrastructure.ble.BleModule;
import pl.urbansoft.rescue.infrastructure.ble.BleService;
import pl.urbansoft.rescue.infrastructure.ble.HtcTagHandler;
import pl.urbansoft.rescue.infrastructure.bus.BusModule;
import pl.urbansoft.rescue.infrastructure.repository.RepositoryModule;
import pl.urbansoft.rescue.infrastructure.sensors.SensorsService;
import pl.urbansoft.rescue.interfaces.broadcast.HeadPhoneReaciver;
import pl.urbansoft.rescue.interfaces.broadcast.PowerButtonReaciver;

@Singleton
@Component(modules={AppModule.class, BleModule.class, BusModule.class,DomainModule.class, RepositoryModule.class})
public interface RescueComponent {
    void inject(HistoryActivity activity);
    void inject(SettingsActivity activity);
    void inject(AlertActivity activity);
    void inject(RegisterActivity activity);
    void inject(AlertPreActiveFragment fragment);
    void inject(IconBarFragment fragment);
    void inject(AlertActiveFragment fragment);
    void inject(AlertInactiveFragment fragment);
    void inject(AlertContactFragment fragment);
    void inject(BaseSettingsFragment fragment);
    void inject(ExtraSettingsFragment fragment);
    void inject(BleFragment fragment);
    void inject(HtcTagHandler handler);
    void inject(AlertAlarmHandler handler);
    void inject(UserHandler handler);
    void inject(UserService service);
    void inject(AlertService service);
    void inject(CameraService service);
    void inject(MicroPhoneService service);
    void inject(LocationService service);
    void inject(BleService service);
    void inject(SensorsService service);
    void inject(HeadPhoneReaciver reciver);
    void inject(PowerButtonReaciver reciver);
    void inject(WidgetProvider provider);
    void inject(PhoneService service);
    void inject(AlertContext conext);
    void inject(BaseActivity activity);
    void inject(InformationActivity activity);
    void inject(TutorialActivity activity);
    void inject(InformationFragment fragment);
}
