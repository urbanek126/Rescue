package pl.urbansoft.rescue.domain.shared.entity.enums;

/**
 * Created by urbanek126 on 1/4/2016.
 */
public enum EventCode {
    ERROR(400),SUCCESS(200);;

    private int code;

    private EventCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
