-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
uses-sdk
ADDED from AndroidManifest.xml:7:5
MERGED from com.android.support:appcompat-v7:21.0.3:15:5
MERGED from com.android.support:support-v4:21.0.3:15:5
	android:targetSdkVersion
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:7:15
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.VIBRATE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
application
ADDED from AndroidManifest.xml:13:5
MERGED from com.android.support:appcompat-v7:21.0.3:16:5
MERGED from com.android.support:support-v4:21.0.3:16:5
	android:label
		ADDED from AndroidManifest.xml:15:9
	android:icon
		ADDED from AndroidManifest.xml:14:9
activity#pl.urbansoft.datacollector.CollectorActivity
ADDED from AndroidManifest.xml:16:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:19:13
	android:label
		ADDED from AndroidManifest.xml:18:13
	android:name
		ADDED from AndroidManifest.xml:17:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:20:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:21:17
	android:name
		ADDED from AndroidManifest.xml:21:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:23:17
	android:name
		ADDED from AndroidManifest.xml:23:27
service#pl.urbansoft.datacollector.SensorsService
ADDED from AndroidManifest.xml:28:9
	android:name
		ADDED from AndroidManifest.xml:28:18
