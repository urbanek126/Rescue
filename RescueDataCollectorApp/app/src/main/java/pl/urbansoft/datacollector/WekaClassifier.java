package pl.urbansoft.datacollector;

/**
 * Created by andrzej on 11/15/15.
 */
public class WekaClassifier {
    public static double classify(Object[] i)
            throws Exception {

        double p = Double.NaN;
        p = WekaClassifier.N8270a830(i);
        return p;
    }
    static double N8270a830(Object []i) {
        double p = Double.NaN;
        if (i[64] == null) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() <= 41.583675) {
            p = 0;
        } else if (((Double) i[64]).doubleValue() > 41.583675) {
            p = WekaClassifier.N3b2322a31(i);
        }
        return p;
    }
    static double N3b2322a31(Object []i) {
        double p = Double.NaN;
        if (i[1] == null) {
            p = 0;
        } else if (((Double) i[1]).doubleValue() <= 334.543682) {
            p = WekaClassifier.N7fb719a92(i);
        } else if (((Double) i[1]).doubleValue() > 334.543682) {
            p = 1;
        }
        return p;
    }
    static double N7fb719a92(Object []i) {
        double p = Double.NaN;
        if (i[4] == null) {
            p = 1;
        } else if (((Double) i[4]).doubleValue() <= 57.827341) {
            p = 1;
        } else if (((Double) i[4]).doubleValue() > 57.827341) {
            p = 0;
        }
        return p;
    }
}

