package pl.urbansoft.datacollector;

import android.os.AsyncTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.activation.CommandMap;
import javax.activation.MailcapCommandMap;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.sql.DataSource;
import javax.activation.DataHandler;


/**
 * Created by andrzej on 11/14/15.
 */
public class MailService {

    private Session session;
    private Multipart multipart;

    private static Properties properties = new Properties();
    static{
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "587");

        MailcapCommandMap mc = (MailcapCommandMap) CommandMap.getDefaultCommandMap();
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html");
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml");
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain");
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed");
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822");
        CommandMap.setDefaultCommandMap(mc);
    }

    public void sendMail(String[] email, String subject, String messageBody ,File file) {
        session = createSessionObject();
        Message message = createMessage(email, subject, messageBody, file);

        new SendMailTask().execute(message);
    }

    private Session createSessionObject() {
        return Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("naratunek.app@gmail.com", "ratunek2015");
            }
        });
    }

    private Message createMessage(String[] emails, String subject, String messageBody ,File file)  {
        Message message = new MimeMessage(this.session);
        this.multipart = new MimeMultipart();

        try {
            message.setFrom(new InternetAddress("naratunek.app@gmail.com", "Kolekcja danych"));
            for(String e:emails) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(e, e));
            }
            message.setSubject(subject);

            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(messageBody, "text/html");
            MimeBodyPart attachment = new MimeBodyPart();
            javax.activation.DataSource ds = new ByteArrayDataSource(new FileInputStream(file), "application/x-any");
            attachment.setFileName(new java.util.Date()+"-" + file.getName());
            attachment.setDataHandler(new DataHandler(ds));

            this.multipart.addBodyPart(messageBodyPart);
            this.multipart.addBodyPart(attachment);
            message.setContent(this.multipart);

        }catch(MessagingException | IOException e){
            e.printStackTrace();
        }
        return message;
    }

    class SendMailTask extends AsyncTask<Message, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected Void doInBackground(Message... messages) {
            try {
                Transport.send(messages[0]);
            } catch (MessagingException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            return null;

        }
    }
}