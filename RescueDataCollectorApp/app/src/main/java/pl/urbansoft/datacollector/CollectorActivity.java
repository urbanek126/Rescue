package pl.urbansoft.datacollector;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;

public class CollectorActivity extends Activity {

	private Bundle savedInstanceState;

	private enum State {
		IDLE, COLLECTING, TRAINING, CLASSIFYING
	};

	private final String[] mLabels = {
			Globals.CLASS_LABEL_NORMAL,
			Globals.CLASS_LABEL_HAZARD
	};

	private RadioGroup radioGroup;
	private final RadioButton[] radioBtns = new RadioButton[4];
	private Intent mServiceIntent;
	private File mFeatureFile;

	private State mState;
	private Button btnDelete;
	private Button btnUpload;

	private DataUpdateReceiver updateReceiver;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(pl.urbansoft.datacollector.R.layout.main);
		radioGroup = (RadioGroup) findViewById(pl.urbansoft.datacollector.R.id.radioGroupLabels);
		radioBtns[0] = (RadioButton) findViewById(R.id.normal);
		radioBtns[1] = (RadioButton) findViewById(R.id.hazard);

		btnDelete = (Button) findViewById(pl.urbansoft.datacollector.R.id.btnDeleteData);
		btnUpload = (Button) findViewById(R.id.btnUpload);

		DateFormat df = DateFormat.getDateTimeInstance();

				mState = State.IDLE;
		mFeatureFile = new File(getExternalFilesDir(null), Globals.FEATURE_FILE_NAME);

		mServiceIntent = new Intent(this, SensorsService.class);

	}

	@Override
	public void onResume() {
		super.onResume();

		if (updateReceiver == null) updateReceiver = new DataUpdateReceiver();
		IntentFilter intentFilter = new IntentFilter(RefreshEvent.HAZARD_DETECTED.name());
		registerReceiver(updateReceiver, intentFilter);
	}


	@Override
	public void onPause(){
		super.onPause();
		if (updateReceiver != null) unregisterReceiver(updateReceiver);

	}

	public void onCollectClicked(View view) {

		if (mState == State.IDLE) {
			mState = State.COLLECTING;
			((Button) view).setText(pl.urbansoft.datacollector.R.string.ui_collector_button_stop_title);
			btnDelete.setEnabled(false);
			btnUpload.setEnabled(false);

			radioBtns[0].setEnabled(false);
			radioBtns[1].setEnabled(false);

			int acvitivtyId = radioGroup.indexOfChild(findViewById(radioGroup
					.getCheckedRadioButtonId()));
			String label = mLabels[acvitivtyId];

			Bundle extras = new Bundle();
			extras.putString(Globals.CLASS_LABEL_KEY, label);
			mServiceIntent.putExtras(extras);

			startService(mServiceIntent);

		} else if (mState == State.COLLECTING) {
			mState = State.IDLE;
			((Button) view).setText(pl.urbansoft.datacollector.R.string.ui_collector_button_start_title);
			btnDelete.setEnabled(true);
			btnUpload.setEnabled(true);

			radioBtns[0].setEnabled(true);
			radioBtns[1].setEnabled(true);

			stopService(mServiceIntent);
			((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).cancelAll();
		}
	}

	public void onDeleteDataClicked(View view) {

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {
			if (mFeatureFile.exists()) {
				mFeatureFile.delete();
			}

			Toast.makeText(getApplicationContext(),
					pl.urbansoft.datacollector.R.string.ui_collector_toast_file_deleted,
					Toast.LENGTH_SHORT).show();
		}
	}

	public void onUploadClicked(View view) {

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

			File file = new File(getExternalFilesDir(null),Globals.FEATURE_FILE_NAME);

			if (file.exists()) {
				new MailService().sendMail(Globals.TARGET_MAIL, "Training data: " + new java.util.Date(),
						"Training data", file);

				Toast.makeText(getApplicationContext(),
						R.string.ui_collector_toast_file_send,
						Toast.LENGTH_SHORT).show();

				file.delete();

			}
			else {
				Toast.makeText(getApplicationContext(),
						R.string.ui_collector_toast_no_file,
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onBackPressed() {

		if (mState == State.TRAINING) {
			return;
		} else if (mState == State.COLLECTING || mState == State.CLASSIFYING) {
			stopService(mServiceIntent);
			((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
					.cancel(Globals.NOTIFICATION_ID);
		}
		super.onBackPressed();
	}

	@Override
	public void onDestroy() {
		// Stop the service and the notification.
		// Need to check whether the mSensorService is null or not.
		if (mState == State.TRAINING) {
			return;
		} else if (mState == State.COLLECTING || mState == State.CLASSIFYING) {
			stopService(mServiceIntent);
			((NotificationManager) getSystemService(NOTIFICATION_SERVICE))
					.cancelAll();
		}
		finish();
		super.onDestroy();
	}

	private class DataUpdateReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {

			Toast.makeText(getApplicationContext(),
					pl.urbansoft.datacollector.R.string.ui_collector_toast_hazard_detected,
					Toast.LENGTH_SHORT).show();

			Vibrator v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(5000);
		}
	}
}